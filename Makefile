CC=gcc
CFLAGS=`gtk-config-2.0 --cflags` -Wall -O -g
LIBS= `gtk-config-2.0 --libs gtk`
OBJS= \
	grbtree.o 		\
	gtklistmodel.o		\
	gtkmodelsimple.o	\
	gtktlcell.o		\
	gtktlcelltext.o		\
	gtktlcelltextpixbuf.o	\
	gtktlcelltoggle.o	\
	gtktlcellpixbuf.o	\
	gtktlview.o		\
	gtktlmodel.o		\
	gtktlselection.o	\
	gtktlcolumn.o

##	gtktreemodel.o\ #We'll fix this up later

all: simple simple2

simple: $(OBJS) simple.o
	$(CC) $(CFLAGS) -o simple simple.o $(OBJS) $(LIBS) -lpango  -lfribidi

simple2: $(OBJS) simple2.o
	$(CC) $(CFLAGS) -o simple2 simple2.o $(OBJS) $(LIBS) -lpango  -lfribidi

filetest: $(OBJS) filetest.o gtkfilemodel.o
	$(CC) $(CFLAGS) -o filetest gtkfilemodel.o filetest.o $(OBJS) $(LIBS) -lpango  -lfribidi

treetest: $(OBJS) treetest.o
	$(CC) $(CFLAGS) -o treetest $(OBJS) treetest.o $(LIBS) -lpango  -lfribidi


clean:
	rm -f *o *a *~ treetest filetest simple simple2 core
