
#include "gtktlview.h"
#include "gtkfilemodel.h"

int
main (int argc, char *argv[])
{
  GtkObject *filetest;
  GtkWidget *window;
  GtkWidget *sw;
  GtkWidget *tlview;
  

  gtk_init (&argc, &argv);

  filetest = gtk_file_model_new ();
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);
  sw = gtk_scrolled_window_new (NULL, NULL);
  tlview = gtk_tlview_new_with_model (GTK_TLMODEL (filetest));

  gtk_container_add (GTK_CONTAINER (window), sw);
  gtk_container_add (GTK_CONTAINER (sw), tlview);
  gtk_widget_show_all (window);

  gtk_main ();

  return 0;
}



