/* gbtree.h
 * Copyright (C) 2000  Red Hat Inc. <jrb@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GTK_TLVIEW_H__
#define __GTK_TLVIEW_H__

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

enum {
	G_RBNODE_BLACK = 0,
	G_RBNODE_RED
};

typedef struct _GRBTree GRBTree
typedef struct _GRBNode GRBNode

struct _GRBTree {
	GRBNode *root;
	GCompareFunc *func;
}

typedef void (*GRBTreeForeachFunc) (GRBNode  *node,
				    gpointer  data);

struct _GRBNode {
  gpointer data;
  GRBNode *left;         /* left child */
  GRBNode *right;        /* right child */
  GRBNode *parent;       /* parent */
  gint color : 1;
};



void     g_rbtree_push_allocator (GAllocator         *allocator);
void     g_rbtree_pop_allocator  (void);
GRBNode *g_rbnode_new            (gpointer            data);
GRBNode *g_rbnode_destroy        (GRBNode            *node);
GRBNode *g_rbtree_insert         (GRBTree            *tree,
				  GRBNode            *node);
void     g_rbtree_foreach        (GRBTree            *tree,
				  GTraverseFlags      flags,
				  GRBTreeForeachFunc  func,
				  gpointer            data);

			   

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
