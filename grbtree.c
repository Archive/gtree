#include "grbtree.h"

static void     g_rbnode_validate_allocator (GAllocator *allocator);
static GRBNode *g_rbnode_new                (GRBTree    *tree,
					     gint       height);
static void     g_rbnode_free               (GRBNode    *node);
static void     g_rbnode_rotate_left        (GRBTree    *tree,
					     GRBNode    *node);
static void     g_rbnode_rotate_left        (GRBTree    *tree,
					     GRBNode    *node);
static void     g_rbtree_insert_fixup       (GRBTree    *tree,
					     GRBNode    *node);
static void     g_rbtree_remove_node_fixup  (GRBTree    *tree,
					     GRBNode    *node);
static gint     count_nodes                 (GRBTree    *tree,
					     GRBNode    *node);



/* node allocation
 */
struct _GAllocator /* from gmem.c */
{
  gchar         *name;
  guint16        n_preallocs;
  guint          is_unused : 1;
  guint          type : 4;
  GAllocator    *last;
  GMemChunk     *mem_chunk;
  GRBNode       *free_nodes; /* implementation specific */
};


G_LOCK_DEFINE_STATIC (current_allocator);
static GAllocator *current_allocator = NULL;

/* HOLDS: current_allocator_lock */
static void
g_rbnode_validate_allocator (GAllocator *allocator)
{
  g_return_if_fail (allocator != NULL);
  g_return_if_fail (allocator->is_unused == TRUE);

  if (allocator->type != G_ALLOCATOR_NODE)
    {
      allocator->type = G_ALLOCATOR_NODE;
      if (allocator->mem_chunk)
	{
	  g_mem_chunk_destroy (allocator->mem_chunk);
	  allocator->mem_chunk = NULL;
	}
    }

  if (!allocator->mem_chunk)
    {
      allocator->mem_chunk = g_mem_chunk_new (allocator->name,
					      sizeof (GRBNode),
					      sizeof (GRBNode) * allocator->n_preallocs,
					      G_ALLOC_ONLY);
      allocator->free_nodes = NULL;
    }

  allocator->is_unused = FALSE;
}

static GRBNode *
g_rbnode_new (GRBTree *tree,
	      gint     height)
{
  GRBNode *node;

  G_LOCK (current_allocator);
  if (!current_allocator)
    {
       GAllocator *allocator = g_allocator_new ("GTK+ default GRBNode allocator",
						128);
       g_rbnode_validate_allocator (allocator);
       allocator->last = NULL;
       current_allocator = allocator;
    }
  if (!current_allocator->free_nodes)
    node = g_chunk_new (GRBNode, current_allocator->mem_chunk);
  else
    {
      node = current_allocator->free_nodes;
      current_allocator->free_nodes = node->left;
    }
  G_UNLOCK (current_allocator);

  node->left = tree->nil;
  node->right = tree->nil;
  node->parent = tree->nil;
  node->flags = G_RBNODE_RED;
  node->count = 1;
  node->children = NULL;
  node->offset = height;
  return node;
}

static void
g_rbnode_free (GRBNode *node)
{
  G_LOCK (current_allocator);
  node->left = current_allocator->free_nodes;
  current_allocator->free_nodes = node;
  G_UNLOCK (current_allocator);
}

static void
g_rbnode_rotate_left (GRBTree *tree,
		      GRBNode *node)
{
  gint node_height, right_height;
  GRBNode *right = node->right;

  g_return_if_fail (node != tree->nil);

  node_height = node->offset -
	  (node->left?node->left->offset:0) -
	  (node->right?node->right->offset:0) -
	  (node->children?node->children->root->offset:0);
  right_height = right->offset -
	  (right->left?right->left->offset:0) -
	  (right->right?right->right->offset:0) -
	  (right->children?right->children->root->offset:0);

  node->right = right->left;
  if (right->left != tree->nil)
    right->left->parent = node;

  if (right != tree->nil)
    right->parent = node->parent;
  if (node->parent != tree->nil)
    {
      if (node == node->parent->left)
	node->parent->left = right;
      else
	node->parent->right = right;
    } else {
      tree->root = right;
    }

    right->left = node;
    if (node != tree->nil)
      node->parent = right;

    node->count = 1 + (node->left?node->left->count:0) +
      (node->right?node->right->count:0);
    right->count = 1 + (right->left?right->left->count:0) +
      (right->right?right->right->count:0);
    node->offset = node_height +
	    (node->left?node->left->offset:0) +
	    (node->right?node->right->offset:0) +
	    (node->children?node->children->root->offset:0);
    right->offset = right_height +
	    (right->left?right->left->offset:0) +
	    (right->right?right->right->offset:0) +
	    (right->children?right->children->root->offset:0);
}

static void
g_rbnode_rotate_right (GRBTree *tree,
		       GRBNode *node)
{
  gint node_height, left_height;
  GRBNode *left = node->left;

  g_return_if_fail (node != tree->nil);

  node_height = node->offset -
	  (node->left?node->left->offset:0) -
	  (node->right?node->right->offset:0) -
	  (node->children?node->children->root->offset:0);
  left_height = left->offset -
	  (left->left?left->left->offset:0) -
	  (left->right?left->right->offset:0) -
	  (left->children?left->children->root->offset:0);

  node->left = left->right;
  if (left->right != tree->nil)
    left->right->parent = node;

  if (left != tree->nil)
    left->parent = node->parent;
  if (node->parent != tree->nil)
    {
      if (node == node->parent->right)
	node->parent->right = left;
      else
	node->parent->left = left;
    }
  else
    {
      tree->root = left;
    }

    /* link node and left */
    left->right = node;
    if (node != tree->nil)
      node->parent = left;

    node->count = 1 + (node->left?node->left->count:0) +
      (node->right?node->right->count:0);
    left->count = 1 + (left->left?left->left->count:0) +
      (left->right?left->right->count:0);
    node->offset = node_height +
	    (node->left?node->left->offset:0) +
	    (node->right?node->right->offset:0) +
	    (node->children?node->children->root->offset:0);
    left->offset = left_height +
	    (left->left?left->left->offset:0) +
	    (left->right?left->right->offset:0) +
	    (left->children?left->children->root->offset:0);
}

static void
g_rbtree_insert_fixup (GRBTree *tree,
		       GRBNode *node)
{

  /* check Red-Black properties */
  while (node != tree->root && G_RBNODE_GET_COLOR (node->parent) == G_RBNODE_RED)
    {
      /* we have a violation */
      if (node->parent == node->parent->parent->left)
	{
	  GRBNode *y = node->parent->parent->right;
	  if (G_RBNODE_GET_COLOR (y) == G_RBNODE_RED)
	    {
	      /* uncle is G_RBNODE_RED */
	      G_RBNODE_SET_COLOR (node->parent, G_RBNODE_BLACK);
	      G_RBNODE_SET_COLOR (y, G_RBNODE_BLACK);
	      G_RBNODE_SET_COLOR (node->parent->parent, G_RBNODE_RED);
	      node = node->parent->parent;
            }
	  else
	    {
	      /* uncle is G_RBNODE_BLACK */
	      if (node == node->parent->right)
		{
		  /* make node a left child */
		  node = node->parent;
		  g_rbnode_rotate_left (tree, node);
                }

	      /* recolor and rotate */
	      G_RBNODE_SET_COLOR (node->parent, G_RBNODE_BLACK);
	      G_RBNODE_SET_COLOR (node->parent->parent, G_RBNODE_RED);
	      g_rbnode_rotate_right(tree, node->parent->parent);
            }
        }
      else
	{
	  /* mirror image of above code */
	  GRBNode *y = node->parent->parent->left;
	  if (G_RBNODE_GET_COLOR (y) == G_RBNODE_RED)
	    {
	      /* uncle is G_RBNODE_RED */
	      G_RBNODE_SET_COLOR (node->parent, G_RBNODE_BLACK);
	      G_RBNODE_SET_COLOR (y, G_RBNODE_BLACK);
	      G_RBNODE_SET_COLOR (node->parent->parent, G_RBNODE_RED);
	      node = node->parent->parent;
            }
	  else
	    {
	      /* uncle is G_RBNODE_BLACK */
	      if (node == node->parent->left)
		{
		  node = node->parent;
		  g_rbnode_rotate_right (tree, node);
                }
	      G_RBNODE_SET_COLOR (node->parent, G_RBNODE_BLACK);
	      G_RBNODE_SET_COLOR (node->parent->parent, G_RBNODE_RED);
	      g_rbnode_rotate_left (tree, node->parent->parent);
            }
        }
    }
  G_RBNODE_SET_COLOR (tree->root, G_RBNODE_BLACK);
}

static void
g_rbtree_remove_node_fixup (GRBTree *tree,
			    GRBNode *node)
{
  while (node != tree->root && G_RBNODE_GET_COLOR (node) == G_RBNODE_BLACK)
    {
      if (node == node->parent->left)
	{
	  GRBNode *w = node->parent->right;
	  if (G_RBNODE_GET_COLOR (w) == G_RBNODE_RED)
	    {
	      G_RBNODE_SET_COLOR (w, G_RBNODE_BLACK);
	      G_RBNODE_SET_COLOR (node->parent, G_RBNODE_RED);
	      g_rbnode_rotate_left (tree, node->parent);
	      w = node->parent->right;
	    }
	  if (G_RBNODE_GET_COLOR (w->left) == G_RBNODE_BLACK && G_RBNODE_GET_COLOR (w->right) == G_RBNODE_BLACK)
	    {
	      G_RBNODE_SET_COLOR (w, G_RBNODE_RED);
	      node = node->parent;
	    }
	  else
	    {
	      if (G_RBNODE_GET_COLOR (w->right) == G_RBNODE_BLACK)
		{
		  G_RBNODE_SET_COLOR (w->left, G_RBNODE_BLACK);
		  G_RBNODE_SET_COLOR (w, G_RBNODE_RED);
		  g_rbnode_rotate_right (tree, w);
		  w = node->parent->right;
		}
	      G_RBNODE_SET_COLOR (w, G_RBNODE_GET_COLOR (node->parent));
	      G_RBNODE_SET_COLOR (node->parent, G_RBNODE_BLACK);
	      G_RBNODE_SET_COLOR (w->right, G_RBNODE_BLACK);
	      g_rbnode_rotate_left (tree, node->parent);
	      node = tree->root;
	    }
	}
      else
	{
	  GRBNode *w = node->parent->left;
	  if (G_RBNODE_GET_COLOR (w) == G_RBNODE_RED)
	    {
	      G_RBNODE_SET_COLOR (w, G_RBNODE_BLACK);
	      G_RBNODE_SET_COLOR (node->parent, G_RBNODE_RED);
	      g_rbnode_rotate_right (tree, node->parent);
	      w = node->parent->left;
	    }
	  if (G_RBNODE_GET_COLOR (w->right) == G_RBNODE_BLACK && G_RBNODE_GET_COLOR (w->left) == G_RBNODE_BLACK)
	    {
	      G_RBNODE_SET_COLOR (w, G_RBNODE_RED);
	      node = node->parent;
	    }
	  else
	    {
	      if (G_RBNODE_GET_COLOR (w->left) == G_RBNODE_BLACK)
		{
		  G_RBNODE_SET_COLOR (w->right, G_RBNODE_BLACK);
		  G_RBNODE_SET_COLOR (w, G_RBNODE_RED);
		  g_rbnode_rotate_left (tree, w);
		  w = node->parent->left;
		}
	      G_RBNODE_SET_COLOR (w, G_RBNODE_GET_COLOR (node->parent));
	      G_RBNODE_SET_COLOR (node->parent, G_RBNODE_BLACK);
	      G_RBNODE_SET_COLOR (w->left, G_RBNODE_BLACK);
	      g_rbnode_rotate_right (tree, node->parent);
	      node = tree->root;
	    }
	}
    }
  G_RBNODE_SET_COLOR (node, G_RBNODE_BLACK);
}

/* Public functions */
void
g_rbnode_push_allocator (GAllocator *allocator)
{
  G_LOCK (current_allocator);
  g_rbnode_validate_allocator ( allocator );
  allocator->last = current_allocator;
  current_allocator = allocator;
  G_UNLOCK (current_allocator);
}

void
g_rbnode_pop_allocator (void)
{
  G_LOCK (current_allocator);
  if (current_allocator)
    {
      GAllocator *allocator;

      allocator = current_allocator;
      current_allocator = allocator->last;
      allocator->last = NULL;
      allocator->is_unused = TRUE;
    }
  G_UNLOCK (current_allocator);
}

GRBTree *
g_rbtree_new (void)
{
  GRBTree *retval;

  retval = (GRBTree *) g_new (GRBTree, 1);
  retval->parent_tree = NULL;
  retval->parent_node = NULL;

  retval->nil = g_new0 (GRBNode, 1);
  retval->nil->left = NULL;
  retval->nil->right = NULL;
  retval->nil->parent = NULL;
  retval->nil->flags = G_RBNODE_BLACK;
  retval->nil->count = 0;
  retval->nil->offset = 0;

  retval->root = retval->nil;
  return retval;
}

static void
g_rbtree_free_helper (GRBTree  *tree,
		      GRBNode  *node,
		      gpointer  data)
{
  if (node->children)
    g_rbtree_free (node->children);

  g_rbnode_free (node);
}

void
g_rbtree_free (GRBTree *tree)
{
  g_rbtree_traverse (tree,
		     tree->root,
		     G_POST_ORDER,
		     g_rbtree_free_helper,
		     NULL);

  if (tree->parent_node &&
      tree->parent_node->children == tree)
    tree->parent_node->children = NULL;
  g_rbnode_free (tree->nil);
  g_free (tree);
}

void
g_rbtree_remove (GRBTree *tree)
{
  GRBTree *tmp_tree;
  GRBNode *tmp_node;

  gint height = tree->root->offset;
  tmp_tree = tree->parent_tree;
  tmp_node = tree->parent_node;

  while (tmp_tree && tmp_node && tmp_node != tmp_tree->nil)
    {
      tmp_node->offset -= height;
      tmp_node = tmp_node->parent;
      if (tmp_node == tmp_tree->nil)
	{
	  tmp_node = tmp_tree->parent_node;
	  tmp_tree = tmp_tree->parent_tree;
	}
    }
  g_rbtree_free (tree);
}


GRBNode *
g_rbtree_insert_after (GRBTree  *tree,
		       GRBNode  *current,
		       gint      height)
{
  GRBNode *node;
  gboolean right = TRUE;
  GRBNode *tmp_node;
  GRBTree *tmp_tree;

  if (current != NULL && current->right != tree->nil)
    {
      current = current->right;
      while (current->left != tree->nil)
	current = current->left;
      right = FALSE;
    }

  /* setup new node */
  node = g_rbnode_new (tree, height);
  node->parent = (current?current:tree->nil);

  /* insert node in tree */
  if (current)
    {
      if (right)
	current->right = node;
      else
	current->left = node;
      tmp_node = node->parent;
      tmp_tree = tree;
    }
  else
    {
      tree->root = node;
      tmp_node = tree->parent_node;
      tmp_tree = tree->parent_tree;
    }

  while (tmp_tree && tmp_node && tmp_node != tmp_tree->nil)
    {
      /* We only want to propagate the count if we are in the tree we
       * started in. */
      if (tmp_tree == tree)
	tmp_node->count++;
      tmp_node->offset += height;
      tmp_node = tmp_node->parent;
      if (tmp_node == tmp_tree->nil)
	{
	  tmp_node = tmp_tree->parent_node;
	  tmp_tree = tmp_tree->parent_tree;
	}
    }
  g_rbtree_insert_fixup (tree, node);
  return node;
}

GRBNode *
g_rbtree_find_count (GRBTree *tree,
		     gint     count)
{
  GRBNode *node;

  node = tree->root;
  while (node != tree->nil && (node->left->count + 1 != count))
    {
      if (node->left->count >= count)
	node = node->left;
      else
	{
	  count -= (node->left->count + 1);
	  node = node->right;
	}
    }
  if (node == tree->nil)
    return NULL;
  return node;
}

void
g_rbtree_node_set_height (GRBTree *tree,
			  GRBNode *node,
			  gint     height)
{
  gint diff = height - G_RBNODE_GET_HEIGHT (node);
  GRBNode *tmp_node = node;
  GRBTree *tmp_tree = tree;

  if (diff == 0)
    return;

  while (tmp_tree && tmp_node && tmp_node != tmp_tree->nil)
    {
      tmp_node->offset += diff;
      tmp_node = tmp_node->parent;
      if (tmp_node == tmp_tree->nil)
	{
	  tmp_node = tmp_tree->parent_node;
	  tmp_tree = tmp_tree->parent_tree;
	}
    }
}

gint
g_rbtree_node_find_offset (GRBTree *tree,
			   GRBNode *node)
{
  GRBNode *last;
  gint retval = node->left->offset;

  while (tree && node && node != tree->nil)
    {
      last = node;
      node = node->parent;
      if (node->right == last)
	retval += node->left->offset + G_RBNODE_GET_HEIGHT (node);
      if (node == tree->nil)
	{
	  node = tree->parent_node;
	  tree = tree->parent_tree;
	  if (node)
	    retval += node->left->offset;
	}
    }
  return retval;
}

gint
g_rbtree_find_offset (GRBTree  *tree,
		      gint      height,
		      GRBTree **new_tree,
		      GRBNode **new_node)
{
  GRBNode *tmp_node;

  tmp_node = tree->root;
  while (tmp_node != tree->nil &&
	 (tmp_node->left->offset > height ||
	  (tmp_node->offset - tmp_node->right->offset) < height))
    {
      if (tmp_node->left->offset > height)
	tmp_node = tmp_node->left;
      else
	{
	  height -= (tmp_node->offset - tmp_node->right->offset);
	  tmp_node = tmp_node->right;
	}
    }
  if (tmp_node == tree->nil)
    {
      *new_tree = NULL;
      *new_node = NULL;
      return 0;
    }
  if (tmp_node->children)
    {
      if ((tmp_node->offset -
	   tmp_node->right->offset -
	   tmp_node->children->root->offset) > height)
	{
	  *new_tree = tree;
	  *new_node = tmp_node;
	  return (height - tmp_node->left->offset);
	}
      return g_rbtree_find_offset (tmp_node->children,
				   height - tmp_node->left->offset -
				   (tmp_node->offset -
				    tmp_node->left->offset -
				    tmp_node->right->offset -
				    tmp_node->children->root->offset),
				   new_tree,
				   new_node);
    }
  *new_tree = tree;
  *new_node = tmp_node;
  return (height - tmp_node->left->offset);
}


void
g_rbtree_remove_node (GRBTree *tree,
		      GRBNode *node)
{
  GRBNode *x, *y;

  g_return_if_fail (tree != NULL);
  g_return_if_fail (node != NULL);
  /* make sure we're deleting a node that's actually in the tree */
  for (x = node; x->parent != tree->nil; x = x->parent)
    ;
  g_return_if_fail (x == tree->root);

  if (node->left == tree->nil || node->right == tree->nil)
    {
      y = node;
    }
  else
    {
      y = node->right;

      while (y->left != tree->nil)
	y = y->left;
    }
  for (x = y; x != tree->nil; x = x->parent)
    x->count--;
  y->count = node->count;
  /* x is y's only child */
  if (y->left != tree->nil)
    x = y->left;
  else
    x = y->right;

  /* remove y from the parent chain */
  x->parent = y->parent;
  if (y->parent != tree->nil)
    if (y == y->parent->left)
      y->parent->left = x;
    else
      y->parent->right = x;
  else
    tree->root = x;

  if (y != node)
	  node->children = y->children;

  if (G_RBNODE_GET_COLOR (y) == G_RBNODE_BLACK)
	  g_rbtree_remove_node_fixup (tree, x);

  G_LOCK (current_allocator);
  y->left = current_allocator->free_nodes;
  current_allocator->free_nodes = y;
  G_UNLOCK (current_allocator);
}

GRBNode *
g_rbtree_next (GRBTree *tree,
	       GRBNode *node)
{
  g_return_val_if_fail (tree != NULL, NULL);
  g_return_val_if_fail (node != NULL, NULL);

  /* Case 1: the node's below us. */
  if (node->right != tree->nil)
    {
      node = node->right;
      while (node->left != tree->nil)
	node = node->left;
      return node;
    }

  /* Case 2: it's an ancestor */
  while (node->parent != tree->nil)
    {
      if (node->parent->right == node)
	node = node->parent;
      else
	return (node->parent);
    }

  /* Case 3: There is no next node */
  return NULL;
}

GRBNode *
g_rbtree_prev (GRBTree *tree,
	       GRBNode *node)
{
  g_return_val_if_fail (tree != NULL, NULL);
  g_return_val_if_fail (node != NULL, NULL);

  /* Case 1: the node's below us. */
  if (node->left != tree->nil)
    {
      node = node->left;
      while (node->right != tree->nil)
	node = node->right;
      return node;
    }

  /* Case 2: it's an ancestor */
  while (node->parent != tree->nil)
    {
      if (node->parent->left == node)
	node = node->parent;
      else
	return (node->parent);
    }

  /* Case 3: There is no next node */
  return NULL;
}

void
g_rbtree_next_full (GRBTree  *tree,
		    GRBNode  *node,
		    GRBTree **new_tree,
		    GRBNode **new_node)
{
  g_return_if_fail (tree != NULL);
  g_return_if_fail (node != NULL);
  g_return_if_fail (new_tree != NULL);
  g_return_if_fail (new_node != NULL);

  if (node->children)
    {
      *new_tree = node->children;
      *new_node = (*new_tree)->root;
      while ((*new_node)->left != (*new_tree)->nil)
	*new_node = (*new_node)->left;
      return;
    }
  
  *new_tree = tree;
  *new_node = g_rbtree_next (tree, node);

  while ((*new_node == NULL) &&
	 (*new_tree != NULL))
    {
      *new_node = (*new_tree)->parent_node;
      *new_tree = (*new_tree)->parent_tree;
      if (*new_tree)
	*new_node = g_rbtree_next (*new_tree, *new_node);
    }
}

void
g_rbtree_prev_full (GRBTree  *tree,
		    GRBNode  *node,
		    GRBTree **new_tree,
		    GRBNode **new_node)
{
  g_return_if_fail (tree != NULL);
  g_return_if_fail (node != NULL);
  g_return_if_fail (new_tree != NULL);
  g_return_if_fail (new_node != NULL);

  *new_tree = tree;
  *new_node = g_rbtree_prev (tree, node);

  if (*new_node == NULL)
    {
      *new_node = (*new_tree)->parent_node;
      *new_tree = (*new_tree)->parent_tree;
    }
  else
    {
      while ((*new_node)->children)
	{
	  *new_tree = (*new_node)->children;
	  *new_node = (*new_tree)->root;
	  while ((*new_node)->right != (*new_tree)->nil)
	    *new_node = (*new_node)->right;
	}
    }
}

static void
g_rbtree_traverse_pre_order (GRBTree             *tree,
			     GRBNode             *node,
			     GRBTreeTraverseFunc  func,
			     gpointer             data)
{
  if (node == tree->nil)
    return;

  (* func) (tree, node, data);
  g_rbtree_traverse_pre_order (tree, node->left, func, data);
  g_rbtree_traverse_pre_order (tree, node->right, func, data);
}

static void
g_rbtree_traverse_post_order (GRBTree             *tree,
			      GRBNode             *node,
			      GRBTreeTraverseFunc  func,
			      gpointer             data)
{
  if (node == tree->nil)
    return;

  g_rbtree_traverse_post_order (tree, node->left, func, data);
  g_rbtree_traverse_post_order (tree, node->right, func, data);
  (* func) (tree, node, data);
}

void
g_rbtree_traverse (GRBTree             *tree,
		   GRBNode             *node,
		   GTraverseType        order,
		   GRBTreeTraverseFunc  func,
		   gpointer             data)
{
  g_return_if_fail (tree != NULL);
  g_return_if_fail (node != NULL);
  g_return_if_fail (func != NULL);
  g_return_if_fail (order <= G_LEVEL_ORDER);

  switch (order)
    {
    case G_PRE_ORDER:
      g_rbtree_traverse_pre_order (tree, node, func, data);
      break;
    case G_POST_ORDER:
      g_rbtree_traverse_post_order (tree, node, func, data);
      break;
    case G_IN_ORDER:
    case G_LEVEL_ORDER:
    default:
      g_warning ("unsupported traversal order.");
      break;
    }
}

static gint
count_nodes (GRBTree *tree, GRBNode *node)
{
  gint res;
  if (node == tree->nil)
    return 0;

  res = (count_nodes (tree, node->left) +
	 count_nodes (tree, node->right) + 1);

  if (res != node->count)
    g_print ("Tree failed\n");
  return res;
}

void
g_rbtree_test (GRBTree *tree)
{
  if ((count_nodes (tree, tree->root->left) +
       count_nodes (tree, tree->root->right) + 1) == tree->root->count)
    g_print ("Tree passed\n");
  else
    g_print ("Tree failed\n");

}

static void
g_rbtree_test_height_helper (GRBTree *tree, GRBNode *node, gint height)
{
  if (node == tree->nil)
    return;

  if (node->offset -
      (node->left?node->left->offset:0) -
      (node->right?node->right->offset:0) -
      (node->children?node->children->root->offset:0) != height)
    g_error ("tree failed\n");

  g_rbtree_test_height_helper (tree, node->left, height);
  g_rbtree_test_height_helper (tree, node->right, height);
  if (node->children)
    g_rbtree_test_height_helper (node->children, node->children->root, height);

}
void
g_rbtree_test_height (GRBTree *tree, gint height)
{
  g_rbtree_test_height_helper (tree, tree->root, height);
}
