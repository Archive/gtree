/* gbtree.h
 * Copyright (C) 2000  Red Hat Inc. <jrb@redhat.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GRBTREE_H__
#define __GRBTREE_H__

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#include <glib.h>
typedef enum
{
  G_RBNODE_BLACK = 1 << 0,
  G_RBNODE_RED = 1 << 1,
  G_RBNODE_IS_PARENT = 1 << 2,
  G_RBNODE_IS_SELECTED = 1 << 3,
  G_RBNODE_IS_PRELIT = 1 << 4,
  G_RBNODE_IS_VIEW = 1 << 5
} GRBNodeColor;

typedef struct _GRBTree GRBTree;
typedef struct _GRBNode GRBNode;
typedef struct _GRBTreeView GRBTreeView;

typedef void (*GRBTreeTraverseFunc) (GRBTree  *tree,
				     GRBNode  *node,
				     gpointer  data);

struct _GRBTree
{
  GRBNode *root;
  GRBNode *nil;
  GRBTree *parent_tree;
  GRBNode *parent_node;
};

struct _GRBNode
{
  guint flags;
  GRBNode *left;
  GRBNode *right;
  GRBNode *parent;
  gint count;  /* aggregate number of children we have */
  gint offset; /* aggregate of the heights of all our children */
  GRBTree *children;
};

struct _GRBNodeView
{
  GRBNode parent;
  gint offset;
  GRBTree *children;
};
    
#define G_RBNODE_GET_COLOR(node) (node?(((node->flags&G_RBNODE_RED)==G_RBNODE_RED)?G_RBNODE_RED:G_RBNODE_BLACK):G_RBNODE_BLACK)
#define G_RBNODE_SET_COLOR(node,color) if((node->flags&color)!=color)node->flags=node->flags^(G_RBNODE_RED|G_RBNODE_BLACK)
#define G_RBNODE_GET_HEIGHT(node) (node->offset-(node->left->offset+node->right->offset+(node->children?node->children->root->offset:0)))
#define G_RBNODE_SET_FLAG(node, flag)   G_STMT_START{ (node->flags|=flag); }G_STMT_END
#define G_RBNODE_UNSET_FLAG(node, flag) G_STMT_START{ (node->flags&=~(flag)); }G_STMT_END
#define G_RBNODE_FLAG_SET(node, flag) (node?(((node->flags&flag)==flag)?TRUE:FALSE):FALSE)

void     g_rbtree_push_allocator (GAllocator           *allocator);
void     g_rbtree_pop_allocator  (void);
GRBTree *g_rbtree_new            (void);
void     g_rbtree_free           (GRBTree              *tree);
void     g_rbtree_remove         (GRBTree              *tree);
void     g_rbtree_destroy        (GRBTree              *tree);
GRBNode *g_rbtree_insert_after   (GRBTree              *tree,
				  GRBNode              *node,
				  gint                  height);
void     g_rbtree_remove_node    (GRBTree              *tree,
				  GRBNode              *node);
GRBNode *g_rbtree_find_count     (GRBTree              *tree,
				  gint                  count);
void     g_rbtree_node_set_height (GRBTree              *tree,
				   GRBNode              *node,
				   gint                  height);
gint     g_rbtree_node_find_offset (GRBTree *tree,
				    GRBNode *node);
gint     g_rbtree_find_offset    (GRBTree              *tree,
				  gint                  offset,
				  GRBTree             **new_tree,
				  GRBNode             **new_node);
void     g_rbtree_traverse       (GRBTree              *tree,
				  GRBNode              *node,
				  GTraverseType         order,
				  GRBTreeTraverseFunc   func,
				  gpointer              data);
GRBNode *g_rbtree_next           (GRBTree              *tree,
				  GRBNode              *node);
GRBNode *g_rbtree_prev           (GRBTree              *tree,
				  GRBNode              *node);
void     g_rbtree_next_full      (GRBTree              *tree,
				  GRBNode              *node,
				  GRBTree             **new_tree,
				  GRBNode             **new_node);
void     g_rbtree_prev_full      (GRBTree              *tree,
				  GRBNode              *node,
				  GRBTree             **new_tree,
				  GRBNode             **new_node);

/* This func just checks the integrity of the tree */
/* It will go away later. */
void     g_rbtree_test           (GRBTree           *tree);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
