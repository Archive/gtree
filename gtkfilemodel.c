/* gtkfilemodel.c
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

#include <string.h>
#include <gtk/gtk.h>
#include "gtktlmodel.h"
#include "gtktlcelltest.h"
#include "gtkfilemodel.h"

enum {
	G_FILE_TEST_EXISTS=(1<<0)|(1<<1)|(1<<2), /*any type of file*/
	G_FILE_TEST_ISFILE=1<<0,
	G_FILE_TEST_ISLINK=1<<1,
	G_FILE_TEST_ISDIR=1<<2
};

int
g_file_test (const char *filename, int test)
{
	struct stat s;
	if(stat (filename, &s) != 0)
		return FALSE;
	if(!(test & G_FILE_TEST_ISFILE) && S_ISREG(s.st_mode))
		return FALSE;
	if(!(test & G_FILE_TEST_ISLINK) && S_ISLNK(s.st_mode))
		return FALSE;
	if(!(test & G_FILE_TEST_ISDIR) && S_ISDIR(s.st_mode))
		return FALSE;
	return TRUE;
}



static void         gtk_file_model_init              (GtkFileModel      *FileModel);
static void         gtk_file_model_class_init        (GtkFileModelClass *klass);
static gint         gtk_file_model_get_columns       (GtkTLModel        *TLModel);
static gchar *      gtk_file_model_get_column_header (GtkTLModel        *TLModel,
						      gint               column);
static GtkTLNode    gtk_file_model_get_node          (GtkTLModel        *tlmodel,
						      GtkTLPath         *path);
static GtkTLCell   *gtk_file_model_node_get_cell     (GtkTLModel        *TLModel,
						      GtkTLNode          node,
						      gint               column);
static gpointer     gtk_file_model_node_get_data     (GtkTLModel        *TLModel,
						      GtkTLNode          node,
						      gint               column);
static gboolean     gtk_file_model_node_next         (GtkTLModel        *tlmodel,
						      GtkTLNode         *node);
static GtkTLNode    gtk_file_model_node_children     (GtkTLModel        *tlmodel,
						      GtkTLNode          node);
static gboolean     gtk_file_model_node_has_child    (GtkTLModel        *tlmodel,
						      GtkTLNode          node);
static gint         gtk_file_model_node_n_children   (GtkTLModel        *tlmodel,
						      GtkTLNode          node);
static GtkTLNode    gtk_file_model_node_nth_child    (GtkTLModel        *tlmodel,
						      GtkTLNode          node,
						      gint               n);
static GtkTLNode    gtk_file_model_node_parent       (GtkTLModel        *tlmodel,
						      GtkTLNode          node);


static GtkTLModelClass *parent_class = NULL;


GtkType
gtk_file_model_get_type (void)
{
  static GtkType file_model_type = 0;

  if (!file_model_type)
    {
      static const GTypeInfo file_model_info =
      {
	sizeof (GtkFileModelClass),
	NULL,		/* base_init */
	NULL,		/* base_finalize */
        (GClassInitFunc) gtk_file_model_class_init,
	NULL,		/* class_finalize */
	NULL,		/* class_data */
        sizeof (GtkFileModel),
	0,
        (GInstanceInitFunc) gtk_file_model_init,
      };

      file_model_type = g_type_register_static (GTK_TYPE_TLMODEL, "GtkFileModel", &file_model_info);
    }

  return file_model_type;
}

static void
gtk_file_model_class_init (GtkFileModelClass *klass)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) klass;

  parent_class = gtk_type_class (gtk_tlmodel_get_type ());

  GTK_TLMODEL_CLASS (klass)->get_columns = gtk_file_model_get_columns;
  GTK_TLMODEL_CLASS (klass)->get_column_header = gtk_file_model_get_column_header;
  GTK_TLMODEL_CLASS (klass)->get_node = gtk_file_model_get_node;
  GTK_TLMODEL_CLASS (klass)->node_get_cell = gtk_file_model_node_get_cell;
  GTK_TLMODEL_CLASS (klass)->node_get_data = gtk_file_model_node_get_data;
  GTK_TLMODEL_CLASS (klass)->node_next = gtk_file_model_node_next;
  GTK_TLMODEL_CLASS (klass)->node_children = gtk_file_model_node_children;
  GTK_TLMODEL_CLASS (klass)->node_has_child = gtk_file_model_node_has_child;
  GTK_TLMODEL_CLASS (klass)->node_n_children = gtk_file_model_node_n_children;
  GTK_TLMODEL_CLASS (klass)->node_nth_child = gtk_file_model_node_nth_child;
  GTK_TLMODEL_CLASS (klass)->node_parent = gtk_file_model_node_parent;
}

static void
gtk_file_model_init (GtkFileModel *file_model)
{
  file_model->renderer = gtk_tlcell_test_new ();
}

GtkObject *
gtk_file_model_new (void)
{
  return GTK_OBJECT (gtk_type_new (gtk_file_model_get_type ()));
}

/* fulfill the GtkTLModel requirements */
static gint
gtk_file_model_get_columns (GtkTLModel *tlmodel)
{
  g_return_val_if_fail (tlmodel != NULL, 0);
  g_return_val_if_fail (GTK_IS_FILE_MODEL (tlmodel), 0);

  return 1;
}

static gchar *
gtk_file_model_get_column_header (GtkTLModel *tlmodel,
				  gint        column)
{
  g_return_val_if_fail (tlmodel != NULL, NULL);
  g_return_val_if_fail (GTK_IS_FILE_MODEL (tlmodel), NULL);

  return g_strdup ("File Name");
}

static GtkTLNode
gtk_file_model_get_node_helper (gchar *dir,
				gint   depth,
				const gint  *indices)
{
  DIR *d;
  struct dirent *dt;
  gint i = 1;

  d = opendir (dir);
  if (d == NULL)
    return NULL;

  while ((dt = readdir (d)) != NULL)
    {
      if (!strcmp (dt->d_name, "."))
	continue;
      if (!strcmp (dt->d_name, ".."))
	continue;
      if ((indices[0] == i))
	{
	  if (depth == 1)
	    return (GtkTLNode) g_strdup_printf ("%s%s", dir, dt->d_name);
	  else
	    {
	      static GtkTLNode retval;
	      gchar *str;

	      str = g_strdup_printf ("%s%s/", dir, dt->d_name);
	      retval = gtk_file_model_get_node_helper (str,
						       depth -1,
						       indices + 1);
	      g_free (str);
	      return retval;
	    }
	}
      i ++;
    }
  return NULL;
}

static GtkTLNode
gtk_file_model_get_node (GtkTLModel *tlmodel,
			 GtkTLPath  *path)
{
  return gtk_file_model_get_node_helper ("/",
					 gtk_tlpath_get_depth (path),
					 gtk_tlpath_get_indices (path));
}

static GtkTLCell *
gtk_file_model_node_get_cell (GtkTLModel *tlmodel,
			      GtkTLNode   node,
			      gint        column)
{
  g_return_val_if_fail (tlmodel != NULL, NULL);
  g_return_val_if_fail (GTK_IS_FILE_MODEL (tlmodel), NULL);
  g_return_val_if_fail (node != NULL, NULL);
  g_return_val_if_fail (column != 1, NULL);

  return GTK_FILE_MODEL (tlmodel)->renderer;
}

static gpointer
gtk_file_model_node_get_data (GtkTLModel *tlmodel,
			      GtkTLNode   node,
			      gint        column)
{
  g_return_val_if_fail (tlmodel != NULL, NULL);
  g_return_val_if_fail (GTK_IS_FILE_MODEL (tlmodel), NULL);
  g_return_val_if_fail (node != NULL, NULL);
  g_return_val_if_fail (column != 1, NULL);

  return node;
}

static gboolean
gtk_file_model_node_next (GtkTLModel  *tlmodel,
			  GtkTLNode   *node)
{
  DIR *d;
  struct dirent *dt;
  gint next = FALSE;
  gchar *base;
  gchar *name;

  if (node == NULL || *node == NULL)
    {
      G_BREAKPOINT ();
      return FALSE;
    }

  g_print (" in gtk_file_model_node_next: %s\n", *node);
  base = gtk_file_model_node_parent (tlmodel, (char *) *node);
  name = 1 + strrchr ((char *) *node, '/');
  d = opendir (base);
  *node = NULL;

  if (d == NULL)
    return FALSE;

  if (name == NULL || *name == '\000')
    next = TRUE;

  while ((dt = readdir (d)) != NULL)
    {
      if (!strcmp (dt->d_name, "."))
	continue;
      if (!strcmp (dt->d_name, ".."))
	continue;
      if (next)
	{
	  gchar *ret = g_strdup_printf ("%s%s", base, dt->d_name);
	  *node = ret;
	  return TRUE;
	}
      if (!strcmp (dt->d_name, name))
	next = TRUE;
    }
  return FALSE;
}

static GtkTLNode
gtk_file_model_node_children (GtkTLModel *tlmodel,
			      GtkTLNode   node)
{
  return NULL;
}

static gboolean
gtk_file_model_node_has_child (GtkTLModel *tlmodel,
			       GtkTLNode   node)
{
  return g_file_test ((char *) node, G_FILE_TEST_ISDIR);
}

static gint
gtk_file_model_node_n_children (GtkTLModel *tlmodel,
				GtkTLNode   node)
{
  struct stat st;

  g_return_val_if_fail (node != NULL, 0);

  stat ((char *) node, &st);
  if (S_ISDIR (st.st_mode))
    return (st.st_nlink - 2);
  return 0;
}

static GtkTLNode
gtk_file_model_node_nth_child (GtkTLModel *tlmodel,
			       GtkTLNode   node,
			       gint        n)
{
  return gtk_file_model_get_node_helper ((char *)node,
					 1,
					 &n);
}

static GtkTLNode
gtk_file_model_node_parent (GtkTLModel *tlmodel,
			    GtkTLNode   node)
{
  gchar *retval, *ptr;
  retval = g_strdup ((char *)node);
  ptr = strrchr ((char *)retval, '/');

  if (ptr && *ptr)
    *(ptr + 1) = '\000';
  return retval;
}
