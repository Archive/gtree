/* gtkfilemodel.h
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_FILE_MODEL_H__
#define __GTK_FILE_MODEL_H__

#include <gtk/gtk.h>
#include "gtktlmodel.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define GTK_TYPE_FILE_MODEL			(gtk_file_model_get_type ())
#define GTK_FILE_MODEL(obj)			(GTK_CHECK_CAST ((obj), GTK_TYPE_FILE_MODEL, GtkFileModel))
#define GTK_FILE_MODEL_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_FILEMODEL, GtkFileModelClass))
#define GTK_IS_FILE_MODEL(obj)			(GTK_CHECK_TYPE ((obj), GTK_TYPE_FILE_MODEL))
#define GTK_IS_FILE_MODEL_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_FILE_MODEL))

typedef struct _GtkFileModel       GtkFileModel;
typedef struct _GtkFileModelClass  GtkFileModelClass;

struct _GtkFileModel
{
  GtkTLModel parent;

  GtkTLCell *renderer;
};

struct _GtkFileModelClass
{
  GtkTLModelClass parent_class;
};

GtkType      gtk_file_model_get_type           (void);
GtkObject   *gtk_file_model_new                (void);



#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_FILE_MODEL_H__ */
