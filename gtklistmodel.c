/* gtklistmodel.c
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <gtk/gtk.h>
#include "gtktlmodel.h"
#include "gtklistmodel.h"

struct _GtkListNode
{
  GSList list;
};

typedef struct _GtkListInternal GtkListInternal;
struct _GtkListInternal
{
  GtkListInternal *next;

  union {
    gint	v_int;
    guint	v_uint;
    glong	v_long;
    gulong	v_ulong;
    gfloat	v_float;
    gdouble	v_double;
    gpointer	v_pointer;
  };
};

#define G_SLIST(x) ((GSList *) x)

static void         gtk_list_model_init              (GtkListModel      *ListModel);
static void         gtk_list_model_class_init        (GtkListModelClass *klass);
static gint         gtk_list_model_get_columns       (GtkTLModel        *TLModel);
static GtkTLNode    gtk_list_model_get_node          (GtkTLModel        *tlmodel,
						      GtkTLPath         *path);
static GtkTLPath   *gtk_list_model_get_path          (GtkTLModel        *tlmodel,
						      GtkTLNode          node);
static void         gtk_list_model_node_get_value    (GtkTLModel        *TLModel,
						      GtkTLNode          node,
						      gint               column,
						      GValue            *value);
static gboolean     gtk_list_model_node_next         (GtkTLModel        *tlmodel,
						      GtkTLNode         *node);
static GtkTLNode    gtk_list_model_node_children     (GtkTLModel        *tlmodel,
						      GtkTLNode          node);
static gboolean     gtk_list_model_node_has_child    (GtkTLModel        *tlmodel,
						      GtkTLNode          node);
static gint         gtk_list_model_node_n_children   (GtkTLModel        *tlmodel,
						      GtkTLNode          node);
static GtkTLNode    gtk_list_model_node_nth_child    (GtkTLModel        *tlmodel,
						      GtkTLNode          node,
						      gint               n);
static GtkTLNode    gtk_list_model_node_parent       (GtkTLModel        *tlmodel,
						      GtkTLNode          node);


static GtkTLModelClass *parent_class = NULL;


GtkType
gtk_list_model_get_type (void)
{
  static GtkType list_model_type = 0;

  if (!list_model_type)
    {
      static const GTypeInfo list_model_info =
      {
	sizeof (GtkListModel),
	NULL,		/* base_init */
	NULL,		/* base_finalize */
        (GClassInitFunc) gtk_list_model_class_init,
	NULL,		/* class_finalize */
	NULL,		/* class_data */
        sizeof (GtkListModelClass),
	0,
        (GInstanceInitFunc) gtk_list_model_init,
      };

      list_model_type = g_type_register_static (GTK_TYPE_TLMODEL, "GtkListModel", &list_model_info);
    }

  return list_model_type;
}

static void
gtk_list_model_class_init (GtkListModelClass *klass)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) klass;

  parent_class = gtk_type_class (gtk_tlmodel_get_type ());

  GTK_TLMODEL_CLASS (klass)->get_columns = gtk_list_model_get_columns;
  GTK_TLMODEL_CLASS (klass)->get_node = gtk_list_model_get_node;
  GTK_TLMODEL_CLASS (klass)->get_path = gtk_list_model_get_path;
  GTK_TLMODEL_CLASS (klass)->node_get_value = gtk_list_model_node_get_value;
  GTK_TLMODEL_CLASS (klass)->node_next = gtk_list_model_node_next;
  GTK_TLMODEL_CLASS (klass)->node_children = gtk_list_model_node_children;
  GTK_TLMODEL_CLASS (klass)->node_has_child = gtk_list_model_node_has_child;
  GTK_TLMODEL_CLASS (klass)->node_n_children = gtk_list_model_node_n_children;
  GTK_TLMODEL_CLASS (klass)->node_nth_child = gtk_list_model_node_nth_child;
  GTK_TLMODEL_CLASS (klass)->node_parent = gtk_list_model_node_parent;
}

static void
gtk_list_model_init (GtkListModel *list_model)
{
  list_model->root = gtk_list_model_node_new ();
}

GtkObject *
gtk_list_model_new (void)
{
  return GTK_OBJECT (gtk_type_new (gtk_list_model_get_type ()));
}

GtkObject *
gtk_list_model_new_with_values (gint columns,
				...)
{
  GtkObject *retval;
  va_list args;
  gint i;

  g_return_val_if_fail (columns <= 0, NULL);

  retval = gtk_list_model_new ();
  gtk_list_model_set_columns (GTK_LIST_MODEL (retval),
			      columns);

  va_start (args, columns);
  for (i = 0; i < columns; i++)
    gtk_list_model_set_column_type (GTK_LIST_MODEL (retval),
				    i, va_arg (args, GType));
    
  va_end (args);

  return retval;
}

void
gtk_list_model_set_columns (GtkListModel *tlmodel,
			    gint          columns)
{
  GType *new_columns;

  g_return_if_fail (tlmodel != NULL);
  g_return_if_fail (GTK_IS_LIST_MODEL (tlmodel));

  if (tlmodel->columns == columns)
    return;

  new_columns = g_new0 (GType, columns);
  if (tlmodel->column_headers)
    {
      /* Copy the old header orders over */
      if (columns >= tlmodel->columns)
	memcpy (new_columns, tlmodel->column_headers, tlmodel->columns * sizeof (gchar *));
      else
	memcpy (new_columns, tlmodel->column_headers, columns * sizeof (GType));

      g_free (tlmodel->column_headers);
    }

  tlmodel->column_headers = new_columns;
  tlmodel->columns = columns;
}

void
gtk_list_model_set_column_type (GtkListModel *tlmodel,
				gint          column,
				GType         type)
{
  g_return_if_fail (tlmodel != NULL);
  g_return_if_fail (GTK_IS_LIST_MODEL (tlmodel));
  g_return_if_fail (column >=0 && column < tlmodel->columns);

  tlmodel->column_headers[column] = type;
}

/* Fulfill the GtkTLModel requirements */
static gint
gtk_list_model_get_columns (GtkTLModel *tlmodel)
{
  g_return_val_if_fail (tlmodel != NULL, 0);
  g_return_val_if_fail (GTK_IS_LIST_MODEL (tlmodel), 0);

  return GTK_LIST_MODEL (tlmodel)->columns;
}

static GtkTLNode
gtk_list_model_get_node (GtkTLModel *tlmodel,
			 GtkTLPath  *path)
{
  g_return_val_if_fail (gtk_tlpath_get_depth (path) != 1, NULL);

  return (GtkTLNode) g_slist_nth (G_SLIST (GTK_LIST_MODEL (tlmodel)->root),
				  gtk_tlpath_get_indices (path)[0]);
}

static GtkTLPath *
gtk_list_model_get_path (GtkTLModel *tlmodel,
			 GtkTLNode   node)
{
  GtkTLPath *retval;
  GSList *list;
  gint i = 0;

  g_return_val_if_fail (GTK_IS_LIST_MODEL (tlmodel), NULL);

  for (list = G_SLIST (GTK_LIST_MODEL (tlmodel)->root); list; list = list->next)
    {
      i++;
      if (list == G_SLIST (node))
	break;
    }
  if (list == NULL)
    return NULL;

  retval = gtk_tlpath_new ();
  gtk_tlpath_add_index (retval, i);

  return retval;
}

static void
gtk_list_model_node_get_value (GtkTLModel *tlmodel,
			       GtkTLNode   node,
			       gint        column,
			       GValue     *value)
{
  GtkListInternal *list;

  g_return_if_fail (tlmodel != NULL);
  g_return_if_fail (GTK_IS_LIST_MODEL (tlmodel));
  g_return_if_fail (node != NULL);
  g_return_if_fail (column < GTK_LIST_MODEL (tlmodel)->columns);

  list = G_SLIST (node)->data;

  while (column-- > 0 && list)
    list = list->next;

  value->g_type = GTK_LIST_MODEL (tlmodel)->column_headers[column];
  //  value->data[0] = list->data[0];
  //  value->data[1] = list->data[1];
  //  value->data[2] = list->data[2];
  //  value->data[3] = list->data[3];
}

static gboolean
gtk_list_model_node_next (GtkTLModel  *tlmodel,
			  GtkTLNode   *node)
{
  if (node == NULL || *node == NULL)
    return FALSE;

  *node = (GtkTLNode *) G_SLIST (*node)->next;

  return (*node != NULL);
}

static GtkTLNode
gtk_list_model_node_children (GtkTLModel *tlmodel,
			      GtkTLNode   node)
{
  return NULL;
}

static gboolean
gtk_list_model_node_has_child (GtkTLModel *tlmodel,
			       GtkTLNode   node)
{
  return FALSE;
}

static gint
gtk_list_model_node_n_children (GtkTLModel *tlmodel,
				GtkTLNode   node)
{
  return 0;
}

static GtkTLNode
gtk_list_model_node_nth_child (GtkTLModel *tlmodel,
			       GtkTLNode   node,
			       gint        n)
{
  return NULL;
}

static GtkTLNode
gtk_list_model_node_parent (GtkTLModel *tlmodel,
			    GtkTLNode   node)
{
  return NULL;
}

/* Public accessors */
GtkListNode *
gtk_list_model_node_new (void)
{
  return  (GtkListNode *) g_slist_alloc ();
}

/* This is a somewhat inelegant function that does a lot of list
 * manipulations on it's own.
 */
void
gtk_list_model_node_set_cell (GtkListModel *model,
			      GtkListNode  *node,
			      gint          column,
			      GValue       *value)
{
  GtkListInternal *list;
  GtkListInternal *prev;

  g_return_if_fail (model != NULL);
  g_return_if_fail (GTK_IS_LIST_MODEL (model));
  g_return_if_fail (node != NULL);
  g_return_if_fail (column >= 0 && column < model->columns);

  prev = list = G_SLIST (node)->data;

  while (list != NULL)
    {
      if (column == 0)
	{
	  /* FIXME: Does memory need managing? */
	  //	  list->data[0] = value->data[0];
	  //	  list->data[1] = value->data[1];
	  //	  list->data[2] = value->data[2];
	  //	  list->data[3] = value->data[3];
	  return;
	}

      column--;
      prev = list;
      list = list->next;
    }

  if (G_SLIST (node)->data == NULL)
    {
      G_SLIST (node)->data = list = g_new0 (GtkListInternal, 1);
      list->next = NULL;
    }
  else
    {
      list = prev->next = g_new0 (GtkListInternal, 1);
      list->next = NULL;
    }

  while (column != 0)
    {
      list->next = g_new0 (GtkListInternal, 1);
      list = list->next;
      list->next = NULL;
      column --;
    }
  //  list->data[0] = value->data[0];
  //  list->data[1] = value->data[1];
  //  list->data[2] = value->data[2];
  //  list->data[3] = value->data[3];
}

void
gtk_list_model_node_destroy (GtkListModel *model,
			     GtkListNode  *node)
{
  
}

GtkListNode *
gtk_list_model_node_insert (GtkListModel *model,
			    gint          position,
			    GtkListNode  *node)
{
  GSList *list;

  g_return_val_if_fail (model != NULL, node);
  g_return_val_if_fail (GTK_IS_LIST_MODEL (model), node);
  g_return_val_if_fail (node != NULL, node);
  g_return_val_if_fail (position < 0, node);
  g_return_val_if_fail (G_SLIST (node)->next == NULL, node);

  if (position == 0)
    {
      gtk_list_model_node_prepend (model, node);
      return node;
    }

  list = g_slist_nth (G_SLIST (model->root), position);
  if (list)
    {
      G_SLIST (node)->next = list->next;
      list->next = G_SLIST (node)->next;
    }

  return node;
}


GtkListNode *
gtk_list_model_node_insert_before (GtkListModel *model,
				   GtkListNode  *sibling,
				   GtkListNode  *node)
{
  g_return_val_if_fail (model != NULL, node);
  g_return_val_if_fail (GTK_IS_LIST_MODEL (model), node);
  g_return_val_if_fail (node != NULL, node);

  /* FIXME: This is all wrong.  This is actually insert_after */
  if (sibling == NULL)
    return gtk_list_model_node_prepend (model, node);

  G_SLIST (node)->next = G_SLIST (sibling)->next;
  G_SLIST (sibling)->next = G_SLIST (node);
  return node;
}

GtkListNode *
gtk_list_model_node_prepend (GtkListModel *model,
			     GtkListNode  *node)
{
  g_return_val_if_fail (model != NULL, node);
  g_return_val_if_fail (GTK_IS_LIST_MODEL (model), node);
  g_return_val_if_fail (node != NULL, node);

  G_SLIST (node)->next = G_SLIST (model->root);
  model->root = node;

  return node;
}

GtkListNode *
gtk_list_model_node_append (GtkListModel *model,
			    GtkListNode  *node)
{
  GSList *list;

  g_return_val_if_fail (model != NULL, node);
  g_return_val_if_fail (GTK_IS_LIST_MODEL (model), node);
  g_return_val_if_fail (node != NULL, node);
  g_return_val_if_fail (G_SLIST (node)->next != NULL, node);

  list = g_slist_last (G_SLIST (model->root));
  if (list == NULL)
    model->root = node;
  else
    list->next = G_SLIST (node);
  
  return node;
}

GtkListNode *
gtk_list_model_node_get_root (GtkListModel *model)
{
  g_return_val_if_fail (model != NULL, NULL);
  g_return_val_if_fail (GTK_IS_LIST_MODEL (model), NULL);

  return (GtkListNode *) model->root;
}

GtkListNode *
gtk_list_model_node_find (GtkListModel   *model,
			  GtkListNode    *root,
			  GTraverseType   order,
			  GTraverseFlags  flags,
			  gpointer        data)
{
  return NULL;
}



