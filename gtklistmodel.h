/* gtklistmodel.h
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_LIST_MODEL_H__
#define __GTK_LIST_MODEL_H__

#include <gtk/gtk.h>
#include "gtktlmodel.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define GTK_TYPE_LIST_MODEL			(gtk_list_model_get_type ())
#define GTK_LIST_MODEL(obj)			(GTK_CHECK_CAST ((obj), GTK_TYPE_LIST_MODEL, GtkListModel))
#define GTK_LIST_MODEL_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_LISTMODEL, GtkListModelClass))
#define GTK_IS_LIST_MODEL(obj)			(GTK_CHECK_TYPE ((obj), GTK_TYPE_LIST_MODEL))
#define GTK_IS_LIST_MODEL_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_LIST_MODEL))

typedef struct _GtkListNode        GtkListNode;
typedef struct _GtkListModel       GtkListModel;
typedef struct _GtkListModelClass  GtkListModelClass;

struct _GtkListModel
{
  GtkTLModel parent;

  GtkListNode *root;
  gint columns;
  GType *column_headers;
};

struct _GtkListModelClass
{
  GtkTLModelClass parent_class;
};

GtkType      gtk_list_model_get_type           (void);
GtkObject   *gtk_list_model_new                (void);
GtkObject   *gtk_list_model_new_with_values    (gint            columns,
						...);
void         gtk_list_model_set_columns        (GtkListModel   *model,
						gint            columns);
void         gtk_list_model_set_column_type    (GtkListModel   *model,
						gint            column,
						GType           type);

GtkListNode *gtk_list_model_node_new           (void);
void         gtk_list_model_node_set_cell      (GtkListModel   *model,
						GtkListNode    *node,
						gint            column,
						GValue         *value);
void         gtk_list_model_node_destroy       (GtkListModel   *model,
						GtkListNode    *node);
GtkListNode *gtk_list_model_node_insert        (GtkListModel   *model,
						gint            position,
						GtkListNode    *node);
GtkListNode *gtk_list_model_node_insert_before (GtkListModel   *model,
						GtkListNode    *sibling,
						GtkListNode    *node);
GtkListNode *gtk_list_model_node_prepend       (GtkListModel   *model,
						GtkListNode    *node);
GtkListNode *gtk_list_model_node_append        (GtkListModel   *model,
						GtkListNode    *node);
GtkListNode *gtk_list_model_node_find          (GtkListModel   *model,
						GtkListNode    *root,
						GTraverseType   order,
						GTraverseFlags  flags,
						gpointer        data);



#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_LIST_MODEL_H__ */
