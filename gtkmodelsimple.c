/* gtkmodelsimple.c
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include "gtkmodelsimple.h"
#include <gtk/gtkmarshal.h>

enum {
  GET_COLUMNS,
  GET_NODE,
  GET_PATH,
  NODE_GET_VALUE,
  NODE_COPY,
  NODE_NEXT,
  NODE_CHILDREN,
  NODE_HAS_CHILD,
  NODE_N_CHILDREN,
  NODE_NTH_CHILD,
  NODE_PARENT,
  LAST_SIGNAL
};

static void       gtk_model_simple_init                 (GtkModelSimple      *model_simple);
static void       gtk_model_simple_class_init           (GtkModelSimpleClass *class);

static gint       gtk_real_model_simple_get_columns     (GtkTLModel          *tlmodel);
static GtkTLNode  gtk_real_model_simple_get_node        (GtkTLModel          *tlmodel,
							 GtkTLPath           *path);
static GtkTLPath *gtk_real_model_simple_get_path        (GtkTLModel          *tlmodel,
							 GtkTLNode            node);
static void       gtk_real_model_simple_node_get_value  (GtkTLModel          *tlmodel,
							 GtkTLNode            node,
							 gint                 column,
							 GValue              *value);
static gboolean   gtk_real_model_simple_node_next       (GtkTLModel          *tlmodel,
							 GtkTLNode           *node);
static GtkTLNode  gtk_real_model_simple_node_children   (GtkTLModel          *tlmodel,
							 GtkTLNode            node);
static gboolean   gtk_real_model_simple_node_has_child  (GtkTLModel          *tlmodel,
							 GtkTLNode            node);
static gint       gtk_real_model_simple_node_n_children (GtkTLModel          *tlmodel,
							 GtkTLNode            node);
static GtkTLNode  gtk_real_model_simple_node_nth_child  (GtkTLModel          *tlmodel,
							 GtkTLNode            node,
							 gint                 n);
static GtkTLNode  gtk_real_model_simple_node_parent     (GtkTLModel          *tlmodel,
							 GtkTLNode            node);


static GtkTLModelClass *parent_class = NULL;
static guint model_simple_signals[LAST_SIGNAL] = { 0 };


GtkType
gtk_model_simple_get_type (void)
{
  static GtkType model_simple_type = 0;

  if (!model_simple_type)
    {
      static const GTypeInfo model_simple_info =
      {
        sizeof (GtkModelSimpleClass),
	NULL,		/* base_init */
	NULL,		/* base_finalize */
        (GClassInitFunc) gtk_model_simple_class_init,
	NULL,		/* class_finalize */
	NULL,		/* class_data */
        sizeof (GtkModelSimple),
	0,
        (GInstanceInitFunc) gtk_model_simple_init
      };

      model_simple_type = g_type_register_static (GTK_TYPE_TLMODEL, "GtkModelSimple", &model_simple_info);
    }

  return model_simple_type;
}

GtkObject *
gtk_model_simple_new (void)
{
  return GTK_OBJECT (gtk_type_new (GTK_TYPE_MODEL_SIMPLE));
}


typedef gint (*GtkSignal_INT__NONE) (GtkObject * object,
				     gpointer user_data);
void
gtk_marshal_INT__NONE (GtkObject * object,
			GtkSignalFunc func, gpointer func_data, GtkArg * args)
{
  GtkSignal_INT__NONE rfunc;
  gint *return_val;
  return_val = GTK_RETLOC_INT (args[0]);
  rfunc = (GtkSignal_INT__NONE) func;
  *return_val = (*rfunc) (object, func_data);
}

typedef gpointer (*GtkSignal_POINTER__NONE) (GtkObject * object,
					 gpointer user_data);
void
gtk_marshal_POINTER__NONE (GtkObject * object,
			   GtkSignalFunc func, gpointer func_data, GtkArg * args)
{
  GtkSignal_POINTER__NONE rfunc;
  gpointer *return_val;
  return_val = GTK_RETLOC_POINTER (args[0]);
  rfunc = (GtkSignal_POINTER__NONE) func;
  *return_val = (*rfunc) (object, func_data);
}

typedef gpointer (*GtkSignal_POINTER__POINTER) (GtkObject * object,
						gpointer arg1,
						gpointer user_data);
void
gtk_marshal_POINTER__POINTER (GtkObject * object,
			      GtkSignalFunc func,
			      gpointer func_data, GtkArg * args)
{
  GtkSignal_POINTER__POINTER rfunc;
  gpointer *return_val;
  return_val = GTK_RETLOC_POINTER (args[1]);
  rfunc = (GtkSignal_POINTER__POINTER) func;
  *return_val = (*rfunc) (object, GTK_VALUE_POINTER (args[0]), func_data);
}

typedef gpointer (*GtkSignal_POINTER__POINTER_INT) (GtkObject * object,
						    gpointer arg1,
						    gint arg2,
						    gpointer user_data);
void
gtk_marshal_POINTER__POINTER_INT (GtkObject * object,
				  GtkSignalFunc func,
				  gpointer func_data, GtkArg * args)
{
  GtkSignal_POINTER__POINTER_INT rfunc;
  gpointer *return_val;
  return_val = GTK_RETLOC_POINTER (args[2]);
  rfunc = (GtkSignal_POINTER__POINTER_INT) func;
  *return_val = (*rfunc) (object, GTK_VALUE_POINTER (args[0]), GTK_VALUE_INT (args[1]), func_data);
}

typedef void (*GtkSignal_NONE__POINTER_INT_POINTER) (GtkObject * object,
						     gpointer arg1,
						     gint arg2,
						     gpointer arg3,
						     gpointer user_data);

void
gtk_marshal_NONE__POINTER_INT_POINTER (GtkObject * object,
				       GtkSignalFunc func,
				       gpointer func_data, GtkArg * args)
{
  GtkSignal_NONE__POINTER_INT_POINTER rfunc;

  rfunc = (GtkSignal_NONE__POINTER_INT_POINTER) func;
  (*rfunc) (object, GTK_VALUE_POINTER (args[0]), GTK_VALUE_INT (args[1]), GTK_VALUE_POINTER (args[2]), func_data);
}

static void
gtk_model_simple_class_init (GtkModelSimpleClass *class)
{
  GtkObjectClass *object_class;

  GtkTLModelClass *tlmodel_class;

  object_class = (GtkObjectClass*) class;
  tlmodel_class = (GtkTLModelClass*) class;
  parent_class = g_type_class_peek_parent (class);


  model_simple_signals[GET_COLUMNS] = 
    gtk_signal_new ("get_columns",
                    GTK_RUN_LAST,
                    GTK_CLASS_TYPE (object_class),
                    0,
                    gtk_marshal_INT__NONE,
                    GTK_TYPE_INT, 0);
  model_simple_signals[GET_NODE] = 
    gtk_signal_new ("get_node",
                    GTK_RUN_LAST,
                    GTK_CLASS_TYPE (object_class),
                    0,
                    gtk_marshal_POINTER__POINTER,
                    GTK_TYPE_POINTER, 1,
		    GTK_TYPE_POINTER);
  model_simple_signals[GET_PATH] = 
    gtk_signal_new ("get_path",
                    GTK_RUN_LAST,
                    GTK_CLASS_TYPE (object_class),
                    0,
                    gtk_marshal_POINTER__POINTER,
                    GTK_TYPE_POINTER, 1,
		    GTK_TYPE_POINTER);
  model_simple_signals[NODE_GET_VALUE] = 
    gtk_signal_new ("node_get_value",
                    GTK_RUN_LAST,
                    GTK_CLASS_TYPE (object_class),
                    0,
                    gtk_marshal_NONE__POINTER_INT_POINTER,
                    GTK_TYPE_NONE, 3,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_INT,
		    GTK_TYPE_POINTER);
  model_simple_signals[NODE_NEXT] = 
    gtk_signal_new ("node_next",
                    GTK_RUN_LAST,
                    GTK_CLASS_TYPE (object_class),
                    0,
                    gtk_marshal_BOOL__POINTER,
                    GTK_TYPE_BOOL, 1,
		    GTK_TYPE_POINTER);
  model_simple_signals[NODE_CHILDREN] = 
    gtk_signal_new ("node_children",
                    GTK_RUN_LAST,
                    GTK_CLASS_TYPE (object_class),
                    0,
                    gtk_marshal_POINTER__POINTER,
                    GTK_TYPE_POINTER, 1,
		    GTK_TYPE_POINTER);
  model_simple_signals[NODE_HAS_CHILD] = 
    gtk_signal_new ("node_has_child",
                    GTK_RUN_LAST,
                    GTK_CLASS_TYPE (object_class),
                    0,
                    gtk_marshal_POINTER__POINTER,
                    GTK_TYPE_POINTER, 1,
		    GTK_TYPE_POINTER);
  model_simple_signals[NODE_N_CHILDREN] = 
    gtk_signal_new ("node_n_children",
                    GTK_RUN_LAST,
                    GTK_CLASS_TYPE (object_class),
                    0,
                    gtk_marshal_POINTER__POINTER,
                    GTK_TYPE_POINTER, 1,
		    GTK_TYPE_POINTER);
  model_simple_signals[NODE_NTH_CHILD] = 
    gtk_signal_new ("node_nth_child",
                    GTK_RUN_LAST,
                    GTK_CLASS_TYPE (object_class),
                    0,
                    gtk_marshal_POINTER__POINTER_INT,
                    GTK_TYPE_POINTER, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);
  model_simple_signals[NODE_PARENT] = 
    gtk_signal_new ("node_parent",
                    GTK_RUN_LAST,
                    GTK_CLASS_TYPE (object_class),
                    0,
                    gtk_marshal_POINTER__POINTER,
                    GTK_TYPE_POINTER, 1,
		    GTK_TYPE_POINTER);


  tlmodel_class->get_columns = gtk_real_model_simple_get_columns;
  tlmodel_class->get_node = gtk_real_model_simple_get_node;
  tlmodel_class->get_path = gtk_real_model_simple_get_path;
  tlmodel_class->node_get_value = gtk_real_model_simple_node_get_value;
  tlmodel_class->node_next = gtk_real_model_simple_node_next;
  tlmodel_class->node_children = gtk_real_model_simple_node_children;
  tlmodel_class->node_has_child = gtk_real_model_simple_node_has_child;
  tlmodel_class->node_n_children = gtk_real_model_simple_node_n_children;
  tlmodel_class->node_nth_child = gtk_real_model_simple_node_nth_child;
  tlmodel_class->node_parent = gtk_real_model_simple_node_parent;

  gtk_object_class_add_signals (object_class, model_simple_signals, LAST_SIGNAL);
}


static void
gtk_model_simple_init (GtkModelSimple *model_simple)
{
}

static gint
gtk_real_model_simple_get_columns (GtkTLModel *tlmodel)
{
  gint retval = 0;

  gtk_signal_emit (GTK_OBJECT (tlmodel), model_simple_signals[GET_COLUMNS], &retval);

  return retval;
}

static GtkTLNode
gtk_real_model_simple_get_node (GtkTLModel *tlmodel,
				GtkTLPath  *path)
{
  GtkTLNode retval;

  gtk_signal_emit (GTK_OBJECT (tlmodel), model_simple_signals[GET_NODE], path, &retval);

  return retval;
}

static GtkTLPath *
gtk_real_model_simple_get_path (GtkTLModel *tlmodel,
				GtkTLNode   node)
{
  GtkTLPath *retval;

  gtk_signal_emit (GTK_OBJECT (tlmodel), model_simple_signals[GET_PATH], node, &retval);

  return retval;
}

static void
gtk_real_model_simple_node_get_value (GtkTLModel *tlmodel,
				      GtkTLNode   node,
				      gint        column,
				      GValue     *value)
{
  gtk_signal_emit (GTK_OBJECT (tlmodel), model_simple_signals[NODE_GET_VALUE], node, column, value);
}

static gboolean
gtk_real_model_simple_node_next (GtkTLModel  *tlmodel,
				 GtkTLNode   *node)
{
  gboolean retval = FALSE;

  gtk_signal_emit (GTK_OBJECT (tlmodel), model_simple_signals[NODE_NEXT], node, &retval);

  return retval;
}

static GtkTLNode
gtk_real_model_simple_node_children (GtkTLModel *tlmodel,
				     GtkTLNode   node)
{
  GtkTLNode retval = NULL;

  gtk_signal_emit (GTK_OBJECT (tlmodel), model_simple_signals[NODE_CHILDREN], node, &retval);

  return retval;
}

static gboolean
gtk_real_model_simple_node_has_child (GtkTLModel *tlmodel,
				      GtkTLNode   node)
{
  gboolean retval = FALSE;

  gtk_signal_emit (GTK_OBJECT (tlmodel), model_simple_signals[NODE_HAS_CHILD], node, &retval);

  return retval;
}

static gint
gtk_real_model_simple_node_n_children (GtkTLModel *tlmodel,
				       GtkTLNode   node)
{
  gint retval = 0;

  gtk_signal_emit (GTK_OBJECT (tlmodel), model_simple_signals[NODE_N_CHILDREN], node, &retval);

  return retval;
}

static GtkTLNode
gtk_real_model_simple_node_nth_child (GtkTLModel *tlmodel,
				      GtkTLNode   node,
				      gint        n)
{
  GtkTLNode retval = NULL;

  gtk_signal_emit (GTK_OBJECT (tlmodel), model_simple_signals[NODE_NTH_CHILD], node, n, &retval);

  return retval;
}

static GtkTLNode
gtk_real_model_simple_node_parent (GtkTLModel *tlmodel,
				   GtkTLNode   node)
{
  GtkTLNode retval = NULL;

  gtk_signal_emit (GTK_OBJECT (tlmodel), model_simple_signals[NODE_PARENT], node, &retval);

  return retval;
}

void
gtk_model_simple_node_changed (GtkModelSimple *simple,
			       GtkTLPath      *path,
			       GtkTLNode      *tlnode)
{
  g_return_if_fail (simple != NULL);
  g_return_if_fail (GTK_IS_MODEL_SIMPLE (simple));
  g_return_if_fail (path != NULL);

  gtk_signal_emit_by_name (GTK_OBJECT (simple), "node_changed", path, tlnode);
}
