/* gtktlcell.h
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_TLCELL_H__
#define __GTK_TLCELL_H__

#include "gtk/gtk.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

typedef enum
{
  GTK_TLCELL_SELECTED = 1 << 0,
  GTK_TLCELL_PRELIT = 1 << 1,
  GTK_TLCELL_INSENSITIVE = 1 << 2
} GtkTLCellType;

#define GTK_TYPE_TLCELL			(gtk_tlcell_get_type ())
#define GTK_TLCELL(obj)			(GTK_CHECK_CAST ((obj), GTK_TYPE_TLCELL, GtkTLCell))
#define GTK_TLCELL_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_TLCELL, GtkTLCellClass))
#define GTK_IS_TLCELL(obj)		(GTK_CHECK_TYPE ((obj), GTK_TYPE_TLCELL))
#define GTK_IS_TLCELL_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_TLCELL))
#define GTK_TLCELL_GET_CLASS(obj)       (GTK_CHECK_GET_CLASS ((obj), GTK_TYPE_TLCELL, GtkTLCellClass))

typedef struct _GtkTLCell GtkTLCell;
typedef struct _GtkTLCellClass GtkTLCellClass;

struct _GtkTLCell
{
  GtkObject parent;

  gfloat xalign;
  gfloat yalign;
  
  guint16 xpad;
  guint16 ypad;

};

struct _GtkTLCellClass
{
  GtkObjectClass parent_class;

  /* vtable - not signals */
  void (* get_size) (GtkTLCell    *cell,
		     GtkWidget    *widget,
		     gint         *width,
		     gint         *height);
  void (* render)   (GtkTLCell    *cell,
		     GdkWindow    *window,
		     GtkWidget    *widget,
		     GdkRectangle *background_area,
		     GdkRectangle *cell_area,
		     GdkRectangle *expose_area,
		     guint         flags);
		   
  gint (* event)    (GtkTLCell    *cell,
		     GdkEvent     *event,
		     GtkWidget    *widget,
		     gchar        *path,
		     GdkRectangle *background_area,
		     GdkRectangle *cell_area,
		     guint         flags);
};

GtkType gtk_tlcell_get_type (void);
void    gtk_tlcell_get_size (GtkTLCell    *cell,
			     GtkWidget    *widget,
			     gint         *width,
			     gint         *height);
void    gtk_tlcell_render   (GtkTLCell    *cell,
			     GdkWindow    *window,
			     GtkWidget    *widget,
			     GdkRectangle *background_area,
			     GdkRectangle *cell_area,
			     GdkRectangle *expose_area,
			     guint         flags);
gint    gtk_tlcell_event    (GtkTLCell    *cell,
			     GdkEvent     *event,
			     GtkWidget    *widget,
			     gchar        *path,
			     GdkRectangle *background_area,
			     GdkRectangle *cell_area,
			     guint         flags);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_TLCELL_H__ */
