/* gtktlcellpixbuf.c
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include "gtktlcellpixbuf.h"

#ifndef _
#define _(x) x
#endif

static void gtk_tlcell_pixbuf_get_param  (GObject              *object,
					  guint                 param_id,
					  GValue               *value,
					  GParamSpec           *pspec,
					  const gchar          *trailer);
static void gtk_tlcell_pixbuf_set_param  (GObject              *object,
					  guint                 param_id,
					  GValue               *value,
					  GParamSpec           *pspec,
					  const gchar          *trailer);
static void gtk_tlcell_pixbuf_init       (GtkTLCellPixbuf      *tlcelltext);
static void gtk_tlcell_pixbuf_class_init (GtkTLCellPixbufClass *class);
static void gtk_tlcell_pixbuf_get_size   (GtkTLCell            *tlcell,
					  GtkWidget            *tlview,
					  gint                 *width,
					  gint                 *height);
static void gtk_tlcell_pixbuf_render     (GtkTLCell            *tlcell,
					  GdkWindow            *window,
					  GtkWidget            *view,
					  GdkRectangle         *background_area,
					  GdkRectangle         *cell_area,
					  GdkRectangle         *expose_area,
					  guint                 flags);


enum {
  PROP_ZERO,
  PROP_PIXBUF
};


GtkType
gtk_tlcell_pixbuf_get_type (void)
{
  static GtkType tlcell_pixbuf_type = 0;

  if (!tlcell_pixbuf_type)
    {
      static const GTypeInfo tlcell_pixbuf_info =
      {
        sizeof (GtkTLCellPixbufClass),
	NULL,		/* base_init */
	NULL,		/* base_finalize */
        (GClassInitFunc) gtk_tlcell_pixbuf_class_init,
	NULL,		/* class_finalize */
	NULL,		/* class_data */
        sizeof (GtkTLCellPixbuf),
	0,              /* n_preallocs */
        (GInstanceInitFunc) gtk_tlcell_pixbuf_init,
      };

      tlcell_pixbuf_type = g_type_register_static (GTK_TYPE_TLCELL, "GtkTLCellPixbuf", &tlcell_pixbuf_info);
    }

  return tlcell_pixbuf_type;
}

static void
gtk_tlcell_pixbuf_init (GtkTLCellPixbuf *tlcellpixbuf)
{
}

static void
gtk_tlcell_pixbuf_class_init (GtkTLCellPixbufClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  GtkTLCellClass *tlcell_class = GTK_TLCELL_CLASS (class);

  object_class->get_param = gtk_tlcell_pixbuf_get_param;
  object_class->set_param = gtk_tlcell_pixbuf_set_param;

  tlcell_class->get_size = gtk_tlcell_pixbuf_get_size;
  tlcell_class->render = gtk_tlcell_pixbuf_render;

  g_object_class_install_param (object_class,
 				PROP_PIXBUF,
				g_param_spec_object ("pixbuf",
						     _("Pixbuf Object"),
						     _("The pixbuf to render."),
						     GDK_TYPE_PIXBUF,
						     G_PARAM_READABLE |
						     G_PARAM_WRITABLE));
}

static void
gtk_tlcell_pixbuf_get_param (GObject        *object,
			     guint           param_id,
			     GValue         *value,
			     GParamSpec     *pspec,
			     const gchar    *trailer)
{
  GtkTLCellPixbuf *cellpixbuf = GTK_TLCELL_PIXBUF (object);

  switch (param_id)
    {
    case PROP_PIXBUF:
      g_value_init (value, G_TYPE_OBJECT);
      g_value_set_object (value, G_OBJECT (cellpixbuf->pixbuf));
      break;
    default:
      break;
    }
}


static void
gtk_tlcell_pixbuf_set_param (GObject     *object,
			     guint        param_id,
			     GValue      *value,
			     GParamSpec  *pspec,
			     const gchar *trailer)
{
  GdkPixbuf *pixbuf;
  GtkTLCellPixbuf *tlcellpixbuf = GTK_TLCELL_PIXBUF (object);

  switch (param_id)
    {
    case PROP_PIXBUF:
      pixbuf = GDK_PIXBUF (g_value_get_object (value));
      g_object_ref (G_OBJECT (pixbuf));
      if (tlcellpixbuf->pixbuf)
	g_object_unref (G_OBJECT (tlcellpixbuf->pixbuf));
      tlcellpixbuf->pixbuf = pixbuf;
      break;
    default:
      break;
    }
}

GtkTLCell *
gtk_tlcell_pixbuf_new (void)
{
  return GTK_TLCELL (gtk_type_new (gtk_tlcell_pixbuf_get_type ()));
}

static void
gtk_tlcell_pixbuf_get_size (GtkTLCell *tlcell,
			    GtkWidget *tlview,
			    gint      *width,
			    gint      *height)
{
  GtkTLCellPixbuf *tlcellpixbuf = (GtkTLCellPixbuf *) tlcell;

  if (width)
    *width = (gint) GTK_TLCELL (tlcellpixbuf)->xpad * 2 + (tlcellpixbuf->pixbuf ? gdk_pixbuf_get_width (tlcellpixbuf->pixbuf) : 0);

  if (height)
    *height = (gint) GTK_TLCELL (tlcellpixbuf)->ypad * 2 + (tlcellpixbuf->pixbuf ? gdk_pixbuf_get_height (tlcellpixbuf->pixbuf) : 0);
}

static void
gtk_tlcell_pixbuf_render (GtkTLCell    *tlcell,
			  GdkWindow    *window,
			  GtkWidget    *view,
			  GdkRectangle *background_area,  
			  GdkRectangle *cell_area,
			  GdkRectangle *expose_area,
			  guint         flags)

{
  GtkTLCellPixbuf *tlcellpixbuf = (GtkTLCellPixbuf *) tlcell;
  GdkPixbuf *pixbuf;
  guchar *pixels;
  gint rowstride;
  gint real_xoffset;
  gint real_yoffset;
  GdkGC *bg_gc = NULL;

  pixbuf = tlcellpixbuf->pixbuf;

  if (!pixbuf)
    return;

  if ((flags & GTK_TLCELL_SELECTED) == GTK_TLCELL_SELECTED)
    bg_gc = view->style->bg_gc [GTK_STATE_SELECTED];
  else
    bg_gc = view->style->base_gc [GTK_STATE_NORMAL];

  gdk_gc_set_clip_rectangle (bg_gc, cell_area);

  rowstride = gdk_pixbuf_get_rowstride (pixbuf);
  pixels = gdk_pixbuf_get_pixels (pixbuf);

  real_xoffset = GTK_TLCELL (tlcellpixbuf)->xalign * (cell_area->width - gdk_pixbuf_get_width (pixbuf) - (2 * GTK_TLCELL (tlcellpixbuf)->xpad));
  real_xoffset = MAX (real_xoffset, 0) + GTK_TLCELL (tlcellpixbuf)->xpad;
  real_yoffset = GTK_TLCELL (tlcellpixbuf)->yalign * (cell_area->height - gdk_pixbuf_get_height (pixbuf) - (2 * GTK_TLCELL (tlcellpixbuf)->ypad));
  real_yoffset = MAX (real_yoffset, 0) + GTK_TLCELL (tlcellpixbuf)->ypad;

  if (gdk_pixbuf_get_has_alpha (pixbuf))
    gdk_draw_rgb_32_image (window,
			   bg_gc,
			   cell_area->x + real_xoffset,
			   cell_area->y + real_yoffset,
			   gdk_pixbuf_get_width (pixbuf),
			   gdk_pixbuf_get_height (pixbuf),
			   GDK_RGB_DITHER_NORMAL,
			   pixels,
			   rowstride);
  else
    gdk_draw_rgb_image (window,
			bg_gc,
			cell_area->x + real_xoffset,
			cell_area->y + real_yoffset,
			gdk_pixbuf_get_width (pixbuf),
			gdk_pixbuf_get_height (pixbuf),
			GDK_RGB_DITHER_NORMAL,
			pixels,
			rowstride);

  gdk_gc_set_clip_rectangle (bg_gc, NULL);
}
