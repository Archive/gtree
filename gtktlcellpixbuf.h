/* gtktlcellpixbuf.h
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_TLCELL_PIXBUF_H__
#define __GTK_TLCELL_PIXBUF_H__

#include <gtk/gtk.h>
#include "gtktlcell.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */


#define GTK_TYPE_TLCELL_PIXBUF		(gtk_tlcell_pixbuf_get_type ())
#define GTK_TLCELL_PIXBUF(obj)		(GTK_CHECK_CAST ((obj), GTK_TYPE_TLCELL_PIXBUF, GtkTLCellPixbuf))
#define GTK_TLCELL_PIXBUF_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_TLCELL_PIXBUF, GtkTLCellPixbufClass))
#define GTK_IS_TLCELL_PIXBUF(obj)		(GTK_CHECK_TYPE ((obj), GTK_TYPE_TLCELL_PIXBUF))
#define GTK_IS_TLCELL_PIXBUF_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_TLCELL_PIXBUF))

typedef struct _GtkTLCellPixbuf GtkTLCellPixbuf;
typedef struct _GtkTLCellPixbufClass GtkTLCellPixbufClass;

struct _GtkTLCellPixbuf
{
  GtkTLCell parent;

  GdkPixbuf *pixbuf;
};

struct _GtkTLCellPixbufClass
{
  GtkTLCellClass parent_class;
};

GtkType    gtk_tlcell_pixbuf_get_type (void);
GtkTLCell *gtk_tlcell_pixbuf_new      (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_TLCELL_PIXBUF_H__ */
