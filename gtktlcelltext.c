/* gtktlcelltext.c
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include "gtktlcelltext.h"

#ifndef _
#define _(x) x
#endif

static void gtk_tlcell_text_init       (GtkTLCellText      *tlcelltext);
static void gtk_tlcell_text_class_init (GtkTLCellTextClass *class);

static void gtk_tlcell_text_get_param  (GObject            *object,
					guint               param_id,
					GValue             *value,
					GParamSpec         *pspec,
					const gchar        *trailer);
static void gtk_tlcell_text_set_param  (GObject            *object,
					guint               param_id,
					GValue             *value,
					GParamSpec         *pspec,
					const gchar        *trailer);

static void gtk_tlcell_text_get_size   (GtkTLCell          *tlcell,
					GtkWidget          *view,
					gint               *width,
					gint               *height);
static void gtk_tlcell_text_render     (GtkTLCell          *tlcell,
					GdkWindow          *window,
					GtkWidget          *view,
					GdkRectangle       *background_area,
					GdkRectangle       *cell_area,
					GdkRectangle       *expose_area,
					guint               flags);

enum {
  PROP_ZERO,
  PROP_TEXT,
  PROP_FONT,
  PROP_BACKGROUND,
  PROP_BACKGROUND_GDK,
  PROP_FOREGROUND,
  PROP_FOREGROUND_GDK,
  PROP_STRIKETHROUGH,
  PROP_UNDERLINE,
  PROP_EDITABLE,
  PROP_ITALIC,
  PROP_BOLD
};

GtkType
gtk_tlcell_text_get_type (void)
{
  static GtkType tlcell_text_type = 0;

  if (!tlcell_text_type)
    {
      static const GTypeInfo tlcell_text_info =
      {
        sizeof (GtkTLCellTextClass),
	NULL,		/* base_init */
	NULL,		/* base_finalize */
        (GClassInitFunc) gtk_tlcell_text_class_init,
	NULL,		/* class_finalize */
	NULL,		/* class_data */
        sizeof (GtkTLCellText),
	0,              /* n_preallocs */
        (GInstanceInitFunc) gtk_tlcell_text_init,
      };

      tlcell_text_type = g_type_register_static (GTK_TYPE_TLCELL, "GtkTLCellText", &tlcell_text_info);
    }

  return tlcell_text_type;
}

static void
gtk_tlcell_text_init (GtkTLCellText *tlcelltext)
{
  tlcelltext->attr_list = pango_attr_list_new ();
  GTK_TLCELL (tlcelltext)->xalign = 0.0;
  GTK_TLCELL (tlcelltext)->yalign = 0.5;
  GTK_TLCELL (tlcelltext)->xpad = 2;
  GTK_TLCELL (tlcelltext)->ypad = 2;
  tlcelltext->underline = FALSE;
}

static void
gtk_tlcell_text_class_init (GtkTLCellTextClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  GtkTLCellClass *tlcell_class = GTK_TLCELL_CLASS (class);

  object_class->get_param = gtk_tlcell_text_get_param;
  object_class->set_param = gtk_tlcell_text_set_param;

  tlcell_class->get_size = gtk_tlcell_text_get_size;
  tlcell_class->render = gtk_tlcell_text_render;

  g_object_class_install_param (object_class,
 				PROP_TEXT,
				g_param_spec_string ("text",
						     _("Text String"),
						     _("The text of the renderer."),
						     "",
						     G_PARAM_READABLE |
						     G_PARAM_WRITABLE));

  g_object_class_install_param (object_class,
 				PROP_FONT,
				g_param_spec_string ("font",
						     _("Font String"),
						     _("The string of the font."),
						     "",
						     G_PARAM_WRITABLE));

  g_object_class_install_param (object_class,
 				PROP_BACKGROUND,
				g_param_spec_string ("background",
						     _("Background Color string"),
						     _("The color for the background of the text."),
						     "white",
						     G_PARAM_WRITABLE));

  g_object_class_install_param (object_class,
 				PROP_FOREGROUND,
				g_param_spec_string ("foreground",
						     _("Foreground Color string"),
						     _("The color for the background of the text."),
						     "black",
						     G_PARAM_WRITABLE));

  g_object_class_install_param (object_class,
 				PROP_STRIKETHROUGH,
				g_param_spec_boolean ("strikethrough",
						      _("Strikethrough"),
						      _("Draw a line through the text."),
						      FALSE,
						      G_PARAM_READABLE |
						      G_PARAM_WRITABLE));

  g_object_class_install_param (object_class,
 				PROP_UNDERLINE,
				g_param_spec_boolean ("underline",
						      _("Underline"),
						      _("Underline the text."),
						      FALSE,
						      G_PARAM_READABLE |
						      G_PARAM_WRITABLE));

  g_object_class_install_param (object_class,
 				PROP_EDITABLE,
				g_param_spec_boolean ("editable",
						      _("Editable"),
						      _("Make the text editable."),
						      FALSE,
						      G_PARAM_READABLE |
						      G_PARAM_WRITABLE));

  g_object_class_install_param (object_class,
 				PROP_ITALIC,
				g_param_spec_boolean ("italic",
						      _("Italic"),
						      _("Make the text italic."),
						      FALSE,
						      G_PARAM_WRITABLE));

  g_object_class_install_param (object_class,
 				PROP_BOLD,
				g_param_spec_boolean ("bold",
						      _("Bold"),
						      _("Make the text bold."),
						      FALSE,
						      G_PARAM_WRITABLE));
}

static void
gtk_tlcell_text_get_param (GObject        *object,
			   guint           param_id,
			   GValue         *value,
			   GParamSpec     *pspec,
			   const gchar    *trailer)
{
  GtkTLCellText *celltext = GTK_TLCELL_TEXT (object);
  PangoAttrIterator *attr_iter;
  PangoAttribute *attr;

  switch (param_id)
    {
    case PROP_TEXT:
      g_value_init (value, G_TYPE_STRING);
      g_value_set_string (value, celltext->text);
      break;
    case PROP_STRIKETHROUGH:
      g_value_init (value, G_TYPE_BOOLEAN);
      attr_iter = pango_attr_list_get_iterator (celltext->attr_list);
      attr = pango_attr_iterator_get (attr_iter,
				      PANGO_ATTR_STRIKETHROUGH);
      g_value_set_boolean (value, ((PangoAttrInt*) attr)->value);
      break;
    case PROP_UNDERLINE:
      g_value_init (value, G_TYPE_BOOLEAN);
      g_value_set_boolean (value, celltext->underline);
      break;
    case PROP_EDITABLE:
      g_value_init (value, G_TYPE_BOOLEAN);
      g_value_set_boolean (value, celltext->editable);
      break;
    default:
      break;
    }
}


static void
gtk_tlcell_text_set_param (GObject     *object,
			   guint        param_id,
			   GValue      *value,
			   GParamSpec  *pspec,
			   const gchar *trailer)
{
  GtkTLCellText *celltext = GTK_TLCELL_TEXT (object);

  GdkColor color;
  PangoFontDescription *font_desc;
  gchar *string;
  PangoAttribute *attribute;
  gchar *font;

  switch (param_id)
    {
    case PROP_TEXT:
      g_free (celltext->text);
      celltext->text = g_value_dup_string (value);
      break;
    case PROP_FONT:
      font = g_value_get_string (value);

      if (font)
	{
	  font_desc = pango_font_description_from_string (font);
	  attribute = pango_attr_font_desc_new (font_desc);
	  attribute->start_index = 0;
	  attribute->end_index = G_MAXINT;
	  pango_font_description_free (font_desc);
	  pango_attr_list_change (celltext->attr_list,
				  attribute);
	}
      break;
    case PROP_BACKGROUND:
      string = g_value_get_string (value);
      if (string && gdk_color_parse (string, &color))
	{
	  attribute = pango_attr_background_new (color.red,
						 color.green,
						 color.blue);
	  attribute->start_index = 0;
	  attribute->end_index = G_MAXINT;
	  pango_attr_list_change (celltext->attr_list,
				  attribute);
	}
      break;
    case PROP_BACKGROUND_GDK:
      break;
    case PROP_FOREGROUND:
      string = g_value_get_string (value);
      if (string && gdk_color_parse (string, &color))
	{
	  attribute = pango_attr_foreground_new (color.red,
						 color.green,
						 color.blue);
	  attribute->start_index = 0;
	  attribute->end_index = G_MAXINT;
	  pango_attr_list_change (celltext->attr_list,
				  attribute);
	}
      break;
    case PROP_FOREGROUND_GDK:
      break;
    case PROP_STRIKETHROUGH:
      attribute = pango_attr_strikethrough_new (g_value_get_boolean (value));
      attribute->start_index = 0;
      attribute->end_index = G_MAXINT;
      pango_attr_list_change (celltext->attr_list,
			      attribute);
      break;
    case PROP_UNDERLINE:
      celltext->underline = g_value_get_boolean (value);
      attribute = pango_attr_underline_new (celltext->underline ?
					    PANGO_UNDERLINE_SINGLE :
					    PANGO_UNDERLINE_NONE);
      attribute->start_index = 0;
      attribute->end_index = G_MAXINT;
      pango_attr_list_change (celltext->attr_list,
			      attribute);
      break;
    case PROP_EDITABLE:
      break;
    case PROP_ITALIC:
      attribute = pango_attr_style_new (g_value_get_boolean (value) ?
					PANGO_STYLE_ITALIC :
					PANGO_STYLE_NORMAL);
      attribute->start_index = 0;
      attribute->end_index = G_MAXINT;
      pango_attr_list_change (celltext->attr_list,
			      attribute);
      break;
    case PROP_BOLD:
      attribute = pango_attr_weight_new (g_value_get_boolean (value) ?
					 PANGO_WEIGHT_BOLD :
					 PANGO_WEIGHT_NORMAL);
      attribute->start_index = 0;
      attribute->end_index = G_MAXINT;
      pango_attr_list_change (celltext->attr_list,
			      attribute);
      break;
    default:
      break;
    }
}

GtkTLCell *
gtk_tlcell_text_new (void)
{
  return GTK_TLCELL (gtk_type_new (gtk_tlcell_text_get_type ()));
}

static void
gtk_tlcell_text_get_size (GtkTLCell *tlcell,
			  GtkWidget *view,
			  gint      *width,
			  gint      *height)
{
  GtkTLCellText *tlcelltext = (GtkTLCellText *)tlcell;
  PangoRectangle rect;
  PangoLayout *layout;
  PangoAttribute *attr;
  PangoUnderline underline;

  layout = gtk_widget_create_pango_layout (view, tlcelltext->text);
  underline = tlcelltext->underline ?
	  PANGO_UNDERLINE_DOUBLE :
	  PANGO_UNDERLINE_NONE;

  attr = pango_attr_underline_new (underline);
  attr->start_index = 0;
  attr->end_index = G_MAXINT;

  pango_attr_list_change (tlcelltext->attr_list, attr);
  pango_layout_set_attributes (layout, tlcelltext->attr_list);
  pango_layout_set_width (layout, -1);
  pango_layout_get_pixel_extents (layout, NULL, &rect);

  if (width)
    *width = GTK_TLCELL (tlcelltext)->xpad * 2 + rect.width;

  if (height)
    *height = GTK_TLCELL (tlcelltext)->ypad * 2 + rect.height;

  g_object_unref (G_OBJECT (layout));
}

static void
gtk_tlcell_text_render (GtkTLCell    *tlcell,
			GdkWindow    *window,
			GtkWidget    *view,
			GdkRectangle *background_area,
			GdkRectangle *cell_area,
			GdkRectangle *expose_area,
			guint         flags)

{
  GtkTLCellText *tlcelltext = (GtkTLCellText *) tlcell;
  PangoRectangle rect;
  PangoLayout *layout;
  PangoAttribute *attr;
  PangoUnderline underline;

  gint real_xoffset;
  gint real_yoffset;

  GdkGC *font_gc = NULL;

  if ((flags & GTK_TLCELL_SELECTED) == GTK_TLCELL_SELECTED)
    font_gc = view->style->fg_gc [GTK_STATE_SELECTED];
  else
    font_gc = view->style->fg_gc [GTK_STATE_NORMAL];

  layout = gtk_widget_create_pango_layout (view, tlcelltext->text);

  if (tlcelltext->underline)
    underline = PANGO_UNDERLINE_SINGLE;
  else
    underline = PANGO_UNDERLINE_NONE;

  attr = pango_attr_underline_new (underline);
  attr->start_index = 0;
  attr->end_index = G_MAXINT;
  pango_attr_list_change (tlcelltext->attr_list, attr);
  pango_layout_set_attributes (layout, tlcelltext->attr_list);

  pango_layout_set_width (layout, -1);
  pango_layout_get_pixel_extents (layout, NULL, &rect);
  
  if ((flags & GTK_TLCELL_PRELIT) == GTK_TLCELL_PRELIT)
    underline = (tlcelltext->underline) ? PANGO_UNDERLINE_DOUBLE:PANGO_UNDERLINE_SINGLE;
  else
    underline = (tlcelltext->underline) ? PANGO_UNDERLINE_SINGLE:PANGO_UNDERLINE_NONE;

  attr = pango_attr_underline_new (underline);
  attr->start_index = 0;
  attr->end_index = G_MAXINT;
  pango_attr_list_change (tlcelltext->attr_list, attr);
  pango_layout_set_attributes (layout, tlcelltext->attr_list);
    
  gdk_gc_set_clip_rectangle (font_gc, cell_area);

  real_xoffset = tlcell->xalign * (cell_area->width - rect.width - (2 * tlcell->xpad));
  real_xoffset = MAX (real_xoffset, 0) + tlcell->xpad;
  real_yoffset = tlcell->yalign * (cell_area->height - rect.height - (2 * tlcell->ypad));
  real_yoffset = MAX (real_yoffset, 0) + tlcell->ypad;

  gdk_draw_layout (window,
		   font_gc,
		   cell_area->x + real_xoffset,
		   cell_area->y + real_yoffset,
		   layout);

  g_object_unref (G_OBJECT (layout));

  gdk_gc_set_clip_rectangle (font_gc, NULL);
}
