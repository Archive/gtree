/* gtktlcelltext.h
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_TLCELL_TEXT_H__
#define __GTK_TLCELL_TEXT_H__

#include <gtk/gtk.h>
#include "gtktlcell.h"
#include <pango/pango.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */


#define GTK_TYPE_TLCELL_TEXT		(gtk_tlcell_text_get_type ())
#define GTK_TLCELL_TEXT(obj)		(GTK_CHECK_CAST ((obj), GTK_TYPE_TLCELL_TEXT, GtkTLCellText))
#define GTK_TLCELL_TEXT_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_TLCELL_TEXT, GtkTLCellTextClass))
#define GTK_IS_TLCELL_TEXT(obj)		(GTK_CHECK_TYPE ((obj), GTK_TYPE_TLCELL_TEXT))
#define GTK_IS_TLCELL_TEXT_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_TLCELL_TEXT))

typedef struct _GtkTLCellText GtkTLCellText;
typedef struct _GtkTLCellTextClass GtkTLCellTextClass;

struct _GtkTLCellText
{
  GtkTLCell parent;
  
  gchar *text;

  PangoAttrList *attr_list;

  gint editable  : 1;
  gint underline : 1;
};

struct _GtkTLCellTextClass
{
  GtkTLCellClass parent_class;
};

GtkType    gtk_tlcell_text_get_type (void);
GtkTLCell *gtk_tlcell_text_new      (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_TLCELL_TEXT_H__ */
