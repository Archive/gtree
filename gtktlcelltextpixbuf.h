/* gtktlcellpixbuftext.h
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_TLCELL_TEXT_PIXBUF_H__
#define __GTK_TLCELL_TEXT_PIXBUF_H__

#include <gtk/gtk.h>
#include "gtktlcelltext.h"
#include "gtktlcellpixbuf.h"
#include <pango/pango.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define GTK_TYPE_TLCELL_TEXT_PIXBUF		(gtk_tlcell_text_pixbuf_get_type ())
#define GTK_TLCELL_TEXT_PIXBUF(obj)		(GTK_CHECK_CAST ((obj), GTK_TYPE_TLCELL_TEXT_PIXBUF, GtkTLCellTextPixbuf))
#define GTK_TLCELL_TEXT_PIXBUF_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_TLCELL_TEXT_PIXBUF, GtkTLCellTextPixbufClass))
#define GTK_IS_TLCELL_TEXT_PIXBUF(obj)		(GTK_CHECK_TYPE ((obj), GTK_TYPE_TLCELL_TEXT_PIXBUF))
#define GTK_IS_TLCELL_TEXT_PIXBUF_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_TLCELL_TEXT_PIXBUF))

typedef struct _GtkTLCellTextPixbuf GtkTLCellTextPixbuf;
typedef struct _GtkTLCellTextPixbufClass GtkTLCellTextPixbufClass;

struct _GtkTLCellTextPixbuf
{
  GtkTLCellText parent;

  /* Feeble attempt at multiple inheritance here */
  GtkTLCellPixbuf *pixbuf;
  GtkPositionType pixbuf_pos;
};

struct _GtkTLCellTextPixbufClass
{
  GtkTLCellTextClass parent_class;
};

GtkType    gtk_tlcell_text_pixbuf_get_type (void);
GtkTLCell *gtk_tlcell_text_pixbuf_new      (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_TLCELL_TEXT_PIXBUF_H__ */
