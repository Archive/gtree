/* gtk-tlcolumn.c
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gtktlcolumn.h"
#include "gtktlprivate.h"

enum {
  CLICKED,
  LAST_SIGNAL
};

static void gtk_tlcolumn_init            (GtkTLColumn      *tlcolumn);
static void gtk_tlcolumn_class_init      (GtkTLColumnClass *klass);
static void gtk_tlcolumn_set_attributesv (GtkTLColumn      *tlcolumn,
					  va_list           args);
static void gtk_real_tlcolumn_clicked    (GtkTLColumn      *tlcolumn);



static GtkObjectClass *parent_class = NULL;
static guint tlcolumn_signals[LAST_SIGNAL] = { 0 };

GtkType
gtk_tlcolumn_get_type (void)
{
  static GtkType tlcolumn_type = 0;

  if (!tlcolumn_type)
    {
      static const GTypeInfo tlcolumn_info =
      {
	sizeof (GtkTLColumnClass),
	NULL,		/* base_init */
	NULL,		/* base_finalize */
        (GClassInitFunc) gtk_tlcolumn_class_init,
	NULL,		/* class_finalize */
	NULL,		/* class_data */
        sizeof (GtkTLColumn),
	0,
        (GInstanceInitFunc) gtk_tlcolumn_init,
      };

      tlcolumn_type = g_type_register_static (GTK_TYPE_OBJECT, "GtkTLColumn", &tlcolumn_info);
    }

  return tlcolumn_type;
}

static void
gtk_tlcolumn_class_init (GtkTLColumnClass *klass)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) klass;

  parent_class = g_type_class_peek_parent (klass);

  tlcolumn_signals[CLICKED] = 
    gtk_signal_new ("clicked",
                    GTK_RUN_FIRST,
                    GTK_CLASS_TYPE (object_class),
                    GTK_SIGNAL_OFFSET (GtkTLColumnClass, clicked),
                    gtk_marshal_NONE__NONE,
                    GTK_TYPE_NONE, 0);


  gtk_object_class_add_signals (object_class, tlcolumn_signals, LAST_SIGNAL);


  klass->clicked = gtk_real_tlcolumn_clicked;
}


static void
gtk_tlcolumn_init (GtkTLColumn *tlcolumn)
{
  tlcolumn->button = NULL;
  tlcolumn->justification = GTK_JUSTIFY_LEFT;
  tlcolumn->size = 0;
  tlcolumn->min_width = -1;
  tlcolumn->max_width = -1;
  tlcolumn->cell = NULL;
  tlcolumn->attributes = NULL;
  tlcolumn->column_type = GTK_TLCOLUMN_AUTOSIZE;
  tlcolumn->visible = TRUE;
  tlcolumn->button_passive = FALSE;
  tlcolumn->dirty = TRUE;
}



/* used to make the buttons 'unclickable' */

static gint
gtk_tlview_passive_func (GtkWidget *widget,
			 GdkEvent  *event,
			 gpointer   data)
{
  g_return_val_if_fail (event != NULL, FALSE);

  switch (event->type)
    {
    case GDK_MOTION_NOTIFY:
    case GDK_BUTTON_PRESS:
    case GDK_2BUTTON_PRESS:
    case GDK_3BUTTON_PRESS:
    case GDK_BUTTON_RELEASE:
    case GDK_ENTER_NOTIFY:
    case GDK_LEAVE_NOTIFY:
      return TRUE;
    default:
      break;
    }
  return FALSE;
}


static void
gtk_real_tlcolumn_clicked (GtkTLColumn *tlcolumn)
{
  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));
  
}

GtkObject *
gtk_tlcolumn_new (void)
{
  GtkObject *retval;

  retval = GTK_OBJECT (gtk_type_new (GTK_TYPE_TLCOLUMN));

  return retval;
}

GtkObject *
gtk_tlcolumn_new_with_vals (gchar     *title,
			    GtkTLCell *cell,
			    ...)
{
  GtkObject *retval;
  va_list args;

  retval = gtk_tlcolumn_new ();

  gtk_tlcolumn_set_title (GTK_TLCOLUMN (retval), title);
  gtk_tlcolumn_set_cell (GTK_TLCOLUMN (retval), cell);

  va_start (args, cell);
  gtk_tlcolumn_set_attributesv (GTK_TLCOLUMN (retval),
				args);
  va_end (args);

  return retval;
}

void
gtk_tlcolumn_set_cell (GtkTLColumn *tlcolumn,
		       GtkTLCell   *cell)
{
  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));
  if (cell)
    g_return_if_fail (GTK_IS_TLCELL (cell));

  if (cell)
    g_object_ref (G_OBJECT (cell));

  if (tlcolumn->cell)
    g_object_unref (G_OBJECT (tlcolumn->cell));

  tlcolumn->cell = cell;
}

void
gtk_tlcolumn_set_attribute (GtkTLColumn *tlcolumn,
			    gchar       *attribute,
			    gint         column)
{
  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));

  tlcolumn->attributes = g_slist_prepend (tlcolumn->attributes, GINT_TO_POINTER (column));
  tlcolumn->attributes = g_slist_prepend (tlcolumn->attributes, g_strdup (attribute));
}

static void
gtk_tlcolumn_set_attributesv (GtkTLColumn *tlcolumn,
			      va_list      args)
{
  gchar *attribute;
  gint column;

  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));

  attribute = va_arg (args, gchar *);

  while (attribute != NULL)
    {
      column = va_arg (args, gint);
      gtk_tlcolumn_set_attribute (tlcolumn,
				  attribute,
				  column);
      attribute = va_arg (args, gchar *);
    }
}

void
gtk_tlcolumn_set_attributes (GtkTLColumn *tlcolumn,
			     ...)
{
  va_list args;

  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));

  va_start (args, tlcolumn);

  gtk_tlcolumn_set_attributesv (tlcolumn, args);

  va_end (args);
}

void
gtk_tlcolumn_set_cell_data (GtkTLColumn *tlcolumn,
			    GtkTLModel  *tlmodel,
			    GtkTLNode    tlnode)
{
  GSList *list;
  GValue value = { 0, };
  GObject *cell;

  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));
  g_return_if_fail (tlcolumn->cell != NULL);

  if (tlcolumn->func && (* tlcolumn->func) (tlcolumn, tlmodel, tlnode, tlcolumn->func_data))
    return;

  cell = (GObject *) tlcolumn->cell;
  list = tlcolumn->attributes;

  while (list && list->next)
    {
      gtk_tlmodel_node_get_value (tlmodel,
				  tlnode,
				  GPOINTER_TO_INT (list->next->data),
				  &value);
      g_object_set_param (cell, (gchar *)list->data, &value);
      g_value_unset (&value);
      list = list->next->next;
    }
}

/* Options for manipulating the columns */
void
gtk_tlcolumn_set_visible (GtkTLColumn *tlcolumn,
			  gboolean      visible)
{
  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));

  if (tlcolumn->visible == visible)
    return;

  tlcolumn->visible = visible;

  if (visible)
    {
      gtk_widget_show (tlcolumn->button);
      gdk_window_show (tlcolumn->window);
    }
  else
    {
      gtk_widget_hide (tlcolumn->button);
      gdk_window_hide (tlcolumn->window);
    }
  if (GTK_WIDGET_REALIZED (tlcolumn->tlview))
    {
      gtk_tlview_set_size (GTK_TLVIEW (tlcolumn->tlview), -1, -1);
      gtk_widget_queue_resize (tlcolumn->tlview);
    }
}

gboolean
gtk_tlcolumn_get_visible (GtkTLColumn *tlcolumn)
{
  g_return_val_if_fail (tlcolumn != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TLCOLUMN (tlcolumn), FALSE);

  return tlcolumn->visible;
}

void
gtk_tlcolumn_set_col_type (GtkTLColumn     *tlcolumn,
			   GtkTLColumnType  type)
{
  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));

  if (type == tlcolumn->column_type)
    return;

  tlcolumn->column_type = type;
  switch (type)
    {
    case GTK_TLCOLUMN_AUTOSIZE:
      tlcolumn->dirty = TRUE;
    case GTK_TLCOLUMN_FIXED:
      gdk_window_hide (tlcolumn->window);
      break;
    default:
      gdk_window_show (tlcolumn->window);
      gdk_window_raise (tlcolumn->window);
      break;
    }
  gtk_widget_queue_resize (tlcolumn->tlview);
}

gint
gtk_tlcolumn_get_col_type (GtkTLColumn *tlcolumn)
{
  g_return_val_if_fail (tlcolumn != NULL, 0);
  g_return_val_if_fail (GTK_IS_TLCOLUMN (tlcolumn), 0);

  return tlcolumn->column_type;
}


gint
gtk_tlcolumn_get_preferred_size (GtkTLColumn *tlcolumn)
{
  return 0;
}

gint
gtk_tlcolumn_get_size (GtkTLColumn *tlcolumn)
{
  g_return_val_if_fail (tlcolumn != NULL, 0);
  g_return_val_if_fail (GTK_IS_TLCOLUMN (tlcolumn), 0);

  return tlcolumn->size;
}

void
gtk_tlcolumn_set_size (GtkTLColumn *tlcolumn,
		       gint         size)
{
  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));
  g_return_if_fail (size > 0);

  if (tlcolumn->column_type == GTK_TLCOLUMN_AUTOSIZE ||
      tlcolumn->size == size)
    return;

  tlcolumn->size = size;

  if (GTK_WIDGET_REALIZED (tlcolumn->tlview))
    gtk_widget_queue_resize (tlcolumn->tlview);
}

void
gtk_tlcolumn_set_min_width (GtkTLColumn *tlcolumn,
			    gint         min_width)
{
  gint real_min_width;

  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));
  g_return_if_fail (min_width >= -1);

  if (min_width == tlcolumn->min_width)
    return;

  real_min_width = (tlcolumn->min_width == -1) ?
    tlcolumn->button->requisition.width : tlcolumn->min_width;

/* We want to queue a resize if the either the old min_size or the
   * new min_size determined the size of the column */
  if (GTK_WIDGET_REALIZED (tlcolumn->tlview) &&
      ((tlcolumn->min_width > tlcolumn->size) ||
       (tlcolumn->min_width == -1 &&
	tlcolumn->button->requisition.width > tlcolumn->size) ||
       (min_width > tlcolumn->size) ||
       (min_width == -1 &&
	tlcolumn->button->requisition.width > tlcolumn->size)))
    gtk_widget_queue_resize (tlcolumn->tlview);

  if (tlcolumn->max_width != -1 &&
      tlcolumn->max_width < real_min_width)
    tlcolumn->max_width = real_min_width;

  tlcolumn->min_width = min_width;
}

gint
gtk_tlcolumn_get_min_width (GtkTLColumn *tlcolumn)
{
  g_return_val_if_fail (tlcolumn != NULL, -1);
  g_return_val_if_fail (GTK_IS_TLCOLUMN (tlcolumn), -1);

  return tlcolumn->min_width;
}

void
gtk_tlcolumn_set_max_width (GtkTLColumn *tlcolumn,
			    gint         max_width)
{
  gint real_min_width;

  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));
  g_return_if_fail (max_width >= -1);

  if (max_width == tlcolumn->max_width)
    return;

  real_min_width = tlcolumn->min_width == -1 ?
    tlcolumn->button->requisition.width : tlcolumn->min_width;

  if (GTK_WIDGET_REALIZED (tlcolumn->tlview) &&
      ((tlcolumn->max_width < tlcolumn->size) ||
       (max_width != -1 && max_width < tlcolumn->size)))
    gtk_widget_queue_resize (tlcolumn->tlview);

  tlcolumn->max_width = max_width;

  if (real_min_width > max_width)
    tlcolumn->min_width = max_width;
}

gint
gtk_tlcolumn_get_max_width (GtkTLColumn *tlcolumn)
{
  g_return_val_if_fail (tlcolumn != NULL, -1);
  g_return_val_if_fail (GTK_IS_TLCOLUMN (tlcolumn), -1);

  return tlcolumn->max_width;
}

void
gtk_tlcolumn_set_title (GtkTLColumn *tlcolumn,
			gchar       *title)
{
  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));

  g_free (tlcolumn->title);
  if (title)
    tlcolumn->title = g_strdup (title);
}

gchar *
gtk_tlcolumn_get_title (GtkTLColumn *tlcolumn)
{
  g_return_val_if_fail (tlcolumn != NULL, NULL);
  g_return_val_if_fail (GTK_IS_TLCOLUMN (tlcolumn), NULL);

  return tlcolumn->title;
}

void
gtk_tlcolumn_set_header_active (GtkTLColumn *tlcolumn,
				gboolean     active)
{
  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));

  if (!tlcolumn->button || !tlcolumn->button_passive)
    return;

  tlcolumn->button_passive = FALSE;

  gtk_signal_disconnect_by_func (GTK_OBJECT (tlcolumn->button),
				 (GtkSignalFunc) gtk_tlview_passive_func,
				 NULL);

  GTK_WIDGET_SET_FLAGS (tlcolumn->button, GTK_CAN_FOCUS);
  if (GTK_WIDGET_VISIBLE (tlcolumn->tlview))
    gtk_widget_queue_draw (tlcolumn->button);
}


void
gtk_tlcolumn_set_widget (GtkTLColumn *tlcolumn,
			 GtkWidget   *widget)
{
#if 0
  gint new_button = 0;
  GtkWidget *old_widget;

  g_return_if_fail (tlview != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (tlview));

  if (column < 0 || column >= tlview->priv->columns)
    return;

  /* if the column button doesn't currently exist,
   * it has to be created first */
  if (!column->button)
    {
      column_button_create (tlview, column);
      new_button = 1;
    }

  column_title_new (clist, column, NULL);

  /* remove and destroy the old widget */
  old_widget = GTK_BIN (clist->column[column].button)->child;
  if (old_widget)
    gtk_container_remove (GTK_CONTAINER (clist->column[column].button),
			  old_widget);

  /* add and show the widget */
  if (widget)
    {
      gtk_container_add (GTK_CONTAINER (clist->column[column].button), widget);
      gtk_widget_show (widget);
    }

  /* if this button didn't previously exist, then the
   * column button positions have to be re-computed */
  if (GTK_WIDGET_VISIBLE (clist) && new_button)
    size_allocate_title_buttons (clist);
#endif
}

GtkWidget *
gtk_tlcolumn_get_widget (GtkTLColumn *tlcolumn)
{
  g_return_val_if_fail (tlcolumn != NULL, NULL);
  g_return_val_if_fail (GTK_IS_TLCOLUMN (tlcolumn), NULL);


  if (tlcolumn->button)
    return GTK_BUTTON (tlcolumn->button)->child;

  return NULL;
}

void
gtk_tlcolumn_set_justification (GtkTLColumn      *tlcolumn,
				GtkJustification  justification)
{
  GtkWidget *alignment;

  g_return_if_fail (tlcolumn != NULL);
  g_return_if_fail (GTK_IS_TLCOLUMN (tlcolumn));

  if (tlcolumn->justification == justification)
    return;
  tlcolumn->justification = justification;

  /* change the alinment of the button title if it's not a
   * custom widget */
  alignment = GTK_BIN (tlcolumn->button)->child;

  if (GTK_IS_ALIGNMENT (alignment))
    {
      switch (tlcolumn->justification)
	{
	case GTK_JUSTIFY_LEFT:
	  gtk_alignment_set (GTK_ALIGNMENT (alignment), 0.0, 0.5, 0.0, 0.0);
	  break;

	case GTK_JUSTIFY_RIGHT:
	  gtk_alignment_set (GTK_ALIGNMENT (alignment), 1.0, 0.5, 0.0, 0.0);
	  break;

	case GTK_JUSTIFY_CENTER:
	  gtk_alignment_set (GTK_ALIGNMENT (alignment), 0.5, 0.5, 0.0, 0.0);
	  break;

	case GTK_JUSTIFY_FILL:
	  gtk_alignment_set (GTK_ALIGNMENT (alignment), 0.5, 0.5, 0.0, 0.0);
	  break;

	default:
	  break;
	}
    }
}

