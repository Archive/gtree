/* gtk-tlcolumn.h
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_TLCOLUMN_H__
#define __GTK_TLCOLUMN_H__

#include <gtk/gtk.h>
#include "grbtree.h"
#include "gtktlcell.h"
#include "gtktlmodel.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define GTK_TYPE_TLCOLUMN			(gtk_tlcolumn_get_type ())
#define GTK_TLCOLUMN(obj)			(GTK_CHECK_CAST ((obj), GTK_TYPE_TLCOLUMN, GtkTLColumn))
#define GTK_TLCOLUMN_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_TLCOLUMN, GtkTLColumnClass))
#define GTK_IS_TLCOLUMN(obj)			(GTK_CHECK_TYPE ((obj), GTK_TYPE_TLCOLUMN))
#define GTK_IS_TLCOLUMN_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_TLCOLUMN))


typedef enum
{
  GTK_TLCOLUMN_RESIZEABLE,
  GTK_TLCOLUMN_AUTOSIZE,
  GTK_TLCOLUMN_FIXED
} GtkTLColumnType;

typedef struct _GtkTLColumn       GtkTLColumn;
typedef struct _GtkTLColumnClass  GtkTLColumnClass;

typedef gboolean (* GtkTreeColumnFunc) (GtkTLColumn *tlcolumn, GtkTLModel *tlmodel, GtkTLNode tlnode, gpointer data);

struct _GtkTLColumn
{
  GtkObject parent;

  GtkWidget *tlview;
  GtkWidget *button;
  GdkWindow *window;
  GtkJustification justification;

  gint id;
  gint size;
  gint min_width;
  gint max_width;

  GtkTreeColumnFunc *func;
  gpointer func_data;
  gchar *title;
  GtkTLCell *cell;
  GSList *attributes;
  GtkTLColumnType column_type;
  guint visible        : 1;
  guint button_passive : 1;
  guint dirty          : 1;
  
};
struct _GtkTLColumnClass
{
  GtkObjectClass parent_class;

  void (*clicked) (GtkTLColumn *tlcolumn);
};


GtkType          gtk_tlcolumn_get_type           (void);
GtkObject       *gtk_tlcolumn_new                (void);
GtkObject       *gtk_tlcolumn_new_with_vals      (gchar            *title,
						  GtkTLCell        *cell,
						  ...);
void             gtk_tlcolumn_set_cell           (GtkTLColumn      *tlcolumn,
						  GtkTLCell        *cell);
void             gtk_tlcolumn_set_attribute      (GtkTLColumn      *tlcolumn,
						  gchar            *attribute,
						  gint              column);
void             gtk_tlcolumn_set_attributes     (GtkTLColumn      *tlcolumn,
						  ...);
void             gtk_tlcolumn_set_cell_data      (GtkTLColumn      *tlcolumn,
						  GtkTLModel       *tlmodel,
						  GtkTLNode         tlnode);
void             gtk_tlcolumn_set_visible        (GtkTLColumn      *tlcolumn,
						  gboolean          visible);
gboolean         gtk_tlcolumn_get_visible        (GtkTLColumn      *tlcolumn);
void             gtk_tlcolumn_set_col_type       (GtkTLColumn      *tlcolumn,
						  GtkTLColumnType   type);
gint             gtk_tlcolumn_get_col_type       (GtkTLColumn      *tlcolumn);
gint             gtk_tlcolumn_get_preferred_size (GtkTLColumn      *tlcolumn);
gint             gtk_tlcolumn_get_size           (GtkTLColumn      *tlcolumn);
void             gtk_tlcolumn_set_size           (GtkTLColumn      *tlcolumn,
						  gint              width);
void             gtk_tlcolumn_set_min_width      (GtkTLColumn      *tlcolumn,
						  gint              min_width);
gint             gtk_tlcolumn_get_min_width      (GtkTLColumn      *tlcolumn);
void             gtk_tlcolumn_set_max_width      (GtkTLColumn      *tlcolumn,
						  gint              max_width);
gint             gtk_tlcolumn_get_max_width      (GtkTLColumn      *tlcolumn);



/* Options for manipulating the column headers */
void             gtk_tlcolumn_set_title          (GtkTLColumn      *tlcolumn,
						  gchar            *title);
gchar           *gtk_tlcolumn_get_title          (GtkTLColumn      *tlcolumn);
void             gtk_tlcolumn_set_header_active  (GtkTLColumn      *tlcolumn,
						  gboolean          active);
void             gtk_tlcolumn_set_widget         (GtkTLColumn      *tlcolumn,
						  GtkWidget        *widget);
GtkWidget       *gtk_tlcolumn_get_widget         (GtkTLColumn      *tlcolumn);
void             gtk_tlcolumn_set_justification  (GtkTLColumn      *tlcolumn,
						  GtkJustification  justification);
GtkJustification gtk_tlcolumn_get_justification  (GtkTLColumn      *tlcolumn);


/* Rendering options */


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_TLCOLUMN_H__ */
