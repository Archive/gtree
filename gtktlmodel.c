/* gtktlmodel.c
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include "gtktlmodel.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

enum {
  NODE_CHANGED,
  NODE_INSERTED,
  NODE_INSERTED_BEFORE,
  NODE_CHILD_ADDED,
  NODE_DELETED,
  LAST_SIGNAL
};

struct _GtkTLPath
{
  gint depth;
  gint *indices;
};

static void gtk_tlmodel_init		        (GtkTLModel		 *TLModel);
static void gtk_tlmodel_class_init	        (GtkTLModelClass	 *klass);

static GtkObjectClass *parent_class = NULL;
static guint tlmodel_signals[LAST_SIGNAL] = { 0 };


GtkType
gtk_tlmodel_get_type (void)
{
  static GtkType tlmodel_type = 0;

  if (!tlmodel_type)
    {
      static const GTypeInfo tlmodel_info =
      {
        sizeof (GtkTLModelClass),
	NULL,		/* base_init */
	NULL,		/* base_finalize */
        (GClassInitFunc) gtk_tlmodel_class_init,
	NULL,		/* class_finalize */
	NULL,		/* class_data */
	sizeof (GtkTLModel),
	0,		/* n_preallocs */
        (GInstanceInitFunc) gtk_tlmodel_init
      };

      tlmodel_type = g_type_register_static (GTK_TYPE_OBJECT, "GtkTLModel", &tlmodel_info);
    }

  return tlmodel_type;
}

static void
gtk_tlmodel_class_init (GtkTLModelClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;

  parent_class = g_type_class_peek_parent (class);

  tlmodel_signals[NODE_CHANGED] = 
    gtk_signal_new ("node_changed",
                    GTK_RUN_FIRST,
                    GTK_CLASS_TYPE (object_class),
                    GTK_SIGNAL_OFFSET (GtkTLModelClass, node_changed),
                    gtk_marshal_NONE__POINTER_POINTER,
                    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);
  tlmodel_signals[NODE_INSERTED] = 
    gtk_signal_new ("node_inserted",
                    GTK_RUN_FIRST,
                    GTK_CLASS_TYPE (object_class),
                    GTK_SIGNAL_OFFSET (GtkTLModelClass, node_inserted),
                    gtk_marshal_NONE__POINTER_POINTER,
                    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);
  tlmodel_signals[NODE_INSERTED_BEFORE] = 
    gtk_signal_new ("node_inserted_before",
                    GTK_RUN_FIRST,
                    GTK_CLASS_TYPE (object_class),
                    GTK_SIGNAL_OFFSET (GtkTLModelClass, node_inserted_before),
                    gtk_marshal_NONE__POINTER_POINTER,
                    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);
  tlmodel_signals[NODE_CHILD_ADDED] = 
    gtk_signal_new ("node_child_added",
                    GTK_RUN_FIRST,
                    GTK_CLASS_TYPE (object_class),
                    GTK_SIGNAL_OFFSET (GtkTLModelClass, node_child_added),
                    gtk_marshal_NONE__POINTER_POINTER,
                    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);
  tlmodel_signals[NODE_DELETED] = 
    gtk_signal_new ("node_deleted",
                    GTK_RUN_FIRST,
                    GTK_CLASS_TYPE (object_class),
                    GTK_SIGNAL_OFFSET (GtkTLModelClass, node_deleted),
                    gtk_marshal_NONE__POINTER,
                    GTK_TYPE_NONE, 1,
		    GTK_TYPE_POINTER);


  gtk_object_class_add_signals (object_class, tlmodel_signals, LAST_SIGNAL);


  class->get_node = NULL;
  class->node_next = NULL;
  class->node_children = NULL;
  class->node_n_children = NULL;
  class->node_nth_child = NULL;
  class->node_parent = NULL;
}


static void
gtk_tlmodel_init (GtkTLModel *TLModel)
{
  
}

/* GtkTLPath Operations */
GtkTLPath *
gtk_tlpath_new (void)
{
  GtkTLPath *retval;
  retval = (GtkTLPath *) g_new (GtkTLPath, 1);
  retval->depth = 0;
  retval->indices = NULL;

  return retval;
}

GtkTLPath *
gtk_tlpath_new_from_string (gchar *path)
{
  GtkTLPath *retval;
  gchar *ptr;
  gint i;

  g_return_val_if_fail (path != NULL, gtk_tlpath_new ());

  retval = gtk_tlpath_new ();

  while (1)
    {
      i = strtol (path, &ptr, 10);
      gtk_tlpath_add_index (retval, i);

      if (*ptr == '\000')
	break;
      path = ptr + 1;
    }

  return retval;
}

gchar *
gtk_tlpath_get_string (GtkTLPath *path)
{
  gchar *retval, *ptr;
  gint i;

  if (path->depth == 0)
    return NULL;

  ptr = retval = (gchar *) g_new0 (char *, path->depth*8);
  sprintf (retval, "%d", path->indices[0]);
  while (*ptr != '\000')
    ptr++;

  for (i = 1; i < path->depth; i++)
    {
      sprintf (ptr, ":%d", path->indices[i]);
      while (*ptr != '\000')
	ptr++;
    }

  return retval;
}

GtkTLPath *
gtk_tlpath_new_root (void)
{
  GtkTLPath *retval;

  retval = gtk_tlpath_new ();
  gtk_tlpath_add_index (retval, 1);

  return retval;
}

void
gtk_tlpath_add_index (GtkTLPath *path, gint index)
{
  gint *new_indices = g_new (gint, ++path->depth);
  if (path->indices == NULL)
    {
      path->indices = new_indices;
      path->indices[0] = index;
      return;
    }

  memcpy (new_indices, path->indices, (path->depth - 1)*sizeof (gint));
  g_free (path->indices);
  path->indices = new_indices;
  path->indices[path->depth] = index;
}

void
gtk_tlpath_prepend_index (GtkTLPath *path,
			  gint       index)
{
  gint *new_indices = g_new (gint, ++path->depth);
  if (path->indices == NULL)
    {
      path->indices = new_indices;
      path->indices[0] = index;
      return;
    }
  memcpy (new_indices + 1, path->indices, (path->depth - 1)*sizeof (gint));
  g_free (path->indices);
  path->indices = new_indices;
  path->indices[0] = index;
}

gint
gtk_tlpath_get_depth (GtkTLPath *path)
{
  return path->depth;
}

const gint *
gtk_tlpath_get_indices (GtkTLPath *path)
{
  return path->indices;
}

void
gtk_tlpath_free (GtkTLPath *path)
{
  g_free (path->indices);
  g_free (path);
}

GtkTLPath *
gtk_tlpath_copy (GtkTLPath *path)
{
  GtkTLPath *retval;

  retval = g_new (GtkTLPath, 1);
  retval->depth = path->depth;
  retval->indices = g_new (gint, path->depth);
  memcpy (retval->indices, path->indices, path->depth * sizeof (gint));
  return retval;
}

gint
gtk_tlpath_cmp (GtkTLPath  *a,
		GtkTLPath  *b)
{
  gint p = 0, q = 0;

  g_return_val_if_fail (a != NULL, 0);
  g_return_val_if_fail (b != NULL, 0);
  g_return_val_if_fail (a->depth > 0, 0);
  g_return_val_if_fail (b->depth > 0, 0);

  do
    {
      if (a->indices[p] == b->indices[q])
	continue;
      return (a->indices[p] < b->indices[q]?1:-1);
    }
  while (++p < a->depth && ++q < b->depth);
  if (a->depth == b->depth)
    return 0;
  return (a->depth < b->depth?1:-1);
}

gint
gtk_tlmodel_get_columns (GtkTLModel *tlmodel)
{
  g_return_val_if_fail (GTK_TLMODEL_GET_CLASS (tlmodel)->get_columns != NULL, 0);
  return (* GTK_TLMODEL_GET_CLASS (tlmodel)->get_columns) (tlmodel);
}

/* Node options */
GtkTLNode
gtk_tlmodel_get_node (GtkTLModel *tlmodel,
		      GtkTLPath  *path)
{
  g_return_val_if_fail (GTK_TLMODEL_GET_CLASS (tlmodel)->get_node != NULL, NULL);
  return (* GTK_TLMODEL_GET_CLASS (tlmodel)->get_node) (tlmodel, path);
}

GtkTLPath *
gtk_tlmodel_get_path (GtkTLModel *tlmodel,
		      GtkTLNode   node)
{
  g_return_val_if_fail (GTK_TLMODEL_GET_CLASS (tlmodel)->get_path != NULL, NULL);
  return (* GTK_TLMODEL_GET_CLASS (tlmodel)->get_path) (tlmodel, node);
}

void
gtk_tlmodel_node_get_value (GtkTLModel *tlmodel,
			    GtkTLNode   node,
			    gint        column,
			    GValue     *value)
{
  g_return_if_fail (GTK_TLMODEL_GET_CLASS (tlmodel)->node_get_value != NULL);
  (* GTK_TLMODEL_GET_CLASS (tlmodel)->node_get_value) (tlmodel, node, column, value);
}

gboolean
gtk_tlmodel_node_next (GtkTLModel  *tlmodel,
		       GtkTLNode   *node)
{
  g_return_val_if_fail (GTK_TLMODEL_GET_CLASS (tlmodel)->node_next != NULL, FALSE);
  return (* GTK_TLMODEL_GET_CLASS (tlmodel)->node_next) (tlmodel, node);
}

GtkTLNode
gtk_tlmodel_node_children (GtkTLModel *tlmodel,
			   GtkTLNode   node)
{
  g_return_val_if_fail (GTK_TLMODEL_GET_CLASS (tlmodel)->node_children != NULL, NULL);
  return (* GTK_TLMODEL_GET_CLASS (tlmodel)->node_children) (tlmodel, node);
}

gboolean
gtk_tlmodel_node_has_child (GtkTLModel *tlmodel,
			    GtkTLNode   node)
{
  g_return_val_if_fail (GTK_TLMODEL_GET_CLASS (tlmodel)->node_has_child != NULL, FALSE);
  return (* GTK_TLMODEL_GET_CLASS (tlmodel)->node_has_child) (tlmodel, node);
}

gint
gtk_tlmodel_node_n_children (GtkTLModel *tlmodel,
			     GtkTLNode   node)
{
  g_return_val_if_fail (GTK_TLMODEL_GET_CLASS (tlmodel)->node_n_children != NULL, -1);
  return (* GTK_TLMODEL_GET_CLASS (tlmodel)->node_n_children) (tlmodel, node);
}

GtkTLNode
gtk_tlmodel_node_nth_child (GtkTLModel *tlmodel,
			    GtkTLNode   node,
			    gint        n)
{
  g_return_val_if_fail (GTK_TLMODEL_GET_CLASS (tlmodel)->node_nth_child != NULL, NULL);
  return (* GTK_TLMODEL_GET_CLASS (tlmodel)->node_nth_child) (tlmodel, node, n);
}

GtkTLNode
gtk_tlmodel_node_parent (GtkTLModel *tlmodel,
			 GtkTLNode   node)
{
  g_return_val_if_fail (GTK_TLMODEL_GET_CLASS (tlmodel)->node_parent != NULL, NULL);
  return (* GTK_TLMODEL_GET_CLASS (tlmodel)->node_parent) (tlmodel, node);

}

