/* gtktlmodel.h
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_TLMODEL_H__
#define __GTK_TLMODEL_H__

#include <gtk/gtk.h>
#include "gtktlcell.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define GTK_TYPE_TLMODEL			(gtk_tlmodel_get_type ())
#define GTK_TLMODEL(obj)			(GTK_CHECK_CAST ((obj), GTK_TYPE_TLMODEL, GtkTLModel))
#define GTK_TLMODEL_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_TLMODEL, GtkTLModelClass))
#define GTK_IS_TLMODEL(obj)			(GTK_CHECK_TYPE ((obj), GTK_TYPE_TLMODEL))
#define GTK_IS_TLMODEL_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_TLMODEL))
#define GTK_TLMODEL_GET_CLASS(obj)              (GTK_CHECK_GET_CLASS ((obj), GTK_TYPE_TLMODEL, GtkTLModelClass))

typedef gpointer                GtkTLNode;
typedef struct _GtkTLPath       GtkTLPath;
typedef struct _GtkTLModel      GtkTLModel;
typedef struct _GtkTLModelClass GtkTLModelClass;

struct _GtkTLModel
{
  GtkObject parent;
};

struct _GtkTLModelClass
{
  GtkObjectClass parent_class;

  /* signals */
  void       (* node_changed)         (GtkTLModel *TLModel, GtkTLPath *path, GtkTLNode *node);
  void       (* node_inserted)        (GtkTLModel *TLModel, GtkTLPath *path, GtkTLNode *node);
  void       (* node_inserted_before) (GtkTLModel *TLModel, GtkTLPath *path, GtkTLNode *node);
  void       (* node_child_added)     (GtkTLModel *TLModel, GtkTLPath *path, GtkTLNode *node);
  void       (* node_deleted)         (GtkTLModel *TLModel, GtkTLPath *path);

  /* VTable - not signals */
  gint       (* get_columns)     (GtkTLModel *TLModel);
  GtkTLNode  (* get_node)        (GtkTLModel *TLModel,
				  GtkTLPath  *path);
  GtkTLPath *(* get_path)        (GtkTLModel *TLModel,
				  GtkTLNode   node);
  void       (* node_get_value)  (GtkTLModel *TLModel,
				  GtkTLNode   node,
				  gint        column,
				  GValue     *value);
  gboolean   (* node_next)       (GtkTLModel *TLModel,
				  GtkTLNode  *node);
  GtkTLNode  (* node_children)   (GtkTLModel *TLModel,
				  GtkTLNode   node);
  gboolean   (* node_has_child)  (GtkTLModel *TLModel,
				  GtkTLNode   node);
  gint       (* node_n_children) (GtkTLModel *TLModel,
				  GtkTLNode   node);
  GtkTLNode  (* node_nth_child)  (GtkTLModel *TLModel,
				  GtkTLNode   node,
				  gint        n);
  GtkTLNode  (* node_parent)     (GtkTLModel *TLModel,
				  GtkTLNode   node);
};


/* Basic tlmodel operations */
GtkType      gtk_tlmodel_get_type          (void);

/* GtkTLPath Operations */
GtkTLPath   *gtk_tlpath_new                (void);
GtkTLPath   *gtk_tlpath_new_from_string    (gchar      *path);
gchar       *gtk_tlpath_get_string         (GtkTLPath  *path);
GtkTLPath   *gtk_tlpath_new_root           (void);
void         gtk_tlpath_add_index          (GtkTLPath  *path,
					    gint        index);
void         gtk_tlpath_prepend_index      (GtkTLPath  *path,
					    gint        index);
gint         gtk_tlpath_get_depth          (GtkTLPath  *path);
const gint * gtk_tlpath_get_indices        (GtkTLPath  *path);
void         gtk_tlpath_free               (GtkTLPath  *path);
GtkTLPath   *gtk_tlpath_copy               (GtkTLPath  *path);
gint         gtk_tlpath_cmp                (GtkTLPath  *a,
					    GtkTLPath  *b);
/* Header operations */
gint         gtk_tlmodel_get_columns       (GtkTLModel *tlmodel);

/* Node operations */
GtkTLNode    gtk_tlmodel_get_node          (GtkTLModel *tlmodel,
					    GtkTLPath  *path);
GtkTLPath   *gtk_tlmodel_get_path          (GtkTLModel *tlmodel,
					    GtkTLNode   node);
void         gtk_tlmodel_node_get_value    (GtkTLModel *TLModel,
					    GtkTLNode   node,
					    gint        column,
					    GValue     *value);
gboolean     gtk_tlmodel_node_next         (GtkTLModel *tlmodel,
					    GtkTLNode  *node);
GtkTLNode    gtk_tlmodel_node_children     (GtkTLModel *tlmodel,
					    GtkTLNode   node);
gboolean     gtk_tlmodel_node_has_child    (GtkTLModel *tlmodel,
					    GtkTLNode   node);
gint         gtk_tlmodel_node_n_children   (GtkTLModel *tlmodel,
					    GtkTLNode   node);
GtkTLNode    gtk_tlmodel_node_nth_child    (GtkTLModel *tlmodel,
					    GtkTLNode   node,
					    gint        n);
GtkTLNode    gtk_tlmodel_node_parent       (GtkTLModel *tlmodel,
					    GtkTLNode   node);






#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_TLMODEL_H__ */
