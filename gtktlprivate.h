/* gtktlprivate.h
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GTK_TLPRIVATE_H__
#define __GTK_TLPRIVATE_H__


#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#include <gtk/gtk.h>
#include "grbtree.h"
#include "gtktlview.h"
#include "gtktlselection.h"


typedef enum {
  GTK_TLVIEW_IS_LIST = 1 << 0,
  GTK_TLVIEW_SHOW_EXPANDERS = 1 << 1,
  GTK_TLVIEW_IN_COLUMN_RESIZE = 1 << 2,
  GTK_TLVIEW_ARROW_PRELIT = 1 << 3,
  GTK_TLVIEW_HEADERS_VISIBLE = 1 << 4,
  GTK_TLVIEW_DRAW_KEYFOCUS = 1 << 5,
  GTK_TLVIEW_MODEL_SETUP = 1 << 6
} GtkTLViewFlags;

#define GTK_TLVIEW_SET_FLAG(tlview, flag)   G_STMT_START{ (tlview->priv->flags|=flag); }G_STMT_END
#define GTK_TLVIEW_UNSET_FLAG(tlview, flag) G_STMT_START{ (tlview->priv->flags&=~(flag)); }G_STMT_END
#define GTK_TLVIEW_FLAG_SET(tlview, flag)   ((tlview->priv->flags&flag)==flag)
#define TLVIEW_HEADER_HEIGHT(tlview)        (GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_HEADERS_VISIBLE)?tlview->priv->header_height:0)
#define TLVIEW_COLUMN_SIZE(column)          (CLAMP (column->size, (column->min_width!=-1)?column->min_width:column->size, (column->max_width!=-1)?column->max_width:column->size))
#define TLVIEW_DRAW_EXPANDERS(tlview)       (!GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_IS_LIST)&&GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_SHOW_EXPANDERS))

struct _GtkTLViewPrivate
{
  GtkTLModel *model;

  guint flags;
  /* tree information */
  GRBTree *tree;

  gint tab_offset;
  GRBNode *button_pressed_node;
  GRBTree *button_pressed_tree;

  GList *children;
  gint width;
  gint height;

  GtkAdjustment *hadjustment;
  GtkAdjustment *vadjustment;

  GdkWindow *bin_window;
  GdkWindow *header_window;

  /* Selection stuff */
  GtkTLPath *anchor;
  GtkTLPath *cursor;

  /* Column Resizing */
  GdkCursor *cursor_drag;
  GdkGC *xor_gc;
  gint drag_pos;
  gint x_drag;

  /* Prelight information */
  GRBNode *prelight_node;
  GRBTree *prelight_tree;
  gint prelight_offset;

  /* Selection information */
  GtkTLSelection *selection;

  /* Header information */
  gint columns;
  GList *column;
  gint header_height;
};

#define TLVIEW_INTERNAL_ASSERT(expr, ret)     G_STMT_START{             \
     if (!(expr))                                                       \
       {                                                                \
         g_log (G_LOG_DOMAIN,                                           \
                G_LOG_LEVEL_CRITICAL,                                   \
		"file %s: line %d (%s): assertion `%s' failed.\n"       \
	        "There is a disparity between the internal view of the GtkTLView,\n"    \
		"and the GtkTLModel.  This generally means that the model has changed\n"\
		"without letting the view know.  Any display from now on is likely to\n"\
		"be incorrect.\n",                                                      \
                __FILE__,                                               \
                __LINE__,                                               \
                __PRETTY_FUNCTION__,                                    \
                #expr);                                                 \
         return ret;                                                    \
       };                               }G_STMT_END

#define TLVIEW_INTERNAL_ASSERT_VOID(expr)     G_STMT_START{             \
     if (!(expr))                                                       \
       {                                                                \
         g_log (G_LOG_DOMAIN,                                           \
                G_LOG_LEVEL_CRITICAL,                                   \
		"file %s: line %d (%s): assertion `%s' failed.\n"       \
	        "There is a disparity between the internal view of the GtkTLView,\n"    \
		"and the GtkTLModel.  This generally means that the model has changed\n"\
		"without letting the view know.  Any display from now on is likely to\n"\
		"be incorrect.\n",                                                      \
                __FILE__,                                               \
                __LINE__,                                               \
                __PRETTY_FUNCTION__,                                    \
                #expr);                                                 \
         return;                                                        \
       };                               }G_STMT_END


/* functions that shouldn't be exported */
void       gtk_tlselection_internal_select_node (GtkTLSelection   *selection,
						 GRBNode          *node,
						 GRBTree          *tree,
						 GtkTLPath        *path,
						 GdkModifierType   state);

gboolean   gtk_tlview_find_node                 (GtkTLView        *tlview,
						 GtkTLPath        *path,
						 GRBTree         **tree,
						 GRBNode         **node);

GtkTLPath *gtk_tlview_find_path                 (GtkTLView        *tlview,
						 GRBTree          *tree,
						 GRBNode          *node);

void       gtk_tlview_set_size                  (GtkTLView        *tlview,
						 gint              width,
						 gint              height);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_TLPRIVATE_H__ */

