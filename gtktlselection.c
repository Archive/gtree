#include "gtktlselection.h"
#include "gtktlprivate.h"
#include "grbtree.h"


static void     gtk_tlselection_init           (GtkTLSelection       *selection);
static void     gtk_tlselection_class_init     (GtkTLSelectionClass  *class);

enum {
  ROW_SELECTED,
  ROW_UNSELECTED,
  LAST_SIGNAL
};

static GtkObjectClass *parent_class = NULL;
static guint tlselection_signals[LAST_SIGNAL] = { 0 };

static void
gtk_tlselection_real_select_node (GtkTLSelection *selection, GRBTree *tree, GRBNode *node, gboolean select)
{
  gboolean selected = FALSE;
  GtkTLPath *path = NULL;

  if (G_RBNODE_FLAG_SET (node, G_RBNODE_IS_SELECTED) != select)
    {
      path = gtk_tlview_find_path (selection->tlview, tree, node);
      if (selection->user_func)
	{
	  if ((*selection->user_func) (selection, selection->tlview->priv->model, path, selection->user_data))
	    selected = TRUE;
	}
      else
	selected = TRUE;
    }
  if (selected == TRUE)
    {
      GtkTLNode tlnode;
      tlnode = gtk_tlmodel_get_node (selection->tlview->priv->model, path);

      node->flags ^= G_RBNODE_IS_SELECTED;
      if (select)
	gtk_signal_emit (GTK_OBJECT (selection), tlselection_signals[ROW_SELECTED], selection->tlview->priv->model, tlnode);
      else
	gtk_signal_emit (GTK_OBJECT (selection), tlselection_signals[ROW_UNSELECTED], selection->tlview->priv->model, tlnode);
      gtk_widget_queue_draw (GTK_WIDGET (selection->tlview));
    }
}

GtkType
gtk_tlselection_get_type (void)
{
  static GtkType selection_type = 0;

  if (!selection_type)
    {
      static const GTypeInfo selection_info =
      {
        sizeof (GtkTLSelectionClass),
	NULL,		/* base_init */
	NULL,		/* base_finalize */
        (GClassInitFunc) gtk_tlselection_class_init,
	NULL,		/* class_finalize */
	NULL,		/* class_data */
        sizeof (GtkTLSelection),
	0,              /* n_preallocs */
        (GInstanceInitFunc) gtk_tlselection_init
      };

      selection_type = g_type_register_static (GTK_TYPE_OBJECT, "GtkTLSelection", &selection_info);
    }

  return selection_type;
}

static void
gtk_tlselection_class_init (GtkTLSelectionClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;
  parent_class = g_type_class_peek_parent (class);

  tlselection_signals[ROW_SELECTED] =
    gtk_signal_new ("row_selected",
		    GTK_RUN_FIRST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GtkTLSelectionClass, row_selected),
		    gtk_marshal_NONE__POINTER_POINTER,
		    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);

  tlselection_signals[ROW_UNSELECTED] =
    gtk_signal_new ("row_unselected",
		    GTK_RUN_FIRST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GtkTLSelectionClass, row_unselected),
		    gtk_marshal_NONE__POINTER_POINTER,
		    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);

  gtk_object_class_add_signals (object_class, tlselection_signals, LAST_SIGNAL);

  class->row_selected = NULL;
  class->row_unselected = NULL;
}

static void
gtk_tlselection_init (GtkTLSelection *selection)
{
  selection->type = GTK_TLSELECTION_MULTI;
  selection->user_func = NULL;
  selection->user_data = NULL;
  selection->user_func = NULL;
  selection->tlview = NULL;
}

GtkObject *
gtk_tlselection_new (void)
{
  GtkObject *selection;

  selection = GTK_OBJECT (gtk_type_new (GTK_TYPE_TLSELECTION));

  return selection;
}

GtkObject *
gtk_tlselection_new_with_tlview (GtkTLView *tlview)
{
  GtkObject *selection;

  g_return_val_if_fail (tlview != NULL, NULL);
  g_return_val_if_fail (GTK_IS_TLVIEW (tlview), NULL);

  selection = gtk_tlselection_new ();
  gtk_tlselection_set_tlview (GTK_TLSELECTION (selection), tlview);

  return selection;
}

void
gtk_tlselection_set_tlview (GtkTLSelection *selection,
			    GtkTLView      *tlview)
{
  g_return_if_fail (selection != NULL);
  g_return_if_fail (GTK_IS_TLSELECTION (selection));
  if (tlview != NULL)
    g_return_if_fail (GTK_IS_TLVIEW (tlview));

  selection->tlview = tlview;
  tlview->priv->selection = selection;
}

void
gtk_tlselection_set_type (GtkTLSelection     *selection,
			  GtkTLSelectionType  type)
{
  g_return_if_fail (selection != NULL);
  g_return_if_fail (GTK_IS_TLSELECTION (selection));

  if (selection->type == type)
    return;

  if (type == GTK_TLSELECTION_SINGLE)
    {
      GRBTree *tree = NULL;
      GRBNode *node = NULL;
      gint selected = FALSE;

      if (selection->tlview->priv->anchor)
	{
	  gtk_tlview_find_node (selection->tlview,
				selection->tlview->priv->anchor,
				&tree,
				&node);

	  if (node && G_RBNODE_FLAG_SET (node, G_RBNODE_IS_SELECTED))
	    selected = TRUE;
	}
      gtk_tlselection_unselect_all (selection);
      if (node && selected)
	G_RBNODE_SET_FLAG (node, G_RBNODE_IS_SELECTED);
    }
  selection->type = type;
}

void
gtk_tlselection_set_select_function (GtkTLSelection     *selection,
				     GtkTLSelectionFunc  func,
				     gpointer            data)
{
  g_return_if_fail (selection != NULL);
  g_return_if_fail (GTK_IS_TLSELECTION (selection));
  g_return_if_fail (func != NULL);

  selection->user_func = func;
  selection->user_data = data;
}

gpointer
gtk_tlselection_get_user_data (GtkTLSelection *selection)
{
  g_return_val_if_fail (selection != NULL, NULL);
  g_return_val_if_fail (GTK_IS_TLSELECTION (selection), NULL);

  return selection->user_data;
}

GtkTLNode *
gtk_tlselection_get_selected (GtkTLSelection *selection)
{
  GtkTLNode *retval;

  g_return_val_if_fail (selection != NULL, NULL);
  g_return_val_if_fail (GTK_IS_TLSELECTION (selection), NULL);

  if (selection->tlview->priv->anchor == NULL)
    return NULL;

  g_return_val_if_fail (selection->tlview != NULL, NULL);
  g_return_val_if_fail (selection->tlview->priv->model != NULL, NULL);

  retval = gtk_tlmodel_get_node (selection->tlview->priv->model,
				 selection->tlview->priv->anchor);
  return retval;
}

void
gtk_tlselection_selected_foreach (GtkTLSelection            *selection,
				  GtkTLSelectionForeachFunc  func,
				  gpointer                   data)
{
  GtkTLPath *path;
  GRBTree *tree;
  GRBNode *node;
  GtkTLNode tlnode;

  g_return_if_fail (selection != NULL);
  g_return_if_fail (GTK_IS_TLSELECTION (selection));
  g_return_if_fail (selection->tlview != NULL);
  g_return_if_fail (selection->tlview->priv->model != NULL);

  if (func == NULL ||
      selection->tlview->priv->tree == NULL ||
      selection->tlview->priv->tree->root == NULL)
    return;

  tree = selection->tlview->priv->tree;
  node = selection->tlview->priv->tree->root;

  while (node->left != tree->nil)
    node = node->left;

  /* find the node internally */
  path = gtk_tlpath_new_root ();
  tlnode = gtk_tlmodel_get_node (selection->tlview->priv->model, path);
  gtk_tlpath_free (path);

  do
    {
      if (G_RBNODE_FLAG_SET (node, G_RBNODE_IS_SELECTED))
	(* func) (selection->tlview->priv->model, tlnode, data);
      if (node->children)
	{
	  tree = node->children;
	  node = tree->root;
	  while (node->left != tree->nil)
	    node = node->left;
	  tlnode = gtk_tlmodel_node_children (selection->tlview->priv->model, tlnode);

	  /* Sanity Check! */
	  TLVIEW_INTERNAL_ASSERT_VOID (tlnode != NULL);
	}
      else
	{
	  gboolean done = FALSE;
	  do
	    {
	      node = g_rbtree_next (tree, node);
	      if (node != NULL)
		{
		  gtk_tlmodel_node_next (selection->tlview->priv->model, &tlnode);
		  done = TRUE;

		  /* Sanity Check! */
		  TLVIEW_INTERNAL_ASSERT_VOID (tlnode != NULL);
		}
	      else
		{
		  node = tree->parent_node;
		  tree = tree->parent_tree;
		  if (tree == NULL)
		    /* we've run out of tree */
		    /* We're done with this function */
		    return;
		  tlnode = gtk_tlmodel_node_parent (selection->tlview->priv->model, tlnode);

		  /* Sanity check */
		  TLVIEW_INTERNAL_ASSERT_VOID (tlnode != NULL);
		}
	    }
	  while (!done);
	}
    }
  while (TRUE);
}

void
gtk_tlselection_select_row (GtkTLSelection *selection,
			    GtkTLPath      *path)
{
  GRBNode *node;
  GRBTree *tree;
  GdkModifierType state = 0;

  g_return_if_fail (selection != NULL);
  g_return_if_fail (GTK_IS_TLSELECTION (selection));
  g_return_if_fail (selection->tlview != NULL);
  g_return_if_fail (path != NULL);

  gtk_tlview_find_node (selection->tlview,
			path,
			&tree,
			&node);

  if (node == NULL || G_RBNODE_FLAG_SET (node, G_RBNODE_IS_SELECTED))
    return;

  if (selection->type == GTK_TLSELECTION_MULTI)
    state = GDK_CONTROL_MASK;

  gtk_tlselection_internal_select_node (selection,
					node,
					tree,
					path,
					state);
}

void
gtk_tlselection_unselect_row (GtkTLSelection *selection,
			      GtkTLPath      *path)
{
  GRBNode *node;
  GRBTree *tree;

  g_return_if_fail (selection != NULL);
  g_return_if_fail (GTK_IS_TLSELECTION (selection));
  g_return_if_fail (selection->tlview != NULL);
  g_return_if_fail (path != NULL);

  gtk_tlview_find_node (selection->tlview,
			path,
			&tree,
			&node);

  if (node == NULL || !G_RBNODE_FLAG_SET (node, G_RBNODE_IS_SELECTED))
    return;

  gtk_tlselection_internal_select_node (selection,
					node,
					tree,
					path,
					GDK_CONTROL_MASK);
}

void
gtk_tlselection_select_node (GtkTLSelection *selection,
			     GtkTLNode      *tlnode)
{
  GtkTLPath *path;

  g_return_if_fail (selection != NULL);
  g_return_if_fail (GTK_IS_TLSELECTION (selection));
  g_return_if_fail (selection->tlview != NULL);
  g_return_if_fail (selection->tlview->priv->model != NULL);

  path = gtk_tlmodel_get_path (selection->tlview->priv->model,
			       tlnode);

  if (path == NULL)
    return;

  gtk_tlselection_select_row (selection, path);
  gtk_tlpath_free (path);
}


void
gtk_tlselection_unselect_node (GtkTLSelection *selection,
			       GtkTLNode      *tlnode)
{
  GtkTLPath *path;

  g_return_if_fail (selection != NULL);
  g_return_if_fail (GTK_IS_TLSELECTION (selection));
  g_return_if_fail (selection->tlview != NULL);

  path = gtk_tlmodel_get_path (selection->tlview->priv->model,
			       tlnode);

  if (path == NULL)
    return;

  gtk_tlselection_select_row (selection, path);
  gtk_tlpath_free (path);
}

/* Wish I was in python, right now... */
struct _TempTuple {
  GtkTLSelection *selection;
  gint dirty;
};

static void
select_all_helper (GRBTree  *tree,
		   GRBNode  *node,
		   gpointer  data)
{
  struct _TempTuple *tuple = data;
  
  if (node->children)
    g_rbtree_traverse (node->children,
		       node->children->root,
		       G_PRE_ORDER,
		       select_all_helper,
		       data);
  if (!G_RBNODE_FLAG_SET (node, G_RBNODE_IS_SELECTED))
    {
      gtk_tlselection_real_select_node (tuple->selection, tree, node, TRUE);
      tuple->dirty = TRUE;
    }
}

void
gtk_tlselection_select_all (GtkTLSelection *selection)
{
  struct _TempTuple *tuple;

  g_return_if_fail (selection != NULL);
  g_return_if_fail (GTK_IS_TLSELECTION (selection));
  g_return_if_fail (selection->tlview != NULL);
  g_return_if_fail (selection->tlview->priv->tree != NULL);

  if (selection->type == GTK_TLSELECTION_SINGLE)
    {
      GRBNode *node;
      node = selection->tlview->priv->tree->root;

      while (node->right != selection->tlview->priv->tree->nil)
	node = node->right;
      return;
    }

  tuple = g_new (struct _TempTuple, 1);
  tuple->selection = selection;
  tuple->dirty = FALSE;

  g_rbtree_traverse (selection->tlview->priv->tree,
		     selection->tlview->priv->tree->root,
		     G_PRE_ORDER,
		     select_all_helper,
		     tuple);
  if (tuple->dirty)
    gtk_widget_queue_draw (GTK_WIDGET (selection->tlview));
  g_free (tuple);
}

static void
unselect_all_helper (GRBTree  *tree,
		   GRBNode  *node,
		   gpointer  data)
{
  struct _TempTuple *tuple = data;

  if (node->children)
    g_rbtree_traverse (node->children,
		       node->children->root,
		       G_PRE_ORDER,
		       unselect_all_helper,
		       data);
  if (G_RBNODE_FLAG_SET (node, G_RBNODE_IS_SELECTED))
    {
      gtk_tlselection_real_select_node (tuple->selection, tree, node, FALSE);
      tuple->dirty = TRUE;
    }
}

void
gtk_tlselection_unselect_all (GtkTLSelection *selection)
{
  struct _TempTuple *tuple;

  g_return_if_fail (selection != NULL);
  g_return_if_fail (GTK_IS_TLSELECTION (selection));
  g_return_if_fail (selection->tlview != NULL);
  g_return_if_fail (selection->tlview->priv->tree != NULL);

  if (selection->type == GTK_TLSELECTION_SINGLE)
    {
      GRBTree *tree = NULL;
      GRBNode *node = NULL;
      if (selection->tlview->priv->anchor == NULL)
	return;

      gtk_tlview_find_node (selection->tlview,
			    selection->tlview->priv->anchor,
			    &tree,
			    &node);
      if (G_RBNODE_FLAG_SET (node, G_RBNODE_IS_SELECTED))
	gtk_tlselection_real_select_node (selection, tree, node, FALSE);
      return;
    }
  
  tuple = g_new (struct _TempTuple, 1);
  tuple->selection = selection;
  tuple->dirty = FALSE;

  g_rbtree_traverse (selection->tlview->priv->tree,
		     selection->tlview->priv->tree->root,
		     G_PRE_ORDER,
		     unselect_all_helper,
		     tuple);
  if (tuple->dirty)
    gtk_widget_queue_draw (GTK_WIDGET (selection->tlview));
  g_free (tuple);
}

void
gtk_tlselection_select_range (GtkTLSelection *selection,
			      GtkTLPath      *start_path,
			      GtkTLPath      *end_path)
{
  GRBNode *start_node, *end_node;
  GRBTree *start_tree, *end_tree;

  g_return_if_fail (selection != NULL);
  g_return_if_fail (GTK_IS_TLSELECTION (selection));
  g_return_if_fail (selection->tlview != NULL);

  switch (gtk_tlpath_cmp (start_path, end_path))
    {
    case -1:
      gtk_tlview_find_node (selection->tlview,
			    end_path,
			    &start_tree,
			    &start_node);
      gtk_tlview_find_node (selection->tlview,
			    start_path,
			    &end_tree,
			    &end_node);
      break;
    case 0:
      gtk_tlview_find_node (selection->tlview,
			    start_path,
			    &start_tree,
			    &start_node);
      end_tree = start_tree;
      end_node = start_node;
      break;
    case 1:
      gtk_tlview_find_node (selection->tlview,
			    start_path,
			    &start_tree,
			    &start_node);
      gtk_tlview_find_node (selection->tlview,
			    end_path,
			    &end_tree,
			    &end_node);
      break;
    }

  g_return_if_fail (start_node != NULL);
  g_return_if_fail (end_node != NULL);

  do
    {
      gtk_tlselection_real_select_node (selection, start_tree, start_node, TRUE);

      if (start_node == end_node)
	return;

      if (start_node->children)
	{
	  start_tree = start_node->children;
	  start_node = start_tree->root;
	  while (start_node->left != start_tree->nil)
	    start_node = start_node->left;
	}
      else
	{
	  gboolean done = FALSE;
	  do
	    {
	      start_node = g_rbtree_next (start_tree, start_node);
	      if (start_node != NULL)
		{
		  done = TRUE;
		}
	      else
		{
		  start_node = start_tree->parent_node;
		  start_tree = start_tree->parent_tree;
		  if (start_tree == NULL)
		    /* we've run out of tree */
		    /* This means we never found end node!! */
		    return;
		}
	    }
	  while (!done);
	}
    }
  while (TRUE);
}


/* Called internally by gtktlview.  It handles actually selecting
 * the tree.  This should almost certainly ever be called by
 * anywhere else */
void
gtk_tlselection_internal_select_node (GtkTLSelection  *selection,
				      GRBNode         *node,
				      GRBTree         *tree,
				      GtkTLPath       *path,
				      GdkModifierType  state)
{
  gint flags;

  if (((state & GDK_SHIFT_MASK) == GDK_SHIFT_MASK) && (selection->tlview->priv->anchor == NULL))
    {
      selection->tlview->priv->anchor = gtk_tlpath_copy (path);
      gtk_tlselection_real_select_node (selection, tree, node, TRUE);
    }
  else if ((state & (GDK_CONTROL_MASK|GDK_SHIFT_MASK)) == (GDK_SHIFT_MASK|GDK_CONTROL_MASK))
    {
      gtk_tlselection_select_range (selection,
				    selection->tlview->priv->anchor,
				    path);
    }
  else if ((state & GDK_CONTROL_MASK) == GDK_CONTROL_MASK)
    {
      flags = node->flags;
      if (selection->type == GTK_TLSELECTION_SINGLE)
	gtk_tlselection_unselect_all (selection);
      if (selection->tlview->priv->anchor)
	gtk_tlpath_free (selection->tlview->priv->anchor);
      selection->tlview->priv->anchor = gtk_tlpath_copy (path);
      if ((flags & G_RBNODE_IS_SELECTED) == G_RBNODE_IS_SELECTED)
	gtk_tlselection_real_select_node (selection, tree, node, FALSE);
      else
	gtk_tlselection_real_select_node (selection, tree, node, TRUE);
    }
  else if ((state & GDK_SHIFT_MASK) == GDK_SHIFT_MASK)
    {
      gtk_tlselection_unselect_all (selection);
      gtk_tlselection_select_range (selection,
				    selection->tlview->priv->anchor,
				    path);
    }
  else
    {
      gtk_tlselection_unselect_all (selection);
      if (selection->tlview->priv->anchor)
	gtk_tlpath_free (selection->tlview->priv->anchor);
      selection->tlview->priv->anchor = gtk_tlpath_copy (path);
      gtk_tlselection_real_select_node (selection, tree, node, TRUE);
    }
}

