/* gtktlselection.h
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_TLSELECTION_H__
#define __GTK_TLSELECTION_H__

#include <gobject/gobject.h>
#include <gtk/gtk.h>
#include "gtktlview.h"
#include "grbtree.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define GTK_TYPE_TLSELECTION		(gtk_tlselection_get_type ())
#define GTK_TLSELECTION(obj)		(GTK_CHECK_CAST ((obj), GTK_TYPE_TLSELECTION, GtkTLSelection))
#define GTK_TLSELECTION_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_TLSELECTION, GtkTLSelectionClass))
#define GTK_IS_TLSELECTION(obj)         (GTK_CHECK_TYPE ((obj), GTK_TYPE_TLSELECTION))
#define GTK_IS_TLSELECTION_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_TLSELECTION))

typedef enum
{
  GTK_TLSELECTION_SINGLE,
  GTK_TLSELECTION_MULTI
} GtkTLSelectionType;

typedef gboolean (*GtkTLSelectionFunc) (GtkTLSelection  *selection,
					GtkTLModel      *model,
					GtkTLPath       *path,
					gpointer         data);
typedef void (*GtkTLSelectionForeachFunc) (GtkTLModel *model,
					   GtkTLNode  *node,
					   gpointer    data);

struct _GtkTLSelection
{
  GtkObject parent;

  GtkTLView *tlview;
  GtkTLSelectionType type;
  GtkTLSelectionFunc user_func;
  gpointer user_data;
};

struct _GtkTLSelectionClass
{
  GtkObjectClass parent_class;

  void (* row_selected)   (GtkTLView  *tlview,
			   GtkTLModel *tlmodel,
			   GtkTLNode  *node);
  void (* row_unselected) (GtkTLView  *tlview,
			   GtkTLModel *tlmodel,
			   GtkTLNode  *node);
};

GtkType        gtk_tlselection_get_type            (void);
GtkObject     *gtk_tlselection_new                 (void);
GtkObject     *gtk_tlselection_new_with_tlview     (GtkTLView                 *tlview);
void           gtk_tlselection_set_tlview          (GtkTLSelection            *selection,
						    GtkTLView                 *tlview);
void           gtk_tlselection_set_type            (GtkTLSelection            *selection,
						    GtkTLSelectionType         type);
void           gtk_tlselection_set_select_function (GtkTLSelection            *selection,
						    GtkTLSelectionFunc         func,
						    gpointer                   data);
gpointer       gtk_tlselection_get_user_data       (GtkTLSelection            *selection);


/* Only meaningful if GTK_TLSELECTION_SINGLE is set */
/* User selected_foreach for GTK_TLSELECTION_MULTI */
GtkTLNode     *gtk_tlselection_get_selected        (GtkTLSelection            *selection);
void           gtk_tlselection_selected_foreach    (GtkTLSelection            *selection,
						    GtkTLSelectionForeachFunc  func,
						    gpointer                   data);
void           gtk_tlselection_select_row          (GtkTLSelection            *selection,
						    GtkTLPath                 *path);
void           gtk_tlselection_unselect_row        (GtkTLSelection            *selection,
						    GtkTLPath                 *path);
void           gtk_tlselection_select_node         (GtkTLSelection            *selection,
						    GtkTLNode                 *tlnode);
void           gtk_tlselection_unselect_node       (GtkTLSelection            *selection,
						    GtkTLNode                 *tlnode);
void           gtk_tlselection_select_all          (GtkTLSelection            *selection);
void           gtk_tlselection_unselect_all        (GtkTLSelection            *selection);
void           gtk_tlselection_select_range        (GtkTLSelection            *selection,
						    GtkTLPath                 *start_path,
						    GtkTLPath                 *end_path);




#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_TLSELECTION_H__ */

