/* gtktlview.c
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#include <gtk/gtk.h>
#include "gtktlview.h"
#include "grbtree.h"
#include "gtktlprivate.h"
#include "gtktlcell.h"
#include <gdk/gdkkeysyms.h>
#include "gtktlcolumn.h"


/* the width of the column resize windows */
#define TLVIEW_DRAG_WIDTH 6
#define TLVIEW_EXPANDER_WIDTH 14
#define TLVIEW_EXPANDER_HEIGHT 14
#define TLVIEW_VERTICAL_SEPERATOR 2
#define TLVIEW_HORIZONTAL_SEPERATOR 0


typedef struct _GtkTLViewChild GtkTLViewChild;

struct _GtkTLViewChild
{
  GtkWidget *widget;
  gint x;
  gint y;
};


static void     gtk_tlview_init                 (GtkTLView        *tlview);
static void     gtk_tlview_class_init           (GtkTLViewClass   *klass);


/* widget signals */
static void     gtk_tlview_set_model_realized   (GtkTLView        *tlview);
static void     gtk_tlview_realize              (GtkWidget        *widget);
static void     gtk_tlview_unrealize            (GtkWidget        *widget);
static void     gtk_tlview_map                  (GtkWidget        *widget);
static void     gtk_tlview_size_request         (GtkWidget        *widget,
						 GtkRequisition   *requisition);
static void     gtk_tlview_size_allocate        (GtkWidget        *widget,
						 GtkAllocation    *allocation);
static void     gtk_tlview_draw                 (GtkWidget        *widget,
						 GdkRectangle     *area);
static gboolean gtk_tlview_expose               (GtkWidget        *widget,
						 GdkEventExpose   *event);
static gboolean gtk_tlview_motion               (GtkWidget        *widget,
						 GdkEventMotion   *event);
static gboolean gtk_tlview_enter_notify         (GtkWidget        *widget,
						 GdkEventCrossing *event);
static gboolean gtk_tlview_leave_notify         (GtkWidget        *widget,
						 GdkEventCrossing *event);
static gboolean gtk_tlview_button_press         (GtkWidget        *widget,
						 GdkEventButton   *event);
static gboolean gtk_tlview_button_release       (GtkWidget        *widget,
						 GdkEventButton   *event);
static void     gtk_tlview_draw_focus           (GtkWidget        *widget);
static gint     gtk_tlview_focus_in             (GtkWidget        *widget,
						 GdkEventFocus    *event);
static gint     gtk_tlview_focus_out            (GtkWidget        *widget,
						 GdkEventFocus    *event);
static gint     gtk_tlview_focus                (GtkContainer     *container,
						 GtkDirectionType  direction);


/* container signals */
static void     gtk_tlview_remove               (GtkContainer     *container,
						 GtkWidget        *widget);
static void     gtk_tlview_forall               (GtkContainer     *container,
						 gboolean          include_internals,
						 GtkCallback       callback,
						 gpointer          callback_data);


/* tlmodel signals */
static void     gtk_tlview_set_adjustments      (GtkTLView        *tlview,
						 GtkAdjustment    *hadj,
						 GtkAdjustment    *vadj);
static void     gtk_tlview_node_changed         (GtkTLModel       *model,
						 GtkTLPath        *path,
						 GtkTLNode        *tlnode,
						 gpointer          data);
static void     gtk_tlview_node_inserted        (GtkTLModel       *model,
						 GtkTLPath        *path,
						 GtkTLNode        *tlnode,
						 gpointer          data);
static void     gtk_tlview_node_inserted_before (GtkTLModel       *model,
						 GtkTLPath        *path,
						 GtkTLNode        *tlnode,
						 gpointer          data);
static void     gtk_tlview_node_child_added     (GtkTLModel       *model,
						 GtkTLPath        *path,
						 GtkTLNode        *tlnode,
						 gpointer          data);
static void     gtk_tlview_node_deleted         (GtkTLModel       *model,
						 GtkTLPath        *path,
						 gpointer          data);


/* Internal functions */
static void     gtk_tlview_draw_arrow           (GtkTLView        *tlview,
						 GRBNode          *node,
						 gint              offset,
						 gint              x,
						 gint              y);
static gint     gtk_tlview_new_column_width     (GtkTLView        *tlview,
						 gint              i,
						 gint             *x);
static void     gtk_tlview_adjustment_changed   (GtkAdjustment    *adjustment,
						 GtkTLView        *tlview);
static void     gtk_tlview_build_tree           (GtkTLView        *tlview,
						 GRBTree          *tree,
						 GtkTLNode         node,
						 gint              depth,
						 gboolean          recurse,
						 gboolean          calc_bounds);
static void     gtk_tlview_calc_size            (GtkTLView        *priv,
						 GRBTree          *tree,
						 GtkTLNode         node,
						 gint              depth);
static gboolean gtk_tlview_discover_dirty_node  (GtkTLView        *tlview,
						 GtkTLNode         node,
						 gint              depth,
						 gint             *height);
static void     gtk_tlview_discover_dirty       (GtkTLView        *tlview,
						 GRBTree          *tree,
						 GtkTLNode         node,
						 gint              depth);
static void     gtk_tlview_check_dirty          (GtkTLView        *tlview);
static void     gtk_tlview_create_button        (GtkTLView        *tlview,
						 gint              i);
static void     gtk_tlview_create_buttons       (GtkTLView        *tlview);
static void     gtk_tlview_button_clicked       (GtkWidget        *widget,
						 gpointer          data);
static void     gtk_tlview_clamp_node_visible   (GtkTLView        *tlview,
						 GRBTree          *tree,
						 GRBNode          *node);


static GtkContainerClass *parent_class = NULL;


/* Class Functions */
GtkType
gtk_tlview_get_type (void)
{
  static GtkType tlview_type = 0;

  if (!tlview_type)
    {
      static const GTypeInfo tlview_info =
      {
        sizeof (GtkTLViewClass),
	NULL,		/* base_init */
	NULL,		/* base_finalize */
        (GClassInitFunc) gtk_tlview_class_init,
	NULL,		/* class_finalize */
	NULL,		/* class_data */
        sizeof (GtkTLView),
	0,              /* n_preallocs */
        (GInstanceInitFunc) gtk_tlview_init
      };

      tlview_type = g_type_register_static (GTK_TYPE_CONTAINER, "GtkTLView", &tlview_info);
    }

  return tlview_type;
}

static void
gtk_tlview_class_init (GtkTLViewClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  GtkContainerClass *container_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;
  container_class = (GtkContainerClass*) class;
  parent_class = g_type_class_peek_parent (class);

  widget_class->realize = gtk_tlview_realize;
  widget_class->unrealize = gtk_tlview_unrealize;
  widget_class->map = gtk_tlview_map;
  widget_class->size_request = gtk_tlview_size_request;
  widget_class->size_allocate = gtk_tlview_size_allocate;
  widget_class->draw = gtk_tlview_draw;
  widget_class->expose_event = gtk_tlview_expose;
//  widget_class->key_press_event = gtk_tlview_key_press;
  widget_class->motion_notify_event = gtk_tlview_motion;
  widget_class->enter_notify_event = gtk_tlview_enter_notify;
  widget_class->leave_notify_event = gtk_tlview_leave_notify;
  widget_class->button_press_event = gtk_tlview_button_press;
  widget_class->button_release_event = gtk_tlview_button_release;
  widget_class->draw_focus = gtk_tlview_draw_focus;
  widget_class->focus_in_event = gtk_tlview_focus_in;
  widget_class->focus_out_event = gtk_tlview_focus_out;

  container_class->forall = gtk_tlview_forall;
  container_class->remove = gtk_tlview_remove;
  container_class->focus = gtk_tlview_focus;

  class->set_scroll_adjustments = gtk_tlview_set_adjustments;

  widget_class->set_scroll_adjustments_signal =
    gtk_signal_new ("set_scroll_adjustments",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GtkTLViewClass, set_scroll_adjustments),
		    gtk_marshal_NONE__POINTER_POINTER,
		    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER, GTK_TYPE_POINTER);
}

static void
gtk_tlview_init (GtkTLView *tlview)
{
  tlview->priv = g_new0 (GtkTLViewPrivate, 1);

  GTK_WIDGET_UNSET_FLAGS (tlview, GTK_NO_WINDOW);
  GTK_WIDGET_SET_FLAGS (tlview, GTK_CAN_FOCUS);

  tlview->priv->flags = GTK_TLVIEW_IS_LIST | GTK_TLVIEW_SHOW_EXPANDERS | GTK_TLVIEW_DRAW_KEYFOCUS | GTK_TLVIEW_HEADERS_VISIBLE;
  tlview->priv->tab_offset = TLVIEW_EXPANDER_WIDTH;
  tlview->priv->columns = 0;
  tlview->priv->column = NULL;
  tlview->priv->button_pressed_node = NULL;
  tlview->priv->button_pressed_tree = NULL;
  tlview->priv->prelight_node = NULL;
  tlview->priv->prelight_offset = 0;
  tlview->priv->header_height = 1;
  tlview->priv->x_drag = 0;
  tlview->priv->drag_pos = -1;
  tlview->priv->selection = NULL;
  tlview->priv->anchor = NULL;
  tlview->priv->cursor = NULL;
  gtk_tlview_set_adjustments (tlview, NULL, NULL);
  gtk_tlview_set_size (tlview, 0, 0);
}

/* Widget methods
 */

static void
gtk_tlview_realize_buttons (GtkTLView *tlview)
{
  GList *list;
  GtkTLColumn *column;
  GdkWindowAttr attr;
  guint attributes_mask;

  if (!GTK_WIDGET_REALIZED (tlview) || tlview->priv->header_window == NULL)
    return;

  attr.window_type = GDK_WINDOW_CHILD;
  attr.wclass = GDK_INPUT_ONLY;
  attr.visual = gtk_widget_get_visual (GTK_WIDGET (tlview));
  attr.colormap = gtk_widget_get_colormap (GTK_WIDGET (tlview));
  attr.event_mask = gtk_widget_get_events (GTK_WIDGET (tlview));
  attr.event_mask = (GDK_BUTTON_PRESS_MASK |
			   GDK_BUTTON_RELEASE_MASK |
			   GDK_POINTER_MOTION_MASK |
			   GDK_POINTER_MOTION_HINT_MASK |
			   GDK_KEY_PRESS_MASK);
  attributes_mask = GDK_WA_CURSOR | GDK_WA_X | GDK_WA_Y;
  attr.cursor = gdk_cursor_new (GDK_SB_H_DOUBLE_ARROW);
  tlview->priv->cursor_drag = attr.cursor;

  attr.y = 0;
  attr.width = TLVIEW_DRAG_WIDTH;
  attr.height = tlview->priv->header_height;

  for (list = tlview->priv->column; list; list = list->next)
    {
      column = list->data;
      if (column->button)
	{
	  if (column->visible == FALSE)
	    continue;
	  gtk_widget_set_parent_window (column->button,
					tlview->priv->header_window);
	  gtk_widget_show (column->button);

	  attr.x = (column->button->allocation.x + column->button->allocation.width) - 3;

	  column->window = gdk_window_new (tlview->priv->header_window,
					   &attr, attributes_mask);
	  gdk_window_set_user_data (column->window, tlview);
	}
    }
}

static void
gtk_tlview_realize (GtkWidget *widget)
{
  GList *tmp_list;
  GtkTLView *tlview;
  GdkGCValues values;
  GdkWindowAttr attributes;
  gint attributes_mask;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (widget));

  tlview = GTK_TLVIEW (widget);

  if (!GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_MODEL_SETUP) &&
      tlview->priv->model)
    gtk_tlview_set_model_realized (tlview);

  gtk_tlview_check_dirty (GTK_TLVIEW (widget));
  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  /* Make the main, clipping window */
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = GDK_VISIBILITY_NOTIFY_MASK;

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new (gtk_widget_get_parent_window (widget),
				   &attributes, attributes_mask);
  gdk_window_set_user_data (widget->window, widget);

  /* Make the window for the tree */
  attributes.x = 0;
  attributes.y = 0;
  attributes.width = tlview->priv->width;
  attributes.height = tlview->priv->height + TLVIEW_HEADER_HEIGHT (tlview);
  attributes.event_mask = GDK_EXPOSURE_MASK |
    GDK_SCROLL_MASK |
    GDK_POINTER_MOTION_MASK |
    GDK_ENTER_NOTIFY_MASK |
    GDK_LEAVE_NOTIFY_MASK |
    GDK_BUTTON_PRESS_MASK |
    GDK_BUTTON_RELEASE_MASK |
    gtk_widget_get_events (widget);

  tlview->priv->bin_window = gdk_window_new (widget->window,
					     &attributes, attributes_mask);
  gdk_window_set_user_data (tlview->priv->bin_window, widget);

  /* Make the column header window */
  attributes.x = 0;
  attributes.y = 0;
  attributes.width = MAX (tlview->priv->width, widget->allocation.width);
  attributes.height = tlview->priv->header_height;
  attributes.event_mask = (GDK_EXPOSURE_MASK |
			   GDK_SCROLL_MASK |
			   GDK_BUTTON_PRESS_MASK |
			    GDK_BUTTON_RELEASE_MASK |
			    GDK_KEY_PRESS_MASK |
			    GDK_KEY_RELEASE_MASK) |
    gtk_widget_get_events (widget);

  tlview->priv->header_window = gdk_window_new (widget->window,
						&attributes, attributes_mask);
  gdk_window_set_user_data (tlview->priv->header_window, widget);


  values.foreground = (widget->style->white.pixel==0 ?
		       widget->style->black:widget->style->white);
  values.function = GDK_XOR;
  values.subwindow_mode = GDK_INCLUDE_INFERIORS;
  tlview->priv->xor_gc = gdk_gc_new_with_values (widget->window,
					  &values,
					  GDK_GC_FOREGROUND |
					  GDK_GC_FUNCTION |
					  GDK_GC_SUBWINDOW);
  /* Add them all up. */
  widget->style = gtk_style_attach (widget->style, widget->window);
  gdk_window_set_background (widget->window, &widget->style->base[widget->state]);
  gdk_window_set_background (tlview->priv->bin_window, &widget->style->base[widget->state]);
  gtk_style_set_background (widget->style, tlview->priv->header_window, GTK_STATE_NORMAL);

  tmp_list = tlview->priv->children;
  while (tmp_list)
  {
	  GtkTLViewChild *child = tmp_list->data;
	  tmp_list = tmp_list->next;

	  gtk_widget_set_parent_window (child->widget, tlview->priv->bin_window);
  }
  gtk_tlview_realize_buttons (GTK_TLVIEW (widget));
  gtk_tlview_set_size (GTK_TLVIEW (widget), -1, -1);
}

static void
gtk_tlview_unrealize (GtkWidget *widget)
{
  GtkTLView *tlview;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (widget));

  tlview = GTK_TLVIEW (widget);

  gdk_window_set_user_data (tlview->priv->bin_window, NULL);
  gdk_window_destroy (tlview->priv->bin_window);
  tlview->priv->bin_window = NULL;

  gdk_window_set_user_data (tlview->priv->header_window, NULL);
  gdk_window_destroy (tlview->priv->header_window);
  tlview->priv->header_window = NULL;

  gdk_gc_destroy (tlview->priv->xor_gc);
  if (GTK_WIDGET_CLASS (parent_class)->unrealize)
    (* GTK_WIDGET_CLASS (parent_class)->unrealize) (widget);
}

static void
gtk_tlview_map (GtkWidget *widget)
{
  GList *tmp_list;
  GtkTLView *tlview;
  GList *list;
  GtkTLColumn *column;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (widget));

  tlview = GTK_TLVIEW (widget);

  GTK_WIDGET_SET_FLAGS (widget, GTK_MAPPED);

  tmp_list = tlview->priv->children;
  while (tmp_list)
    {
      GtkTLViewChild *child = tmp_list->data;
      tmp_list = tmp_list->next;

      if (GTK_WIDGET_VISIBLE (child->widget))
	{
	  if (!GTK_WIDGET_MAPPED (child->widget))
	    gtk_widget_map (child->widget);
	}
    }
  gdk_window_show (tlview->priv->bin_window);
  if (GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_HEADERS_VISIBLE))
    {
      for (list = tlview->priv->column; list; list = list->next)
	{
	  column = list->data;
	  gtk_widget_map (column->button);
	}
      for (list = tlview->priv->column; list; list = list->next)
	{
	  column = list->data;
	  if (column->visible == FALSE)
	    continue;
	  if (column->column_type == GTK_TLCOLUMN_RESIZEABLE)
	    {
	      gdk_window_raise (column->window);
	      gdk_window_show (column->window);
	    }
	  else
	    gdk_window_hide (column->window);
	}
      gdk_window_show (tlview->priv->header_window);
    }
  gdk_window_show (widget->window);
}

static void
gtk_tlview_size_request (GtkWidget      *widget,
			 GtkRequisition *requisition)
{
  GtkTLView *tlview;
  GList *tmp_list;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (widget));

  tlview = GTK_TLVIEW (widget);

  requisition->width = 200;
  requisition->height = 200;

  tmp_list = tlview->priv->children;

  while (tmp_list)
    {
      GtkTLViewChild *child = tmp_list->data;
      GtkRequisition child_requisition;

      tmp_list = tmp_list->next;

      gtk_widget_size_request (child->widget, &child_requisition);
    }
}

static void
gtk_tlview_size_allocate_buttons (GtkWidget *widget)
{
  GtkTLView *tlview;
  GList *list;
  GList *last_column;
  GtkTLColumn *column;
  GtkAllocation allocation;
  gint width = 0;

  tlview = GTK_TLVIEW (widget);

  allocation.y = 0;
  allocation.height = tlview->priv->header_height;

  for (last_column = g_list_last (tlview->priv->column);
       last_column && !(GTK_TLCOLUMN (last_column->data)->visible);
       last_column = last_column->prev)
    ;

  if (last_column == NULL)
    return;

  for (list = tlview->priv->column; list != last_column; list = list->next)
    {
      column = list->data;

      if (!column->visible)
	continue;

      allocation.x = width;
      allocation.width = column->size;
      width += column->size;
      gtk_widget_size_allocate (column->button, &allocation);

      if (column->window)
	gdk_window_move (column->window, width - TLVIEW_DRAG_WIDTH/2, 0);
    }
  column = list->data;
  allocation.x = width;
  allocation.width = MAX (widget->allocation.width, tlview->priv->width) - width;
  gtk_widget_size_allocate (column->button, &allocation);
  if (column->window)
    gdk_window_move (column->window,
		     allocation.x +allocation.width - TLVIEW_DRAG_WIDTH/2,
		     0);
}

static void
gtk_tlview_size_allocate (GtkWidget     *widget,
			  GtkAllocation *allocation)
{
  GList *tmp_list;
  GtkTLView *tlview;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (widget));

  widget->allocation = *allocation;

  tlview = GTK_TLVIEW (widget);

  tmp_list = tlview->priv->children;

  while (tmp_list)
    {
      GtkAllocation allocation;
      GtkRequisition requisition;

      GtkTLViewChild *child = tmp_list->data;
      tmp_list = tmp_list->next;

      allocation.x = child->x;
      allocation.y = child->y;
      gtk_widget_get_child_requisition (child->widget, &requisition);
      allocation.width = requisition.width;
      allocation.height = requisition.height;

      gtk_widget_size_allocate (child->widget, &allocation);
    }

  if (GTK_WIDGET_REALIZED (widget))
    {
      gdk_window_move_resize (widget->window,
			      allocation->x, allocation->y,
			      allocation->width, allocation->height);
      gdk_window_move_resize (tlview->priv->header_window,
			      0, 0,
			      MAX (tlview->priv->width, allocation->width),
			      tlview->priv->header_height);
    }

  tlview->priv->hadjustment->page_size = allocation->width;
  tlview->priv->hadjustment->page_increment = allocation->width / 2;
  tlview->priv->hadjustment->lower = 0;
  tlview->priv->hadjustment->upper = tlview->priv->width;
  if (tlview->priv->hadjustment->value + allocation->width > tlview->priv->width)
    tlview->priv->hadjustment->value = MAX (tlview->priv->width - allocation->width, 0);
  gtk_signal_emit_by_name (GTK_OBJECT (tlview->priv->hadjustment), "changed");

  tlview->priv->vadjustment->page_size = allocation->height - TLVIEW_HEADER_HEIGHT (tlview);
  tlview->priv->vadjustment->page_increment = (allocation->height - TLVIEW_HEADER_HEIGHT (tlview)) / 2;
  tlview->priv->vadjustment->lower = 0;
  tlview->priv->vadjustment->upper = tlview->priv->height;
  if (tlview->priv->vadjustment->value + allocation->height > tlview->priv->height)
    gtk_adjustment_set_value (tlview->priv->vadjustment,
			      (gfloat) MAX (tlview->priv->height - allocation->height, 0));
  gtk_signal_emit_by_name (GTK_OBJECT (tlview->priv->vadjustment), "changed");

  if (GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_MODEL_SETUP))
    gtk_tlview_size_allocate_buttons (widget);
}

static void
gtk_tlview_draw (GtkWidget    *widget,
		 GdkRectangle *area)
{
  GList *tmp_list;
  GtkTLView *tlview;
  GtkTLColumn *column;
  GdkRectangle child_area;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (widget));

  tlview = GTK_TLVIEW (widget);

  /* We don't have any way of telling themes about this properly,
   * so we just assume a background pixmap
   */
  if (!GTK_WIDGET_APP_PAINTABLE (widget))
    {
      gdk_window_clear_area (tlview->priv->bin_window,
			     area->x, area->y, area->width, area->height);
      gdk_window_clear_area (tlview->priv->header_window,
			     area->x, area->y, area->width, area->height);
    }

  tmp_list = tlview->priv->children;
  while (tmp_list)
    {
      GtkTLViewChild *child = tmp_list->data;
      tmp_list = tmp_list->next;

      if (gtk_widget_intersect (child->widget, area, &child_area))
	gtk_widget_draw (child->widget, &child_area);
    }
  for (tmp_list = tlview->priv->column; tmp_list; tmp_list = tmp_list->next)
    {
      column = tmp_list->data;
      if (!column->visible)
	continue;
      if (column->button &&
	  gtk_widget_intersect(column->button, area, &child_area))
	gtk_widget_draw (column->button, &child_area);
    }
}

/* Warning: Very scary function.
 * Modify at your own risk
 */
static gboolean
gtk_tlview_bin_expose (GtkWidget      *widget,
		       GdkEventExpose *event)
{
  GtkTLView *tlview;
  GtkTLPath *path;
  GRBTree *tree;
  GList *list;
  GRBNode *node, *last_node = NULL;
  GRBNode *cursor = NULL;
  GRBTree *cursor_tree = NULL, *last_tree = NULL;
  GtkTLNode tlnode;
  GtkTLCell *cell;
  gint new_y;
  gint y_offset, x_offset, cell_offset;
  gint i, max_height;
  gint depth;
  GdkRectangle background_area;
  GdkRectangle cell_area;
  guint flags;
  gboolean last_selected;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TLVIEW (widget), FALSE);

  tlview = GTK_TLVIEW (widget);

  if (tlview->priv->tree == NULL)
    return TRUE;

  gtk_tlview_check_dirty (GTK_TLVIEW (widget));
  /* we want to account for a potential HEADER offset.
   * That is, if the header exists, we want to offset our event by its
   * height to find the right node.
   */
  new_y = (event->area.y<TLVIEW_HEADER_HEIGHT (tlview))?TLVIEW_HEADER_HEIGHT (tlview):event->area.y;
  y_offset = -g_rbtree_find_offset (tlview->priv->tree,
				    new_y - TLVIEW_HEADER_HEIGHT (tlview),
				    &tree,
				    &node) + new_y - event->area.y;
  if (node == NULL)
    return TRUE;

  /* See if the last node was selected */
  g_rbtree_prev_full (tree, node, &last_tree, &last_node);
  last_selected = (last_node && G_RBNODE_FLAG_SET (last_node, G_RBNODE_IS_SELECTED));

  /* find the path for the node */
  path = gtk_tlview_find_path ((GtkTLView *)widget,
			       tree,
			       node);
  tlnode = gtk_tlmodel_get_node (tlview->priv->model, path);
  depth = gtk_tlpath_get_depth (path);
  gtk_tlpath_free (path);

  if (tlview->priv->cursor)
    gtk_tlview_find_node (tlview, tlview->priv->cursor, &cursor_tree, &cursor);

  /* Actually process the expose event.  To do this, we want to
   * start at the first node of the event, and walk the tree in
   * order, drawing each successive node.
   */

  do
    {
      /* Need to think about this more.
      if (tlview->priv->show_expanders)
	max_height = MAX (TLVIEW_EXPANDER_MIN_HEIGHT, G_RBNODE_GET_HEIGHT (node));
      else
      */
      max_height = G_RBNODE_GET_HEIGHT (node);

      x_offset = -event->area.x;
      cell_offset = 0;

      background_area.y = y_offset + event->area.y + TLVIEW_VERTICAL_SEPERATOR;
      background_area.height = max_height - TLVIEW_VERTICAL_SEPERATOR;
      flags = 0;

      if (G_RBNODE_FLAG_SET (node, G_RBNODE_IS_PRELIT))
	flags |= GTK_TLCELL_PRELIT;

      if (G_RBNODE_FLAG_SET (node, G_RBNODE_IS_SELECTED))
	{
	  flags |= GTK_TLCELL_SELECTED;

	  /* Draw the selection */
	  gdk_draw_rectangle (event->window,
			      GTK_WIDGET (tlview)->style->bg_gc [GTK_STATE_SELECTED],
			      TRUE,
			      event->area.x,
			      background_area.y - (last_selected?TLVIEW_VERTICAL_SEPERATOR:0),
			      event->area.width,
			      background_area.height + (last_selected?TLVIEW_VERTICAL_SEPERATOR:0));
	  last_selected = TRUE;
	}
      else
	{
	  last_selected = FALSE;
	}

      for (i = 0, list = tlview->priv->column; i < tlview->priv->columns; i++, list = list->next)
	{
	  GtkTLColumn *column = list->data;

	  if (!column->visible)
	    continue;

	  cell = column->cell;
	  gtk_tlcolumn_set_cell_data (column,
				      tlview->priv->model,
				      tlnode);

	  background_area.x = cell_offset;
	  background_area.width = TLVIEW_COLUMN_SIZE (column);
	  if (i == 0 && TLVIEW_DRAW_EXPANDERS(tlview))
	    {
	      cell_area = background_area;
	      cell_area.x += depth*tlview->priv->tab_offset;
	      cell_area.width -= depth*tlview->priv->tab_offset;
	      gtk_tlcell_render (cell,
				 event->window,
				 widget,
				 &background_area,
				 &cell_area,
				 &event->area,
				 flags);
	      if ((node->flags & G_RBNODE_IS_PARENT) == G_RBNODE_IS_PARENT)
		{
		  gint x, y;
		  gdk_window_get_pointer (tlview->priv->bin_window, &x, &y, 0);
		  gtk_tlview_draw_arrow (GTK_TLVIEW (widget),
					 node,
					 event->area.y + y_offset,
					 x, y);
		}
	    }
	  else
	    {
	      cell_area = background_area;
	      gtk_tlcell_render (cell,
				 event->window,
				 widget,
				 &background_area,
				 &cell_area,
				 &event->area,
				 flags);
	    }
	  cell_offset += TLVIEW_COLUMN_SIZE (column);
	}

      if (node == cursor &&
	  GTK_WIDGET_HAS_FOCUS (widget))
	gtk_tlview_draw_focus (widget);

      y_offset += max_height;
      if (node->children)
	{
	  tree = node->children;
	  node = tree->root;
	  while (node->left != tree->nil)
	    node = node->left;
	  tlnode = gtk_tlmodel_node_children (tlview->priv->model, tlnode);
	  cell = gtk_tlview_get_column (tlview, 0)->cell;
	  depth++;

	  /* Sanity Check! */
	  TLVIEW_INTERNAL_ASSERT (tlnode != NULL, FALSE);
	}
      else
	{
	  gboolean done = FALSE;
	  do
	    {
	      node = g_rbtree_next (tree, node);
	      if (node != NULL)
		{
		  gtk_tlmodel_node_next (tlview->priv->model, &tlnode);
		  cell = gtk_tlview_get_column (tlview, 0)->cell;
		  done = TRUE;

		  /* Sanity Check! */
		  TLVIEW_INTERNAL_ASSERT (tlnode != NULL, FALSE);
		}
	      else
		{
		  node = tree->parent_node;
		  tree = tree->parent_tree;
		  if (tree == NULL)
		    /* we've run out of tree.  It's okay though, as we'd only break
		     * out of the while loop below. */
		    return TRUE;
		  tlnode = gtk_tlmodel_node_parent (tlview->priv->model, tlnode);
		  depth--;

		  /* Sanity check */
		  TLVIEW_INTERNAL_ASSERT (tlnode != NULL, FALSE);
		}
	    }
	  while (!done);
	}
    }
  while (y_offset < event->area.height);

  return TRUE;
}

static gboolean
gtk_tlview_expose (GtkWidget      *widget,
		   GdkEventExpose *event)
{
  GtkTLView *tlview;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TLVIEW (widget), FALSE);

  tlview = GTK_TLVIEW (widget);

  if (event->window == tlview->priv->bin_window)
    return gtk_tlview_bin_expose (widget, event);

  return TRUE;
}

static gboolean
gtk_tlview_motion (GtkWidget *widget,
		   GdkEventMotion  *event)
{
  GtkTLView *tlview;
  GRBTree *tree;
  GRBNode *node;
  gint new_y;
  gint y_offset;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TLVIEW (widget), FALSE);

  tlview = GTK_TLVIEW (widget);

  if (GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_IN_COLUMN_RESIZE))
    {
      gint x;
      gint new_width;

      if (event->is_hint || event->window != widget->window)
	gtk_widget_get_pointer (widget, &x, NULL);
      else
	x = event->x;

      new_width = gtk_tlview_new_column_width (GTK_TLVIEW (widget), tlview->priv->drag_pos, &x);
      if (x != tlview->priv->x_drag)
	{
	  gtk_tlcolumn_set_size (gtk_tlview_get_column (GTK_TLVIEW (widget), tlview->priv->drag_pos), new_width);
	}

      /* FIXME: We need to scroll */
      gtk_tlview_set_size (GTK_TLVIEW (widget), -1, tlview->priv->height);
      return FALSE;
    }

  /* Sanity check it */
  if (event->window != tlview->priv->bin_window)
    return FALSE;
  if (tlview->priv->tree == NULL)
    return FALSE;

  if (tlview->priv->prelight_node != NULL)
    {
      if ((((gint) event->y - TLVIEW_HEADER_HEIGHT (tlview) < tlview->priv->prelight_offset) ||
	   ((gint) event->y - TLVIEW_HEADER_HEIGHT (tlview) >=
	    (tlview->priv->prelight_offset + G_RBNODE_GET_HEIGHT (tlview->priv->prelight_node))) ||
	   ((gint) event->x > tlview->priv->tab_offset)))
	    /* We need to unprelight the old one. */
	{
	  if (GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_ARROW_PRELIT))
	    {
	      G_RBNODE_UNSET_FLAG (tlview->priv->prelight_node, G_RBNODE_IS_PRELIT);
	      gtk_tlview_draw_arrow (GTK_TLVIEW (widget),
				     tlview->priv->prelight_node,
				     tlview->priv->prelight_offset,
				     event->x,
				     event->y);
	      GTK_TLVIEW_UNSET_FLAG (tlview, GTK_TLVIEW_ARROW_PRELIT);
	    }

	  G_RBNODE_UNSET_FLAG (tlview->priv->prelight_node, G_RBNODE_IS_PRELIT);
	  tlview->priv->prelight_node = NULL;
	  tlview->priv->prelight_tree = NULL;
	  tlview->priv->prelight_offset = 0;
	}
    }

  new_y = ((gint)event->y<TLVIEW_HEADER_HEIGHT (tlview))?TLVIEW_HEADER_HEIGHT (tlview):(gint)event->y;
  y_offset = -g_rbtree_find_offset (tlview->priv->tree, new_y - TLVIEW_HEADER_HEIGHT (tlview),
				    &tree,
				    &node) + new_y - (gint)event->y;

  if (node == NULL)
    return TRUE;

  /* If we are currently pressing down a button, we don't want to prelight anything else. */
  if ((tlview->priv->button_pressed_node != NULL) &&
      (tlview->priv->button_pressed_node != node))
    return TRUE;

  /* Do we want to prelight a tab? */
  GTK_TLVIEW_UNSET_FLAG (tlview, GTK_TLVIEW_ARROW_PRELIT);
  if (event->x <= tlview->priv->tab_offset &&
      event->x >= 0 &&
      ((node->flags & G_RBNODE_IS_PARENT) == G_RBNODE_IS_PARENT))
    {
      tlview->priv->prelight_offset = event->y+y_offset;
      GTK_TLVIEW_SET_FLAG (tlview, GTK_TLVIEW_ARROW_PRELIT);
    }

  tlview->priv->prelight_node = node;
  tlview->priv->prelight_tree = tree;
  tlview->priv->prelight_offset = event->y+y_offset;

  G_RBNODE_SET_FLAG (node, G_RBNODE_IS_PRELIT);
  gtk_widget_queue_draw (widget);

  return TRUE;
}

/* Is this function necessary? Can I get an enter_notify event w/o either
 * an expose event or a mouse motion event?
 */
static gboolean
gtk_tlview_enter_notify (GtkWidget        *widget,
			 GdkEventCrossing *event)
{
  GtkTLView *tlview;
  GRBTree *tree;
  GRBNode *node;
  gint new_y;
  gint y_offset;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TLVIEW (widget), FALSE);

  tlview = GTK_TLVIEW (widget);

  /* Sanity check it */
  if (event->window != tlview->priv->bin_window)
    return FALSE;
  if (tlview->priv->tree == NULL)
    return FALSE;

  if ((tlview->priv->button_pressed_node != NULL) &&
      (tlview->priv->button_pressed_node != node))
    return TRUE;

  /* find the node internally */
  new_y = ((gint)event->y<TLVIEW_HEADER_HEIGHT (tlview))?TLVIEW_HEADER_HEIGHT (tlview):(gint)event->y;
  y_offset = -g_rbtree_find_offset (tlview->priv->tree,
				    new_y - TLVIEW_HEADER_HEIGHT (tlview),
				    &tree,
				    &node) + new_y - (gint)event->y;

  if (node == NULL)
       return FALSE;

  /* Do we want to prelight a tab? */
  GTK_TLVIEW_UNSET_FLAG (tlview, GTK_TLVIEW_ARROW_PRELIT);
  if (event->x <= tlview->priv->tab_offset &&
      event->x >= 0 &&
      ((node->flags & G_RBNODE_IS_PARENT) == G_RBNODE_IS_PARENT))
    {
      tlview->priv->prelight_offset = event->y+y_offset;
      GTK_TLVIEW_SET_FLAG (tlview, GTK_TLVIEW_ARROW_PRELIT);
    }

  tlview->priv->prelight_node = node;
  tlview->priv->prelight_tree = tree;
  tlview->priv->prelight_offset = event->y+y_offset;

  G_RBNODE_SET_FLAG (node, G_RBNODE_IS_PRELIT);
  gtk_widget_queue_draw (widget);

  return TRUE;
}

static gboolean
gtk_tlview_leave_notify (GtkWidget        *widget,
			 GdkEventCrossing *event)
{
  GtkTLView *tlview;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TLVIEW (widget), FALSE);

  tlview = GTK_TLVIEW (widget);

  if (tlview->priv->prelight_node != NULL)
    {
      G_RBNODE_UNSET_FLAG (tlview->priv->prelight_node, G_RBNODE_IS_PRELIT);
      tlview->priv->prelight_node = NULL;
      tlview->priv->prelight_tree = NULL;
      tlview->priv->prelight_offset = 0;
      GTK_TLVIEW_SET_FLAG (tlview, GTK_TLVIEW_ARROW_PRELIT);
      gtk_widget_queue_draw (widget);
    }
  return TRUE;
}

static gboolean
gtk_tlview_button_press (GtkWidget      *widget,
			 GdkEventButton *event)
{
  GtkTLView *tlview;
  GList *list;
  GtkTLColumn *column;
  gint i;
  GdkRectangle background_area;
  GdkRectangle cell_area;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TLVIEW (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  tlview = GTK_TLVIEW (widget);

  if (event->window == tlview->priv->bin_window)
    {
      GRBNode *node;
      GRBTree *tree;
      GtkTLPath *path;
      gchar *path_string;
      gint depth;
      gint new_y;
      gint y_offset;

      if (!GTK_WIDGET_HAS_FOCUS (widget))
	gtk_widget_grab_focus (widget);
      GTK_TLVIEW_UNSET_FLAG (tlview, GTK_TLVIEW_DRAW_KEYFOCUS);
      /* are we in an arrow? */
      if (tlview->priv->prelight_node != FALSE && GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_ARROW_PRELIT))
	{
	  if (event->button == 1)
	    {
	      gtk_grab_add (widget);
	      tlview->priv->button_pressed_node = tlview->priv->prelight_node;
	      tlview->priv->button_pressed_tree = tlview->priv->prelight_tree;
	      gtk_tlview_draw_arrow (GTK_TLVIEW (widget),
				     tlview->priv->prelight_node,
				     tlview->priv->prelight_offset,
				     event->x,
				     event->y);
	    }
	  return TRUE;
	}

      /* find the node that was clicked */
      new_y = ((gint)event->y<TLVIEW_HEADER_HEIGHT (tlview))?TLVIEW_HEADER_HEIGHT (tlview):(gint)event->y;
      y_offset = -g_rbtree_find_offset (tlview->priv->tree,
					new_y - TLVIEW_HEADER_HEIGHT (tlview),
					&tree,
					&node) + new_y - (gint)event->y;

      if (node == NULL)
	/* We clicked in dead space */
	return TRUE;

      /* Get the path and the node */
      path = gtk_tlview_find_path (tlview, tree, node);
      depth = gtk_tlpath_get_depth (path);
      background_area.y = y_offset + event->y + TLVIEW_VERTICAL_SEPERATOR;
      background_area.height = G_RBNODE_GET_HEIGHT (node) - TLVIEW_VERTICAL_SEPERATOR;
      background_area.x = 0;
      /* Let the cell have a chance at selecting it. */

      for (i = 0, list = tlview->priv->column; i < tlview->priv->columns; i++, list = list->next)
	{
	  GtkTLColumn *column = list->data;
	  GtkTLCell *cell;
	  GtkTLNode *tlnode;

	  if (!column->visible)
	    continue;

	  background_area.width = TLVIEW_COLUMN_SIZE (column);
	  if (i == 0 && TLVIEW_DRAW_EXPANDERS(tlview))
	    {
	      cell_area = background_area;
	      cell_area.x += depth*tlview->priv->tab_offset;
	      cell_area.width -= depth*tlview->priv->tab_offset;
	    }
	  else
	    {
	      cell_area = background_area;
	    }

	  cell = column->cell;

	  if ((background_area.x > (gint) event->x) ||
	      (background_area.y > (gint) event->y) ||
	      (background_area.x + background_area.width <= (gint) event->x) ||
	      (background_area.y + background_area.height <= (gint) event->y))
	    {
	      background_area.x += background_area.width;
	      continue;
	    }

	  tlnode = gtk_tlmodel_get_node (tlview->priv->model,
					 path);
	  gtk_tlcolumn_set_cell_data (column,
				      tlview->priv->model,
				      tlnode);

	  path_string = gtk_tlpath_get_string (path);
	  if (gtk_tlcell_event (cell,
				(GdkEvent *)event,
				widget,
				path_string,
				&background_area,
				&cell_area,
				0))

	    {
	      g_free (path_string);
	      gtk_tlpath_free (path);
	      return TRUE;
	    }
	  else
	    {
	      g_free (path_string);
	      break;
	    }
	}
      /* Handle the selection */
      if (tlview->priv->selection == NULL)
	gtk_tlselection_new_with_tlview (tlview);

      gtk_tlselection_internal_select_node (tlview->priv->selection,
					    node,
					    tree,
					    path,
					    event->state);
      gtk_tlpath_free (path);
      return TRUE;
    }

  for (i = 0, list = tlview->priv->column; list; list = list->next, i++)
    {
      column = list->data;
      if (event->window == column->window &&
	  column->column_type == GTK_TLCOLUMN_RESIZEABLE &&
	  column->window)
	{
	  gpointer drag_data;

	  if (gdk_pointer_grab (column->window, FALSE,
				GDK_POINTER_MOTION_HINT_MASK |
				GDK_BUTTON1_MOTION_MASK |
				GDK_BUTTON_RELEASE_MASK,
				NULL, NULL, event->time))
	    return FALSE;

	  gtk_grab_add (widget);
	  GTK_TLVIEW_SET_FLAG (tlview, GTK_TLVIEW_IN_COLUMN_RESIZE);

	  /* block attached dnd signal handler */
	  drag_data = gtk_object_get_data (GTK_OBJECT (widget), "gtk-site-data");
	  if (drag_data)
	    gtk_signal_handler_block_by_data (GTK_OBJECT (widget), drag_data);

	  if (!GTK_WIDGET_HAS_FOCUS (widget))
	    gtk_widget_grab_focus (widget);

	  tlview->priv->drag_pos = i;
	  tlview->priv->x_drag = (column->button->allocation.x + column->button->allocation.width);
	}
    }
  return TRUE;
}

static gboolean
gtk_tlview_button_release (GtkWidget      *widget,
			   GdkEventButton *event)
{
  GtkTLView *tlview;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TLVIEW (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  tlview = GTK_TLVIEW (widget);

  if (GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_IN_COLUMN_RESIZE))
    {
      gpointer drag_data;
      gint width;
      gint x;
      gint i;

      i = tlview->priv->drag_pos;
      tlview->priv->drag_pos = -1;

      /* unblock attached dnd signal handler */
      drag_data = gtk_object_get_data (GTK_OBJECT (widget), "gtk-site-data");
      if (drag_data)
	gtk_signal_handler_unblock_by_data (GTK_OBJECT (widget), drag_data);

      GTK_TLVIEW_UNSET_FLAG (tlview, GTK_TLVIEW_IN_COLUMN_RESIZE);
      gtk_widget_get_pointer (widget, &x, NULL);
      gtk_grab_remove (widget);
      gdk_pointer_ungrab (event->time);

      width = gtk_tlview_new_column_width (GTK_TLVIEW (widget), i, &x);
      gtk_tlcolumn_set_size (gtk_tlview_get_column (GTK_TLVIEW (widget), i), width);
      return FALSE;
    }

  if (tlview->priv->button_pressed_node == NULL)
    return FALSE;

  if (event->button == 1)
    {
      gtk_grab_remove (widget);
      if (tlview->priv->button_pressed_node == tlview->priv->prelight_node && GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_ARROW_PRELIT))
	{
	  GtkTLPath *path;
	  GtkTLNode *tlnode;

	  /* Actually activate the node */
	  if (tlview->priv->button_pressed_node->children == NULL)
	    {
	      path = gtk_tlview_find_path (GTK_TLVIEW (widget),
					   tlview->priv->button_pressed_tree,
					   tlview->priv->button_pressed_node);
	      tlview->priv->button_pressed_node->children = g_rbtree_new ();
	      tlview->priv->button_pressed_node->children->parent_tree = tlview->priv->button_pressed_tree;
	      tlview->priv->button_pressed_node->children->parent_node = tlview->priv->button_pressed_node;
	      tlnode = gtk_tlmodel_get_node (tlview->priv->model, path);
	      tlnode = gtk_tlmodel_node_children (tlview->priv->model, tlnode);

	      gtk_tlview_build_tree (tlview,
				     tlview->priv->button_pressed_node->children,
				     tlnode,
				     gtk_tlpath_get_depth (path) + 1,
				     FALSE,
				     GTK_WIDGET_REALIZED (widget));
	    }
	  else
	    {
	      path = gtk_tlview_find_path (GTK_TLVIEW (widget),
					   tlview->priv->button_pressed_node->children,
					   tlview->priv->button_pressed_node->children->root);
	      tlnode = gtk_tlmodel_get_node (tlview->priv->model, path);

	      gtk_tlview_discover_dirty (GTK_TLVIEW (widget),
					 tlview->priv->button_pressed_node->children,
					 tlnode,
					 gtk_tlpath_get_depth (path));
	      g_rbtree_remove (tlview->priv->button_pressed_node->children);
	    }
	  gtk_tlpath_free (path);

	  gtk_tlview_set_size (GTK_TLVIEW (widget), -1, -1);
	  gtk_widget_queue_resize (widget);
	}

      tlview->priv->button_pressed_node = NULL;
    }

  return TRUE;
}


static void
gtk_tlview_draw_focus (GtkWidget *widget)
{
  GtkTLView *tlview;
  GRBTree *cursor_tree = NULL;
  GRBNode *cursor = NULL;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (widget));

  tlview = GTK_TLVIEW (widget);

  if (! GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_DRAW_KEYFOCUS))
    return;
  if (tlview->priv->cursor == NULL)
    return;

  gtk_tlview_find_node (tlview, tlview->priv->cursor, &cursor_tree, &cursor);
  if (cursor == NULL)
    return;

  gdk_draw_rectangle (tlview->priv->bin_window,
		      widget->style->fg_gc[GTK_STATE_NORMAL],
		      FALSE,
		      0,
		      g_rbtree_node_find_offset (cursor_tree, cursor) + TLVIEW_HEADER_HEIGHT (tlview),
		      (gint) MAX (tlview->priv->width, tlview->priv->hadjustment->upper),
		      G_RBNODE_GET_HEIGHT (cursor));
}


static gint
gtk_tlview_focus_in (GtkWidget     *widget,
		     GdkEventFocus *event)
{
  GtkTLView *tlview;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TLVIEW (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  tlview = GTK_TLVIEW (widget);

  GTK_WIDGET_SET_FLAGS (widget, GTK_HAS_FOCUS);

  gtk_widget_draw_focus (widget);

  return FALSE;
}


static gint
gtk_tlview_focus_out (GtkWidget     *widget,
		      GdkEventFocus *event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TLVIEW (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  GTK_WIDGET_UNSET_FLAGS (widget, GTK_HAS_FOCUS);

  gtk_widget_queue_draw (widget);

  return FALSE;
}

/* FIXME: It would be neat to someday make the headers a seperate widget that
 * can be shared between various apps
 */
/* Returns TRUE if the focus is within the headers, after the focus operation is
 * done
 */
static gboolean
gtk_tlview_header_focus (GtkTLView        *tlview,
			 GtkDirectionType  dir)
{
  GtkWidget *focus_child;
  GtkContainer *container;

  GList *last_column, *first_column;
  GList *tmp_list;

  if (! GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_HEADERS_VISIBLE))
    return FALSE;

  focus_child = GTK_CONTAINER (tlview)->focus_child;
  container = GTK_CONTAINER (tlview);

  for (last_column = g_list_last (tlview->priv->column);
       last_column &&
	 !(GTK_TLCOLUMN (last_column->data)->visible) &&
	 GTK_WIDGET_CAN_FOCUS (GTK_TLCOLUMN (last_column->data)->button);
       last_column = last_column->prev)
    ;

  for (first_column = tlview->priv->column;
       first_column &&
	 !(GTK_TLCOLUMN (first_column->data)->visible) &&
	 GTK_WIDGET_CAN_FOCUS (GTK_TLCOLUMN (first_column->data)->button);
       first_column = first_column->next)
    ;

    /* no headers are visible, or are focussable.  We can't focus in or out.
     * I wonder if focussable is a real word...
     */
  if (last_column == NULL)
    {
      gtk_container_set_focus_child (container, NULL);
      return FALSE;
    }

  /* First thing we want to handle is entering and leaving the headers.
   */
  switch (dir)
    {
    case GTK_DIR_TAB_BACKWARD:
      if (!focus_child)
	{
	  focus_child = GTK_TLCOLUMN (last_column->data)->button;
	  gtk_container_set_focus_child (container, focus_child);
	  gtk_widget_grab_focus (focus_child);
	  goto cleanup;
	}
      if (focus_child == GTK_TLCOLUMN (first_column->data)->button)
	{
	  focus_child = NULL;
	  goto cleanup;
	}
      break;

    case GTK_DIR_TAB_FORWARD:
      if (!focus_child)
	{
	  focus_child = GTK_TLCOLUMN (first_column->data)->button;
	  gtk_container_set_focus_child (container, focus_child);
	  gtk_widget_grab_focus (focus_child);
	  goto cleanup;
	}
      if (focus_child == GTK_TLCOLUMN (last_column->data)->button)
	{
	  focus_child = NULL;
	  goto cleanup;
	}
      break;

    case GTK_DIR_LEFT:
      if (!focus_child)
	{
	  focus_child = GTK_TLCOLUMN (last_column->data)->button;
	  gtk_container_set_focus_child (container, focus_child);
	  gtk_widget_grab_focus (focus_child);
	  goto cleanup;
	}
      if (focus_child == GTK_TLCOLUMN (first_column->data)->button)
	{
	  focus_child = NULL;
	  goto cleanup;
	}
      break;

    case GTK_DIR_RIGHT:
      if (!focus_child)
	{
	  focus_child = GTK_TLCOLUMN (first_column->data)->button;
	  gtk_container_set_focus_child (container, focus_child);
	  gtk_widget_grab_focus (focus_child);
	  goto cleanup;
	}
      if (focus_child == GTK_TLCOLUMN (last_column->data)->button)
	{
	  focus_child = NULL;
	  goto cleanup;
	}
      break;

    case GTK_DIR_UP:
      if (!focus_child)
	{
	  focus_child = GTK_TLCOLUMN (first_column->data)->button;
	  gtk_container_set_focus_child (container, focus_child);
	  gtk_widget_grab_focus (focus_child);
	}
      else
	{
	  focus_child = NULL;
	}
      goto cleanup;

    case GTK_DIR_DOWN:
      if (!focus_child)
	{
	  focus_child = GTK_TLCOLUMN (first_column->data)->button;
	  gtk_container_set_focus_child (container, focus_child);
	  gtk_widget_grab_focus (focus_child);
	}
      else
	{
	  focus_child = NULL;
	}
      goto cleanup;
    }

  /* We need to move the focus to the next button. */
  if (focus_child)
    {
      for (tmp_list = tlview->priv->column; tmp_list; tmp_list = tmp_list->next)
	if (GTK_TLCOLUMN (tmp_list->data)->button == focus_child)
	  {
	    if (gtk_container_focus (GTK_CONTAINER (GTK_TLCOLUMN (tmp_list->data)->button), dir))
	      {
		/* The focus moves inside the button. */
		/* This is probably a great example of bad UI */
		goto cleanup;
	      }
	    break;
	  }

      /* We need to move the focus among the row of buttons. */
      while (tmp_list)
	{
	  GtkTLColumn *column;

	  if (dir == GTK_DIR_RIGHT || dir == GTK_DIR_TAB_FORWARD)
	    tmp_list = tmp_list->next;
	  else
	    tmp_list = tmp_list->prev;

	  if (tmp_list == NULL)
	    {
	      g_warning ("Internal button not found");
	      goto cleanup;
	    }
	  column = tmp_list->data;
	  if (column->button &&
	      column->visible &&
	      GTK_WIDGET_CAN_FOCUS (column->button))
	    {
	      focus_child = column->button;
	      gtk_container_set_focus_child (container, column->button);
	      gtk_widget_grab_focus (column->button);
	      break;
	    }
	}
    }

 cleanup:
  /* if focus child is non-null, we assume it's been set to the current focus child
   */
  if (focus_child)
    {
      /* If the following isn't true, then the view is smaller then the scrollpane.
       */
      if ((focus_child->allocation.x + focus_child->allocation.width) <=
	  (tlview->priv->hadjustment->upper))
	{
	  /* Scroll to the button, if needed */
	  if ((tlview->priv->hadjustment->value + tlview->priv->hadjustment->page_size) <
	      (focus_child->allocation.x + focus_child->allocation.width))
	    gtk_adjustment_set_value (tlview->priv->hadjustment,
				      focus_child->allocation.x + focus_child->allocation.width -
				      tlview->priv->hadjustment->page_size);
	  else if (tlview->priv->hadjustment->value > focus_child->allocation.x)
	    gtk_adjustment_set_value (tlview->priv->hadjustment,
				      focus_child->allocation.x);
	}
    }
  else
    {
      gtk_container_set_focus_child (container, NULL);
    }

  return (focus_child != NULL);
}

/* WARNING: Scary function */
static gint
gtk_tlview_focus (GtkContainer     *container,
		  GtkDirectionType  direction)
{
  GtkTLView *tlview;
  GtkWidget *focus_child;
  GdkEvent *event;
  GRBTree *cursor_tree;
  GRBNode *cursor_node;

  g_return_val_if_fail (container != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TLVIEW (container), FALSE);
  g_return_val_if_fail (GTK_WIDGET_VISIBLE (container), FALSE);

  tlview = GTK_TLVIEW (container);

  if (!GTK_WIDGET_IS_SENSITIVE (container))
    return FALSE;
  if (tlview->priv->tree == NULL)
    return FALSE;

  focus_child = container->focus_child;

  /* Case 1.  Headers have focus. */
  if (focus_child)
    {
      switch (direction)
	{
	case GTK_DIR_LEFT:
	case GTK_DIR_TAB_BACKWARD:
	  return (gtk_tlview_header_focus (tlview, direction));
	case GTK_DIR_UP:
	  gtk_container_set_focus_child (container, NULL);
	  return FALSE;
	case GTK_DIR_TAB_FORWARD:
	case GTK_DIR_RIGHT:
	case GTK_DIR_DOWN:
	  if (direction == GTK_DIR_DOWN)
	    {
	      gtk_container_set_focus_child (container, NULL);
	    }
	  else
	    {
	      if (gtk_tlview_header_focus (tlview, direction))
		return TRUE;
	    }
	  GTK_TLVIEW_SET_FLAG (tlview, GTK_TLVIEW_DRAW_KEYFOCUS);
	  gtk_widget_grab_focus (GTK_WIDGET (container));

	  if (tlview->priv->selection == NULL)
	    gtk_tlselection_new_with_tlview (tlview);

	  /* if there is no keyboard focus yet, we select the first node
	   */
	  if (tlview->priv->cursor == NULL)
	    tlview->priv->cursor = gtk_tlpath_new_root ();
	  if (tlview->priv->cursor)
	    gtk_tlselection_select_row (tlview->priv->selection,
					tlview->priv->cursor);
	  gtk_widget_queue_draw (GTK_WIDGET (tlview));
	  return TRUE;
	}
    }

  /* Case 2. We don't have focus at all. */
  if (!GTK_WIDGET_HAS_FOCUS (container))
    {
      if ((direction == GTK_DIR_TAB_FORWARD) ||
	  (direction == GTK_DIR_RIGHT) ||
	  (direction == GTK_DIR_DOWN))
	{
	  if (gtk_tlview_header_focus (tlview, direction))
	    return TRUE;
	}

      /* The headers didn't want the focus, so we take it. */
      GTK_TLVIEW_SET_FLAG (tlview, GTK_TLVIEW_DRAW_KEYFOCUS);
      gtk_widget_grab_focus (GTK_WIDGET (container));

      if (tlview->priv->selection == NULL)
	gtk_tlselection_new_with_tlview (tlview);

      if (tlview->priv->cursor == NULL)
	tlview->priv->cursor = gtk_tlpath_new_root ();

      if (tlview->priv->cursor)
	gtk_tlselection_select_row (tlview->priv->selection,
				    tlview->priv->cursor);
      gtk_widget_queue_draw (GTK_WIDGET (tlview));
      return TRUE;
    }

  /* Case 3. We have focus already, but no cursor.  We pick the first one
   * and run with it. */
  if (tlview->priv->cursor == NULL)
    {
      /* We lost our cursor somehow.  Arbitrarily select the first node, and
       * return
       */
      tlview->priv->cursor = gtk_tlpath_new_root ();

      if (tlview->priv->cursor)
	gtk_tlselection_select_row (tlview->priv->selection,
				    tlview->priv->cursor);
      gtk_adjustment_set_value (GTK_ADJUSTMENT (tlview->priv->vadjustment),
				0.0);
      gtk_widget_queue_draw (GTK_WIDGET (tlview));
      return TRUE;
    }


  /* Case 3. We have focus already.  Move the cursor. */
  if (direction == GTK_DIR_LEFT)
    {
      gfloat val;
      val = tlview->priv->hadjustment->value - tlview->priv->hadjustment->page_size/2;
      val = MAX (val, 0.0);
      gtk_adjustment_set_value (GTK_ADJUSTMENT (tlview->priv->hadjustment), val);
      gtk_widget_grab_focus (GTK_WIDGET (tlview));
      return TRUE;
    }
  if (direction == GTK_DIR_RIGHT)
    {
      gfloat val;
      val = tlview->priv->hadjustment->value + tlview->priv->hadjustment->page_size/2;
      val = MIN (tlview->priv->hadjustment->upper - tlview->priv->hadjustment->page_size, val);
      gtk_adjustment_set_value (GTK_ADJUSTMENT (tlview->priv->hadjustment), val);
      gtk_widget_grab_focus (GTK_WIDGET (tlview));
      return TRUE;
    }
  cursor_tree = NULL;
  cursor_node = NULL;

  gtk_tlview_find_node (tlview, tlview->priv->cursor,
			&cursor_tree,
			&cursor_node);
  switch (direction)
    {
    case GTK_DIR_TAB_BACKWARD:
    case GTK_DIR_UP:
      g_rbtree_prev_full (cursor_tree,
			  cursor_node,
			  &cursor_tree,
			  &cursor_node);
      break;
    case GTK_DIR_TAB_FORWARD:
    case GTK_DIR_DOWN:
      g_rbtree_next_full (cursor_tree,
			  cursor_node,
			  &cursor_tree,
			  &cursor_node);
      break;
    default:
      break;
    }

  if (cursor_node)
    {
      GdkModifierType state = 0;

      event = gdk_event_peek ();
      if (event && event->type == GDK_KEY_PRESS)
	/* FIXME: This doesn't seem to work. )-:
	 * I fear the event may already have been gotten */
	state = ((GdkEventKey *)event)->state;

      if (event)
	gdk_event_free (event);
      gtk_tlpath_free (tlview->priv->cursor);

      tlview->priv->cursor = gtk_tlview_find_path (tlview,
						   cursor_tree,
						   cursor_node);
      if (tlview->priv->cursor)
	gtk_tlselection_internal_select_node (tlview->priv->selection,
					      cursor_node,
					      cursor_tree,
					      tlview->priv->cursor,
					      state);
      gtk_tlview_clamp_node_visible (tlview, cursor_tree, cursor_node);
      gtk_widget_grab_focus (GTK_WIDGET (tlview));
      gtk_widget_queue_draw (GTK_WIDGET (tlview));
      return TRUE;
    }

  /* At this point, we've progressed beyond the edge of the rows. */

  if ((direction == GTK_DIR_LEFT) ||
      (direction == GTK_DIR_TAB_BACKWARD) ||
      (direction == GTK_DIR_UP))
    /* We can't go back anymore.  Try the headers */
    return (gtk_tlview_header_focus (tlview, direction));

  /* we've reached the end of the tree.  Go on. */
  return FALSE;
}

/* Container method
 */
static void
gtk_tlview_remove (GtkContainer *container,
		   GtkWidget    *widget)
{
  GtkTLView *tlview;
  GtkTLViewChild *child = NULL;
  GList *tmp_list;

  g_return_if_fail (container != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (container));

  tlview = GTK_TLVIEW (container);

  tmp_list = tlview->priv->children;
  while (tmp_list)
    {
      child = tmp_list->data;
      if (child->widget == widget)
	break;
      tmp_list = tmp_list->next;
    }

  if (tmp_list)
    {
      gtk_widget_unparent (widget);

      tlview->priv->children = g_list_remove_link (tlview->priv->children, tmp_list);
      g_list_free_1 (tmp_list);
      g_free (child);
    }
}

static void
gtk_tlview_forall (GtkContainer *container,
		   gboolean      include_internals,
		   GtkCallback   callback,
		   gpointer      callback_data)
{
  GtkTLView *tlview;
  GtkTLViewChild *child = NULL;
  GtkTLColumn *column;
  GList *tmp_list;

  g_return_if_fail (container != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (container));
  g_return_if_fail (callback != NULL);

  tlview = GTK_TLVIEW (container);

  tmp_list = tlview->priv->children;
  while (tmp_list)
    {
      child = tmp_list->data;
      tmp_list = tmp_list->next;

      (* callback) (child->widget, callback_data);
    }
  if (include_internals == FALSE)
    return;

  for (tmp_list = tlview->priv->column; tmp_list; tmp_list = tmp_list->next)
    {
      column = tmp_list->data;
      if (column->button)
	(* callback) (column->button, callback_data);
    }
}

/* TLModel Methods
 */

static void
gtk_tlview_node_changed (GtkTLModel *model,
			 GtkTLPath  *path,
			 GtkTLNode  *tlnode,
			 gpointer    data)
{
  GtkTLView *tlview = (GtkTLView *)data;
  GRBTree *tree;
  GRBNode *node;
  gint height;
  gboolean dirty_marked;
  if (gtk_tlview_find_node (tlview,
			    path,
			    &tree,
			    &node))
    /* We aren't actually showing the node */
    return;

  dirty_marked = gtk_tlview_discover_dirty_node (tlview,
						 tlnode,
						 gtk_tlpath_get_depth (path),
						 &height);

  if (G_RBNODE_GET_HEIGHT (node) != height + TLVIEW_VERTICAL_SEPERATOR)
    {
      g_print ("different heights %d\t%d\n", G_RBNODE_GET_HEIGHT (node), height + TLVIEW_VERTICAL_SEPERATOR);
      g_rbtree_node_set_height (tree, node, height + TLVIEW_VERTICAL_SEPERATOR);
      gtk_widget_queue_resize (GTK_WIDGET (data));
      return;
    }
  if (dirty_marked)
    gtk_widget_queue_resize (GTK_WIDGET (data));
  else
    {
      /* FIXME: just redraw the node */
      gtk_widget_queue_resize (GTK_WIDGET (data));
    }
}

static void
gtk_tlview_node_inserted (GtkTLModel *model,
			  GtkTLPath  *path,
			  GtkTLNode  *tlnode,
			  gpointer    data)
{
  GtkTLView *tlview = (GtkTLView *) data;

  
}

static void
gtk_tlview_node_inserted_before (GtkTLModel *model,
				 GtkTLPath  *path,
				 GtkTLNode  *tlnode,
				 gpointer    data)
{

}

static void
gtk_tlview_node_child_added (GtkTLModel *model,
			     GtkTLPath  *path,
			     GtkTLNode  *tlnode,
			     gpointer    data)
{

}

static void
gtk_tlview_node_deleted (GtkTLModel *model,
			 GtkTLPath  *path,
			 gpointer    data)
{

}

/* Internal tree functions */
static void
gtk_tlview_build_tree (GtkTLView        *tlview,
		       GRBTree          *tree,
		       GtkTLNode         node,
		       gint              depth,
		       gboolean          recurse,
		       gboolean          calc_bounds)
{
  GRBNode *temp = NULL;
  GtkTLColumn *column;
  GList *list;
  GtkTLNode child;
  GtkTLCell *cell;
  gint max_height;
  gint i;

  if (!node)
    return;
  do
    {
      max_height = 0;
      if (calc_bounds)
	{
	  /* do stuff with node */
	  for (i = 0, list = tlview->priv->column; i < tlview->priv->columns; i++, list = list->next)
	    {
	      gint height = 0, width = 0;
	      column = list->data;

	      if (column->column_type == GTK_TLCOLUMN_FIXED)
		continue;
	      if (!column->visible)
		continue;
	      cell = column->cell;
	      gtk_tlcolumn_set_cell_data (column, tlview->priv->model, node);

	      gtk_tlcell_get_size (cell, GTK_WIDGET (tlview), &width, &height);
	      max_height = MAX (max_height, TLVIEW_VERTICAL_SEPERATOR + height);

	      if (i == 0 && TLVIEW_DRAW_EXPANDERS (tlview))
		column->size = MAX (column->size, depth * tlview->priv->tab_offset + width);
	      else
		column->size = MAX (column->size, width);
	    }
	}
      temp = g_rbtree_insert_after (tree, temp, max_height);
      if (recurse)
	{
	  child = gtk_tlmodel_node_children (tlview->priv->model, node);
	  if (child != NULL)
	    {
	      temp->children = g_rbtree_new ();
	      temp->children->parent_tree = tree;
	      temp->children->parent_node = temp;
	      gtk_tlview_build_tree (tlview, temp->children, child, depth + 1, recurse, calc_bounds);
	    }
	}
      if (gtk_tlmodel_node_has_child (tlview->priv->model, node))
	{
	  if ((temp->flags&G_RBNODE_IS_PARENT) != G_RBNODE_IS_PARENT)
	    temp->flags ^= G_RBNODE_IS_PARENT;
	  GTK_TLVIEW_UNSET_FLAG (tlview, GTK_TLVIEW_IS_LIST);
	}
    }
  while (gtk_tlmodel_node_next (tlview->priv->model, &node));
}

static void
gtk_tlview_calc_size (GtkTLView *tlview,
		      GRBTree   *tree,
		      GtkTLNode  node,
		      gint       depth)
{
  GRBNode *temp = tree->root;
  GtkTLNode child;
  GtkTLCell *cell;
  GList *list;
  GtkTLColumn *column;
  gint max_height;
  gint i;

  /* FIXME: Make this function robust against internal inconsistencies! */
  if (!node)
    return;
  TLVIEW_INTERNAL_ASSERT_VOID (tree != NULL);

  while (temp->left != tree->nil)
    temp = temp->left;

  do
    {
      max_height = 0;
      /* Do stuff with node */
      for (list = tlview->priv->column, i = 0; i < tlview->priv->columns; list = list->next, i++)
	{
	  gint height = 0, width = 0;
	  column = list->data;
	  if (column->dirty == FALSE || column->column_type == GTK_TLCOLUMN_FIXED)
	    continue;
	  if (!column->visible)
	    continue;

	  gtk_tlcolumn_set_cell_data (column, tlview->priv->model, node);
	  cell = column->cell;
	  gtk_tlcell_get_size (cell, GTK_WIDGET (tlview), &width, &height);
	  max_height = MAX (max_height, TLVIEW_VERTICAL_SEPERATOR + height);

	  if (i == 0 && TLVIEW_DRAW_EXPANDERS (tlview))
	    column->size = MAX (column->size, depth * tlview->priv->tab_offset + width);
	  else
	    column->size = MAX (column->size, width);
	}
      g_rbtree_node_set_height (tree, temp, max_height);
      child = gtk_tlmodel_node_children (tlview->priv->model, node);
      if (child != NULL && temp->children != NULL)
	gtk_tlview_calc_size (tlview, temp->children, child, depth + 1);
      temp = g_rbtree_next (tree, temp);
    }
  while (gtk_tlmodel_node_next (tlview->priv->model, &node));
}

static gboolean
gtk_tlview_discover_dirty_node (GtkTLView *tlview,
				GtkTLNode  node,
				gint       depth,
				gint      *height)
{
  GtkTLCell *cell;
  GtkTLColumn *column;
  GList *list;
  gint i;
  gint retval = FALSE;
  gint tmpheight;

  /* Do stuff with node */
  if (height)
    *height = 0;

  for (i = 0, list = tlview->priv->column; list; list = list->next, i++)
    {
      gint width;
      column = list->data;
      if (column->dirty == TRUE || column->column_type == GTK_TLCOLUMN_FIXED)
	continue;
      if (!column->visible)
	continue;

      cell = column->cell;
      gtk_tlcolumn_set_cell_data (column, tlview->priv->model, node);

      if (height)
	{
	  gtk_tlcell_get_size (cell, GTK_WIDGET (tlview), &width, &tmpheight);
	  *height = MAX (*height, tmpheight);
	}
      else
	{
	  gtk_tlcell_get_size (cell, GTK_WIDGET (tlview), &width, NULL);
	}
      if (i == 0 && TLVIEW_DRAW_EXPANDERS (tlview))
	{
	  if (depth * tlview->priv->tab_offset + width >= column->size)
	    {
	      column->dirty = TRUE;
	      retval = TRUE;
	    }
	}
      else
	{
	  if (width >= column->size)
	    {
	      column->dirty = TRUE;
	      retval = TRUE;
	    }
	}
    }

  return retval;
}

static void
gtk_tlview_discover_dirty (GtkTLView *tlview,
			   GRBTree   *tree,
			   GtkTLNode  node,
			   gint       depth)
{
  GRBNode *temp = tree->root;
  GtkTLColumn *column;
  GList *list;
  GtkTLNode child;
  gboolean is_all_dirty;

  /* FIXME: Make this function robust against internal inconsistencies! */
  if (!node)
    return;
  TLVIEW_INTERNAL_ASSERT_VOID (tree != NULL);

  while (temp->left != tree->nil)
    temp = temp->left;

  do
    {
      is_all_dirty = TRUE;
      for (list = tlview->priv->column; list; list = list->next)
	{
	  column = list->data;
	  if (column->dirty == FALSE)
	    {
	      is_all_dirty = FALSE;
	      break;
	    }
	}
      if (is_all_dirty)
	  return;

      gtk_tlview_discover_dirty_node (tlview,
				      node,
				      depth,
				      FALSE);
      child = gtk_tlmodel_node_children (tlview->priv->model, node);
      if (child != NULL && temp->children != NULL)
	gtk_tlview_discover_dirty (tlview, temp->children, child, depth + 1);
      temp = g_rbtree_next (tree, temp);
    }
  while (gtk_tlmodel_node_next (tlview->priv->model, &node));
}


static void
gtk_tlview_check_dirty (GtkTLView *tlview)
{
  GtkTLPath *path;
  GtkTLNode *tlnode;
  gboolean dirty = FALSE;
  GList *list;
  GtkTLColumn *column;

  for (list = tlview->priv->column; list; list = list->next)
    {
      column = list->data;
      if (column->dirty)
	{
	  dirty = TRUE;
	  if (column->column_type == GTK_TLCOLUMN_AUTOSIZE)
	    {
	      column->size = column->button->requisition.width;
	    }
	}
    }
  if (dirty == FALSE)
    return;

  path = gtk_tlpath_new_root ();
  if (path != NULL)
    {
      tlnode = gtk_tlmodel_get_node (tlview->priv->model, path);
      gtk_tlpath_free (path);
      gtk_tlview_calc_size (tlview, tlview->priv->tree, tlnode, 1);
      gtk_tlview_set_size (tlview, -1, -1);
    }

  for (list = tlview->priv->column; list; list = list->next)
    {
      column = list->data;
      column->dirty = FALSE;
    }
}

static void
gtk_tlview_create_button (GtkTLView *tlview,
			  gint       i)
{
  GtkWidget *button;
  GtkTLColumn *column;

  column = g_list_nth (tlview->priv->column, i)->data;
  gtk_widget_push_composite_child ();
  button = column->button = gtk_button_new ();
  gtk_widget_pop_composite_child ();

  gtk_widget_set_parent (button, GTK_WIDGET (tlview));

  gtk_signal_connect (GTK_OBJECT (button), "clicked",
  		      (GtkSignalFunc) gtk_tlview_button_clicked,
		      (gpointer) tlview);
}

static void
gtk_tlview_create_buttons (GtkTLView *tlview)
{
  GtkWidget *alignment;
  GtkWidget *label;
  GtkRequisition requisition;
  GList *list;
  GtkTLColumn *column;
  gint i;

  for (list = tlview->priv->column, i = 0; list; list = list->next, i++)
    {
      column = list->data;

      gtk_tlview_create_button (tlview, i);
      switch (column->justification)
	{
	case GTK_JUSTIFY_LEFT:
	  alignment = gtk_alignment_new (0.0, 0.5, 0.0, 0.0);
	  break;
	case GTK_JUSTIFY_RIGHT:
	  alignment = gtk_alignment_new (1.0, 0.5, 0.0, 0.0);
	  break;
	case GTK_JUSTIFY_CENTER:
	  alignment = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
	  break;
	case GTK_JUSTIFY_FILL:
	default:
	  alignment = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
	  break;
	}
      label = gtk_label_new (column->title);

      gtk_container_add (GTK_CONTAINER (alignment), label);
      gtk_container_add (GTK_CONTAINER (column->button), alignment);

      gtk_widget_show (label);
      gtk_widget_show (alignment);
      gtk_widget_size_request (column->button, &requisition);

      column->size = MAX (column->size, requisition.width);
      tlview->priv->header_height = MAX (tlview->priv->header_height, requisition.height);
    }
  if (GTK_WIDGET_REALIZED (tlview))
    {
      gtk_tlview_realize_buttons (tlview);
      if (GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_HEADERS_VISIBLE))
	{
	  /* We need to do this twice, as we need to map
	   * all the buttons before we map the columns */
	  for (list = tlview->priv->column; list; list = list->next)
	    {
	      column = list->data;
	      if (column->visible == FALSE)
		continue;
	      gtk_widget_map (column->button);
	    }
	  for (list = tlview->priv->column; list; list = list->next)
	    {
	      column = list->data;
	      if (column->visible == FALSE)
		continue;
	      if (column->column_type == GTK_TLCOLUMN_RESIZEABLE)
		{
		  gdk_window_raise (column->window);
		  gdk_window_show (column->window);
		}
	      else
		gdk_window_hide (column->window);
	    }
	}
    }
}

static void
gtk_tlview_button_clicked (GtkWidget *widget,
			   gpointer   data)
{
  GList *list;
  GtkTLView *tlview;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (data));

  tlview = GTK_TLVIEW (data);

  /* find the column who's button was pressed */
  for (list = tlview->priv->column; list; list = list->next)
    if (GTK_TLCOLUMN (list->data)->button == widget)
      break;

//  gtk_signal_emit (GTK_OBJECT (clist), clist_signals[CLICK_COLUMN], i);
}

/* Make sure the node is visible vertically */
static void
gtk_tlview_clamp_node_visible (GtkTLView *tlview,
			       GRBTree   *tree,
			       GRBNode   *node)
{
  gint offset;

  offset = g_rbtree_node_find_offset (tree, node);

  /* we reverse the order, b/c in the unusual case of the
   * node's height being taller then the visible area, we'd rather
   * have the node flush to the top
   */
  if (offset + G_RBNODE_GET_HEIGHT (node) >
      tlview->priv->vadjustment->value + tlview->priv->vadjustment->page_size)
    gtk_adjustment_set_value (GTK_ADJUSTMENT (tlview->priv->vadjustment),
			      offset + G_RBNODE_GET_HEIGHT (node) -
			      tlview->priv->vadjustment->page_size);
  if (offset < tlview->priv->vadjustment->value)
    gtk_adjustment_set_value (GTK_ADJUSTMENT (tlview->priv->vadjustment),
			      offset);
}

/* This function could be more efficient.
 * I'll optimize it if profiling seems to imply that
 * it's important */
GtkTLPath *
gtk_tlview_find_path (GtkTLView *tlview,
		      GRBTree   *tree,
		      GRBNode   *node)
{
  GtkTLPath *path;
  GRBTree *tmp_tree;
  GRBNode *tmp_node, *last;
  gint count;

  path = gtk_tlpath_new ();

  g_return_val_if_fail (node != NULL, path);
  g_return_val_if_fail (node != tree->nil, path);

  count = 1 + node->left->count;

  last = node;
  tmp_node = node->parent;
  tmp_tree = tree;
  while (tmp_tree)
    {
      while (tmp_node != tmp_tree->nil)
	{
	  if (tmp_node->right == last)
	    count += 1 + tmp_node->left->count;
	  last = tmp_node;
	  tmp_node = tmp_node->parent;
	}
      gtk_tlpath_prepend_index (path, count);
      last = tmp_tree->parent_node;
      tmp_tree = tmp_tree->parent_tree;
      if (last)
	{
	  count = 1 + last->left->count;
	  tmp_node = last->parent;
	}
    }
  return path;
}

/* Returns wether or not it's a parent, or not */
gboolean
gtk_tlview_find_node (GtkTLView  *tlview,
		      GtkTLPath  *path,
		      GRBTree   **tree,
		      GRBNode   **node)
{
  GRBNode *tmpnode = NULL;
  GRBTree *tmptree = tlview->priv->tree;
  const gint *indices = gtk_tlpath_get_indices (path);
  gint i = 0;

  *node = NULL;
  *tree = NULL;

  do
    {
      if (tmptree == NULL)
	{
	  *node = tmpnode;
	  *tree = tmptree;
	  return TRUE;
	}
      tmpnode = g_rbtree_find_count (tmptree, indices[i]);
      if (++i >= gtk_tlpath_get_depth (path))
	{
	  *node = tmpnode;
	  *tree = tmptree;
	  return FALSE;
	}
      tmptree = tmpnode->children;
    }
  while (1);
}

/* x and y are the mouse position
 */
static void
gtk_tlview_draw_arrow (GtkTLView *tlview,
		       GRBNode   *node,
		       gint       offset,
		       gint       x,
		       gint       y)
{
  GdkRectangle area;
  GtkStateType state;
  GtkShadowType shadow;
  GtkArrowType arrow_dir = GTK_ARROW_RIGHT;
  GdkPoint points[3];

  area.x = 0;
  area.y = offset + TLVIEW_VERTICAL_SEPERATOR;
  area.width = tlview->priv->tab_offset - 2;
  area.height = G_RBNODE_GET_HEIGHT (node) - TLVIEW_VERTICAL_SEPERATOR;

  if (node == tlview->priv->button_pressed_node)
    {
      if (x >= area.x && x <= (area.x + area.width) &&
	  y >= area.y && y <= (area.y + area.height))
	{
	  state = GTK_STATE_ACTIVE;
	  shadow = GTK_SHADOW_IN;
	}
      else
	{
	  state = GTK_STATE_NORMAL;
	  shadow = GTK_SHADOW_OUT;
	}
    }
  else
    {
      state = (node==tlview->priv->prelight_node&&GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_ARROW_PRELIT)?GTK_STATE_PRELIGHT:GTK_STATE_NORMAL);
      shadow = GTK_SHADOW_OUT;
    }

  if (TRUE ||
      (((node->flags & G_RBNODE_IS_PARENT) == G_RBNODE_IS_PARENT) &&
       node->children))
    {
	  points[0].x = area.x + 2;
	  points[0].y = area.y + (area.height - TLVIEW_EXPANDER_HEIGHT)/2;
	  points[1].x = points[0].x + TLVIEW_EXPANDER_WIDTH/2;
	  points[1].y = points[0].y + TLVIEW_EXPANDER_HEIGHT/2;
	  points[2].x = points[0].x;
	  points[2].y = points[0].y + TLVIEW_EXPANDER_HEIGHT;

    }

  gdk_draw_polygon (tlview->priv->bin_window,
		    GTK_WIDGET (tlview)->style->base_gc[state],
		    TRUE, points, 3);
  gdk_draw_polygon (tlview->priv->bin_window,
		    GTK_WIDGET (tlview)->style->fg_gc[state],
		    FALSE, points, 3);


/*    gtk_paint_arrow (GTK_WIDGET (tlview)->style, */
/*  		   tlview->priv->bin_window, */
/*  		   state, */
/*  		   shadow, */
/*  		   &area, */
/*  		   GTK_WIDGET (tlview), */
/*  		   "GtkTLView", */
/*  		   arrow_dir, */
/*  		   TRUE, */
/*  		   area.x, area.y, */
/*  		   area.width, area.height); */
}

void
gtk_tlview_set_size (GtkTLView     *tlview,
		     gint           width,
		     gint           height)
{
  GList *list;
  GtkTLColumn *column;
  gint i;

  if (tlview->priv->model == NULL)
    {
      tlview->priv->width = 1;
      tlview->priv->height = 1;
      return;
    }
  if (width == -1)
    {
      width = 0;
      for (list = tlview->priv->column, i = 0; list; list = list->next, i++)
	{
	  column = list->data;
	  if (!column->visible)
	    continue;
	  width += TLVIEW_COLUMN_SIZE (column);
	}
    }
  if (height == -1)
    height = tlview->priv->tree->root->offset + TLVIEW_VERTICAL_SEPERATOR;

  tlview->priv->width = width;
  tlview->priv->height = height;

  if (tlview->priv->hadjustment->upper != tlview->priv->width)
    {
      tlview->priv->hadjustment->upper = tlview->priv->width;
      gtk_signal_emit_by_name (GTK_OBJECT (tlview->priv->hadjustment), "changed");
    }

  if (tlview->priv->vadjustment->upper != tlview->priv->height)
    {
      tlview->priv->vadjustment->upper = tlview->priv->height;
      gtk_signal_emit_by_name (GTK_OBJECT (tlview->priv->vadjustment), "changed");
    }

  if (GTK_WIDGET_REALIZED (tlview))
    {
      gdk_window_resize (tlview->priv->bin_window, MAX (width, GTK_WIDGET (tlview)->allocation.width), height + TLVIEW_HEADER_HEIGHT (tlview));
      gdk_window_resize (tlview->priv->header_window, MAX (width, GTK_WIDGET (tlview)->allocation.width), tlview->priv->header_height);
    }
  gtk_widget_queue_resize (GTK_WIDGET (tlview));
}

/* this function returns the new width of the column being resized given
 * the column and x position of the cursor; the x cursor position is passed
 * in as a pointer and automagicly corrected if it's beyond min/max limits */
static gint
gtk_tlview_new_column_width (GtkTLView *tlview,
			     gint       i,
			     gint      *x)
{
  GtkTLColumn *column;
  gint width;

  /* first translate the x position from widget->window
   * to clist->clist_window */

  column = g_list_nth (tlview->priv->column, i)->data;
  width = *x - column->button->allocation.x;

  /* Clamp down the value */
  if (column->min_width == -1)
    width = MAX (column->button->requisition.width,
		 width);
  else
    width = MAX (column->min_width,
		 width);
  if (column->max_width != -1)
    width = MIN (width, column->max_width != -1);
  *x = column->button->allocation.x + width;

  return width;
}

/* Callbacks */
static void
gtk_tlview_adjustment_changed (GtkAdjustment *adjustment,
			       GtkTLView     *tlview)
{
  if (GTK_WIDGET_REALIZED (tlview))
    {
      gdk_window_move (tlview->priv->bin_window,
		       - tlview->priv->hadjustment->value,
		       - tlview->priv->vadjustment->value);
      gdk_window_move (tlview->priv->header_window,
		       - tlview->priv->hadjustment->value,
		       0);

      gdk_window_process_updates (tlview->priv->bin_window, TRUE);
      gdk_window_process_updates (tlview->priv->header_window, TRUE);
    }
}



/* Public methods
 */
GtkWidget *
gtk_tlview_new (void)
{
  GtkTLView *tlview;

  tlview = GTK_TLVIEW (gtk_type_new (gtk_tlview_get_type ()));

  return GTK_WIDGET (tlview);
}

GtkWidget *
gtk_tlview_new_with_model (GtkTLModel *model)
{
  GtkTLView *tlview;

  tlview = GTK_TLVIEW (gtk_type_new (gtk_tlview_get_type ()));
  gtk_tlview_set_model (tlview, model);

  return GTK_WIDGET (tlview);
}

GtkTLModel *
gtk_tlview_get_model (GtkTLView *tlview)
{
  g_return_val_if_fail (tlview != NULL, NULL);
  g_return_val_if_fail (GTK_IS_TLVIEW (tlview), NULL);

  return tlview->priv->model;
}

static void
gtk_tlview_set_model_realized (GtkTLView *tlview)
{
  GtkTLPath *path;
  GtkTLNode  node;

  tlview->priv->tree = g_rbtree_new ();

  gtk_signal_connect (GTK_OBJECT (tlview->priv->model),
		      "node_changed",
		      gtk_tlview_node_changed,
		      tlview);
  gtk_signal_connect (GTK_OBJECT (tlview->priv->model),
		      "node_inserted",
		      gtk_tlview_node_inserted,
		      tlview);
  gtk_signal_connect (GTK_OBJECT (tlview->priv->model),
		      "node_inserted_before",
		      gtk_tlview_node_inserted_before,
		      tlview);
  gtk_signal_connect (GTK_OBJECT (tlview->priv->model),
		      "node_child_added",
		      gtk_tlview_node_child_added,
		      tlview);
  gtk_signal_connect (GTK_OBJECT (tlview->priv->model),
		      "node_deleted",
		      gtk_tlview_node_deleted,
		      tlview);

  if (tlview->priv->column == NULL)
    return;

  path = gtk_tlpath_new_root ();
  if (path == NULL)
      return;

  node = gtk_tlmodel_get_node (tlview->priv->model, path);
  gtk_tlpath_free (path);
  gtk_tlview_build_tree (tlview, tlview->priv->tree, node, 1, FALSE, GTK_WIDGET_REALIZED (tlview));

  gtk_tlview_create_buttons (tlview);
  GTK_TLVIEW_SET_FLAG (tlview, GTK_TLVIEW_MODEL_SETUP);
}

void
gtk_tlview_set_model (GtkTLView *tlview, GtkTLModel *model)
{
  GList *list;
  GtkTLColumn *column;

  g_return_if_fail (tlview != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (tlview));

  if (tlview->priv->model != NULL)
    {
      for (list = tlview->priv->column; list; list = list->next)
	{
	  column = list->data;
	  if (column->button)
	    {
	      gtk_widget_unparent (column->button);
	      gdk_window_set_user_data (column->window, NULL);
	      gdk_window_destroy (column->window);
	    }
	}
      if (GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_MODEL_SETUP))
	{
	  gtk_signal_disconnect_by_func (GTK_OBJECT (tlview->priv->model),
					 gtk_tlview_node_changed,
					 tlview);
	  gtk_signal_disconnect_by_func (GTK_OBJECT (tlview->priv->model),
					 gtk_tlview_node_inserted,
					 tlview);
	  gtk_signal_disconnect_by_func (GTK_OBJECT (tlview->priv->model),
					 gtk_tlview_node_inserted_before,
					 tlview);
	  gtk_signal_disconnect_by_func (GTK_OBJECT (tlview->priv->model),
					 gtk_tlview_node_child_added,
					 tlview);
	  gtk_signal_disconnect_by_func (GTK_OBJECT (tlview->priv->model),
					 gtk_tlview_node_deleted,
					 tlview);
	  g_rbtree_free (tlview->priv->tree);
	}

      g_list_free (tlview->priv->column);
      tlview->priv->column = NULL;
      GTK_TLVIEW_UNSET_FLAG (tlview, GTK_TLVIEW_MODEL_SETUP);
    }

  tlview->priv->model = model;
  if (model == NULL)
    {
      tlview->priv->tree = NULL;
      tlview->priv->columns = 0;
      tlview->priv->column = NULL;
      if (GTK_WIDGET_REALIZED (tlview))
	gtk_tlview_set_size (tlview, 0, 0);
      return;
    }

  if (GTK_WIDGET_REALIZED (tlview))
    {
      gtk_tlview_set_model_realized (tlview);
      gtk_tlview_set_size (tlview, -1, -1);
    }
}

GtkTLSelection *
gtk_tlview_get_selection (GtkTLView *tlview)
{
  g_return_val_if_fail (tlview != NULL, NULL);
  g_return_val_if_fail (GTK_IS_TLVIEW (tlview), NULL);

  if (tlview->priv->selection == NULL)
    gtk_tlselection_new_with_tlview (tlview);

  return tlview->priv->selection;
}

void
gtk_tlview_set_selection (GtkTLView      *tlview,
			  GtkTLSelection *selection)
{
  g_return_if_fail (tlview != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (tlview));
  g_return_if_fail (selection != NULL);
  g_return_if_fail (GTK_IS_TLSELECTION (selection));

  g_object_ref (G_OBJECT (selection));

  if (tlview->priv->selection != NULL)
    g_object_unref (G_OBJECT (tlview->priv->selection));

  tlview->priv->selection = selection;
}

GtkAdjustment *
gtk_tlview_get_hadjustment (GtkTLView *tlview)
{
  g_return_val_if_fail (tlview != NULL, NULL);
  g_return_val_if_fail (GTK_IS_TLVIEW (tlview), NULL);

  return tlview->priv->hadjustment;
}

void
gtk_tlview_set_hadjustment (GtkTLView     *tlview,
			    GtkAdjustment *adjustment)
{
  g_return_if_fail (tlview != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (tlview));

  gtk_tlview_set_adjustments (tlview,
			      adjustment,
			      tlview->priv->vadjustment);
}

GtkAdjustment *
gtk_tlview_get_vadjustment (GtkTLView *tlview)
{
  g_return_val_if_fail (tlview != NULL, NULL);
  g_return_val_if_fail (GTK_IS_TLVIEW (tlview), NULL);

  return tlview->priv->vadjustment;
}

void
gtk_tlview_set_vadjustment (GtkTLView     *tlview,
			    GtkAdjustment *adjustment)
{
  g_return_if_fail (tlview != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (tlview));

  gtk_tlview_set_adjustments (tlview,
			      tlview->priv->hadjustment,
			      adjustment);
}

static void
gtk_tlview_set_adjustments (GtkTLView     *tlview,
			    GtkAdjustment *hadj,
			    GtkAdjustment *vadj)
{
  gboolean need_adjust = FALSE;

  g_return_if_fail (tlview != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (tlview));

  if (hadj)
    g_return_if_fail (GTK_IS_ADJUSTMENT (hadj));
  else
    hadj = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0));
  if (vadj)
    g_return_if_fail (GTK_IS_ADJUSTMENT (vadj));
  else
    vadj = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0));

  if (tlview->priv->hadjustment && (tlview->priv->hadjustment != hadj))
    {
      gtk_signal_disconnect_by_data (GTK_OBJECT (tlview->priv->hadjustment), tlview);
      gtk_object_unref (GTK_OBJECT (tlview->priv->hadjustment));
    }

  if (tlview->priv->vadjustment && (tlview->priv->vadjustment != vadj))
    {
      gtk_signal_disconnect_by_data (GTK_OBJECT (tlview->priv->vadjustment), tlview);
      gtk_object_unref (GTK_OBJECT (tlview->priv->vadjustment));
    }

  if (tlview->priv->hadjustment != hadj)
    {
      tlview->priv->hadjustment = hadj;
      gtk_object_ref (GTK_OBJECT (tlview->priv->hadjustment));
      gtk_object_sink (GTK_OBJECT (tlview->priv->hadjustment));

      gtk_signal_connect (GTK_OBJECT (tlview->priv->hadjustment), "value_changed",
			  (GtkSignalFunc) gtk_tlview_adjustment_changed,
			  tlview);
      need_adjust = TRUE;
    }

  if (tlview->priv->vadjustment != vadj)
    {
      tlview->priv->vadjustment = vadj;
      gtk_object_ref (GTK_OBJECT (tlview->priv->vadjustment));
      gtk_object_sink (GTK_OBJECT (tlview->priv->vadjustment));

      gtk_signal_connect (GTK_OBJECT (tlview->priv->vadjustment), "value_changed",
			  (GtkSignalFunc) gtk_tlview_adjustment_changed,
			  tlview);
      need_adjust = TRUE;
    }

  if (need_adjust)
    gtk_tlview_adjustment_changed (NULL, tlview);
}


/* Column and header operations */

gboolean
gtk_tlview_get_headers_visible (GtkTLView *tlview)
{
  g_return_val_if_fail (tlview != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TLVIEW (tlview), FALSE);

  return GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_HEADERS_VISIBLE);
}

void
gtk_tlview_set_headers_visible (GtkTLView *tlview,
				gboolean   headers_visible)
{
  gint x, y;
  GList *list;
  GtkTLColumn *column;

  g_return_if_fail (tlview != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (tlview));

  if (GTK_TLVIEW_FLAG_SET (tlview, GTK_TLVIEW_HEADERS_VISIBLE) == headers_visible)
    return;

  if (headers_visible)
    GTK_TLVIEW_SET_FLAG (tlview, GTK_TLVIEW_HEADERS_VISIBLE);
  else
    GTK_TLVIEW_UNSET_FLAG (tlview, GTK_TLVIEW_HEADERS_VISIBLE);

  if (GTK_WIDGET_REALIZED (tlview))
    {
      gdk_window_get_position (tlview->priv->bin_window, &x, &y);
      if (headers_visible)
	{
	  gdk_window_move_resize (tlview->priv->bin_window, x, y, tlview->priv->width, tlview->priv->height + TLVIEW_HEADER_HEIGHT (tlview));
	  for (list = tlview->priv->column; list; list = list->next)
	    {
	      column = list->data;
	      gtk_widget_map (column->button);
	    }

	  for (list = tlview->priv->column; list; list = list->next)
	    {
	      column = list->data;
	      if (column->visible == FALSE)
		continue;
	      if (column->column_type == GTK_TLCOLUMN_RESIZEABLE)
		{
		  gdk_window_raise (column->window);
		  gdk_window_show (column->window);
		}
	      else
		gdk_window_hide (column->window);
	    }
	  gdk_window_show (tlview->priv->header_window);
 	}
      else
	{
	  gdk_window_move_resize (tlview->priv->bin_window, x, y, tlview->priv->width, tlview->priv->height);
	  for (list = tlview->priv->column; list; list = list->next)
	    {
	      column = list->data;
	      gtk_widget_unmap (column->button);
	    }
	  gdk_window_hide (tlview->priv->header_window);
	}
    }

  tlview->priv->vadjustment->page_size = GTK_WIDGET (tlview)->allocation.height - TLVIEW_HEADER_HEIGHT (tlview);
  tlview->priv->vadjustment->page_increment = (GTK_WIDGET (tlview)->allocation.height - TLVIEW_HEADER_HEIGHT (tlview)) / 2;
  tlview->priv->vadjustment->lower = 0;
  tlview->priv->vadjustment->upper = tlview->priv->height;
  gtk_signal_emit_by_name (GTK_OBJECT (tlview->priv->vadjustment), "changed");

  gtk_widget_queue_resize (GTK_WIDGET (tlview));
}


void
gtk_tlview_columns_autosize (GtkTLView *tlview)
{
  gboolean dirty = FALSE;
  GList *list;
  GtkTLColumn *column;

  g_return_if_fail (tlview != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (tlview));

  for (list = tlview->priv->column; list; list = list->next)
    {
      column = list->data;
      if (column->column_type == GTK_TLCOLUMN_AUTOSIZE)
	continue;
      column->dirty = TRUE;
      dirty = TRUE;
    }

  if (dirty)
    gtk_widget_queue_resize (GTK_WIDGET (tlview));
}

void
gtk_tlview_set_headers_active (GtkTLView *tlview,
			       gboolean   active)
{
  GList *list;

  g_return_if_fail (tlview != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (tlview));
  g_return_if_fail (tlview->priv->model != NULL);

  for (list = tlview->priv->column; list; list = list->next)
    gtk_tlcolumn_set_header_active (GTK_TLCOLUMN (list->data), active);
}

gint
gtk_tlview_add_column (GtkTLView   *tlview,
		       GtkTLColumn *column)
{
  g_return_val_if_fail (tlview != NULL, -1);
  g_return_val_if_fail (GTK_IS_TLVIEW (tlview), -1);
  g_return_val_if_fail (column != NULL, -1);
  g_return_val_if_fail (GTK_IS_TLCOLUMN (column), -1);
  g_return_val_if_fail (column->tlview == NULL, -1);

  tlview->priv->column = g_list_append (tlview->priv->column,
					column);
  column->tlview = GTK_WIDGET (tlview);
  return tlview->priv->columns++;
}

GtkTLColumn *
gtk_tlview_get_column (GtkTLView *tlview,
		       gint       n)
{
  g_return_val_if_fail (tlview != NULL, NULL);
  g_return_val_if_fail (GTK_IS_TLVIEW (tlview), NULL);
  g_return_val_if_fail (tlview->priv->model != NULL, NULL);
  g_return_val_if_fail (n >= 0 || n < tlview->priv->columns, NULL);

  if (tlview->priv->column == NULL)
    return NULL;

  return GTK_TLCOLUMN (g_list_nth (tlview->priv->column, n)->data);
}

void
gtk_tlview_move_to (GtkTLView *tlview,
		    GtkTLPath *path,
		    gint       column,
		    gfloat     row_align,
		    gfloat     col_align)
{
  GRBNode *node = NULL;
  GRBTree *tree = NULL;

  g_return_if_fail (tlview != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (tlview));

  if (column < -1 || column > tlview->priv->columns)
    return;

  row_align = CLAMP (row_align, 0, 1);
  col_align = CLAMP (col_align, 0, 1);

  if (path != NULL)
    {
      gtk_tlview_find_node (tlview, path,
			    &tree, &node);
      if (node == NULL)
	return;
    }

  if (tlview->priv->hadjustment && column >= 0)
    {
      GtkTLColumn *col;

      col = g_list_nth (tlview->priv->column, column)->data;
      /* FIXME -- write  */
    }
}

static void
gtk_tlview_expand_all_helper (GRBTree  *tree,
			      GRBNode  *node,
			      gpointer  data)
{
  GtkTLView *tlview = data;

  if (node->children)
    g_rbtree_traverse (node->children,
		       node->children->root,
		       G_PRE_ORDER,
		       gtk_tlview_expand_all_helper,
		       data);
  else if ((node->flags & G_RBNODE_IS_PARENT) == G_RBNODE_IS_PARENT && node->children == NULL)
    {
      GtkTLPath *path;
      GtkTLNode tlnode;

      node->children = g_rbtree_new ();
      node->children->parent_tree = tree;
      node->children->parent_node = node;
      path = gtk_tlview_find_path (tlview, tree, node);
      tlnode = gtk_tlmodel_get_node (tlview->priv->model, path);
      tlnode = gtk_tlmodel_node_children (tlview->priv->model, tlnode);
      gtk_tlview_build_tree (tlview,
			     node->children,
			     tlnode,
			     gtk_tlpath_get_depth (path) + 1,
			     TRUE,
			     GTK_WIDGET_REALIZED (tlview));
      gtk_tlpath_free (path);
    }
}

void
gtk_tlview_expand_all (GtkTLView *tlview)
{
  g_return_if_fail (tlview != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (tlview));
  g_return_if_fail (tlview->priv->tree != NULL);

  g_rbtree_traverse (tlview->priv->tree,
		     tlview->priv->tree->root,
		     G_PRE_ORDER,
		     gtk_tlview_expand_all_helper,
		     tlview);

  gtk_tlview_set_size (tlview, -1,-1);
}

static void
gtk_tlview_collapse_all_helper (GRBTree  *tree,
				GRBNode  *node,
				gpointer  data)
{
  if (node->children)
    {
      GtkTLPath *path;
      GtkTLNode *tlnode;

      path = gtk_tlview_find_path (GTK_TLVIEW (data),
				   node->children,
				   node->children->root);
      tlnode = gtk_tlmodel_get_node (GTK_TLVIEW (data)->priv->model, path);
      gtk_tlview_discover_dirty (GTK_TLVIEW (data),
				 node->children,
				 tlnode,
				 gtk_tlpath_get_depth (path));
      g_rbtree_remove (node->children);
      gtk_tlpath_free (path);
    }
}

void
gtk_tlview_collapse_all (GtkTLView *tlview)
{
  g_return_if_fail (tlview != NULL);
  g_return_if_fail (GTK_IS_TLVIEW (tlview));
  g_return_if_fail (tlview->priv->tree != NULL);

  g_rbtree_traverse (tlview->priv->tree,
		     tlview->priv->tree->root,
		     G_PRE_ORDER,
		     gtk_tlview_collapse_all_helper,
		     tlview);

  if (GTK_WIDGET_REALIZED (tlview))
    gtk_widget_queue_draw (GTK_WIDGET (tlview));
}

