/* gtktlview.h
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_TLVIEW_H__
#define __GTK_TLVIEW_H__

#include <gtk/gtk.h>
#include "gtktlmodel.h"
#include "gtktlcolumn.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define GTK_TYPE_TLVIEW			(gtk_tlview_get_type ())
#define GTK_TLVIEW(obj)			(GTK_CHECK_CAST ((obj), GTK_TYPE_TLVIEW, GtkTLView))
#define GTK_TLVIEW_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_TLVIEW, GtkTLViewClass))
#define GTK_IS_TLVIEW(obj)		(GTK_CHECK_TYPE ((obj), GTK_TYPE_TLVIEW))
#define GTK_IS_TLVIEW_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_TLVIEW))

typedef struct _GtkTLView       GtkTLView;
typedef struct _GtkTLViewClass  GtkTLViewClass;
typedef struct _GtkTLViewPrivate GtkTLViewPrivate;

typedef struct _GtkTLSelection       GtkTLSelection;
typedef struct _GtkTLSelectionClass  GtkTLSelectionClass;


struct _GtkTLView
{
  GtkContainer parent;

  GtkTLViewPrivate *priv;
};

struct _GtkTLViewClass
{
  GtkContainerClass parent_class;

  void (*set_scroll_adjustments) (GtkTLView     *tlview,
				  GtkAdjustment *hadjustment,
				  GtkAdjustment *vadjustment);
  gint (*expand_row)     (GtkTLView *tlview,
			  GtkTLNode *node);
};

/* Creators */
GtkType          gtk_tlview_get_type            (void);
GtkWidget       *gtk_tlview_new                 (void);

GtkWidget       *gtk_tlview_new_with_model      (GtkTLModel     *model);
GtkTLModel      *gtk_tlview_get_model           (GtkTLView      *tlview);
void             gtk_tlview_set_model           (GtkTLView      *tlview,
						 GtkTLModel     *tlmodel);
GtkTLSelection  *gtk_tlview_get_selection       (GtkTLView      *tlview);
void             gtk_tlview_set_selection       (GtkTLView      *tlview,
						 GtkTLSelection *selection);
GtkAdjustment   *gtk_tlview_get_hadjustment     (GtkTLView      *layout);
void             gtk_tlview_set_hadjustment     (GtkTLView      *layout,
						 GtkAdjustment  *adjustment);
GtkAdjustment   *gtk_tlview_get_vadjustment     (GtkTLView      *layout);
void             gtk_tlview_set_vadjustment     (GtkTLView      *layout,
						 GtkAdjustment  *adjustment);
gboolean         gtk_tlview_get_headers_visible (GtkTLView      *tlview);
void             gtk_tlview_set_headers_visible (GtkTLView      *tlview,
						 gboolean        headers_visible);
void             gtk_tlview_columns_autosize    (GtkTLView      *tlview);
void             gtk_tlview_set_headers_active  (GtkTLView      *tlview,
						 gboolean        active);
gint             gtk_tlview_add_column          (GtkTLView      *tlview,
						 GtkTLColumn    *column);
GtkTLColumn     *gtk_tlview_get_column          (GtkTLView      *tlview,
						 gint            n);

/* Actions */
void             gtk_tlview_move_to             (GtkTLView      *tlview,
						 GtkTLPath      *path,
						 gint            column,
						 gfloat          row_align,
						 gfloat          col_align);
void             gtk_tlview_expand_all          (GtkTLView      *tlview);
void             gtk_tlview_collapse_all        (GtkTLView      *tlview);



#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_TLVIEW_H__ */

