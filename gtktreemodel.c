/* gtktreemodel.c
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include "gtktlmodel.h"
#include "gtktlcelltext.h"
#include "gtktreemodel.h"
#include <string.h>

#define G_NODE(node) ((GNode *)node)

struct _GtkTreeNode
{
  GNode node;
};

typedef struct _GtkTreeCellData GtkTreeCellData;
struct _GtkTreeCellData
{
  GtkTLCell *renderer;
  gpointer   data;
};

static void         gtk_tree_model_init              (GtkTreeModel      *TreeModel);
static void         gtk_tree_model_class_init        (GtkTreeModelClass *klass);
static gint         gtk_tree_model_get_columns       (GtkTLModel        *TLModel);
static GtkTLNode    gtk_tree_model_get_node          (GtkTLModel        *tlmodel,
						      GtkTLPath         *path);
static GtkTLPath   *gtk_tree_model_get_path          (GtkTLModel        *tlmodel,
						      GtkTLNode          node);
static gpointer     gtk_tree_model_node_get_data     (GtkTLModel        *TLModel,
						      GtkTLNode          node,
						      gint               column);
static gboolean     gtk_tree_model_node_next         (GtkTLModel        *tlmodel,
						      GtkTLNode         *node);
static GtkTLNode    gtk_tree_model_node_children     (GtkTLModel        *tlmodel,
						      GtkTLNode          node);
static gboolean     gtk_tree_model_node_has_child    (GtkTLModel        *tlmodel,
						      GtkTLNode          node);
static gint         gtk_tree_model_node_n_children   (GtkTLModel        *tlmodel,
						      GtkTLNode          node);
static GtkTLNode    gtk_tree_model_node_nth_child    (GtkTLModel        *tlmodel,
						      GtkTLNode          node,
						      gint               n);
static GtkTLNode    gtk_tree_model_node_parent       (GtkTLModel        *tlmodel,
						      GtkTLNode          node);


static GtkTLModelClass *parent_class = NULL;


GtkType
gtk_tree_model_get_type (void)
{
  static GtkType tree_model_type = 0;

  if (!tree_model_type)
    {
      static const GTypeInfo tree_model_info =
      {
        sizeof (GtkTreeModelClass),
	NULL,		/* base_init */
	NULL,		/* base_finalize */
        (GClassInitFunc) gtk_tree_model_class_init,
	NULL,		/* class_finalize */
	NULL,		/* class_data */
        sizeof (GtkTreeModel),
	0,              /* n_preallocs */
        (GInstanceInitFunc) gtk_tree_model_init
      };

      tree_model_type = g_type_register_static (GTK_TYPE_TLMODEL, "GtkTreeModel", &tree_model_info);
    }

  return tree_model_type;
}

static void
gtk_tree_model_class_init (GtkTreeModelClass *klass)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) klass;

  parent_class = gtk_type_class (gtk_tlmodel_get_type ());
  
  GTK_TLMODEL_CLASS (klass)->get_columns = gtk_tree_model_get_columns;
  GTK_TLMODEL_CLASS (klass)->get_node = gtk_tree_model_get_node;
  GTK_TLMODEL_CLASS (klass)->get_path = gtk_tree_model_get_path;
  GTK_TLMODEL_CLASS (klass)->node_get_data = gtk_tree_model_node_get_data;
  GTK_TLMODEL_CLASS (klass)->node_next = gtk_tree_model_node_next;
  GTK_TLMODEL_CLASS (klass)->node_children = gtk_tree_model_node_children;
  GTK_TLMODEL_CLASS (klass)->node_has_child = gtk_tree_model_node_has_child;
  GTK_TLMODEL_CLASS (klass)->node_n_children = gtk_tree_model_node_n_children;
  GTK_TLMODEL_CLASS (klass)->node_nth_child = gtk_tree_model_node_nth_child;
  GTK_TLMODEL_CLASS (klass)->node_parent = gtk_tree_model_node_parent;
}

static void
gtk_tree_model_init (GtkTreeModel *tree_model)
{
  tree_model->root = gtk_tree_model_node_new ();
}

GtkObject *
gtk_tree_model_new (void)
{
  return GTK_OBJECT (gtk_type_new (gtk_tree_model_get_type ()));
}

void
gtk_tree_model_set_columns (GtkTreeModel *tlmodel,
			    gint          columns)
{
  g_return_if_fail (tlmodel != NULL);
  g_return_if_fail (GTK_IS_TREE_MODEL (tlmodel));

  tlmodel->columns = columns;
}

/* Private accessors */
/* fulfill the GtkTLModel requirements */
static gint
gtk_tree_model_get_columns (GtkTLModel *tlmodel)
{
  g_return_val_if_fail (tlmodel != NULL, 0);
  g_return_val_if_fail (GTK_IS_TREE_MODEL (tlmodel), 0);

  return GTK_TREE_MODEL (tlmodel)->columns;
}

static GtkTLNode
gtk_tree_model_get_node (GtkTLModel *tlmodel,
			 GtkTLPath  *path)
{
  gint i;
  GtkTreeNode *node;
  const gint *indices = gtk_tlpath_get_indices (path);

  node = GTK_TREE_MODEL (tlmodel)->root;

  for (i = 0; i < gtk_tlpath_get_depth (path); i ++)
    {
      node = (GtkTreeNode *) gtk_tree_model_node_nth_child (tlmodel,
							  (GtkTLNode *) node,
							  indices[i]);
      if (node == NULL)
	return NULL;
    };
  return (GtkTLNode) node;
}

static GtkTLPath *
gtk_tree_model_get_path (GtkTLModel *tlmodel,
			 GtkTLNode   node)
{
  /* FIXME: this function needs writing. */
  return NULL;
}


static gpointer
gtk_tree_model_node_get_data (GtkTLModel *tlmodel,
			      GtkTLNode   node,
			      gint        column)
{
  GSList *list;

  g_return_val_if_fail (tlmodel != NULL, NULL);
  g_return_val_if_fail (GTK_IS_TREE_MODEL (tlmodel), NULL);
  g_return_val_if_fail (node != NULL, NULL);
  g_return_val_if_fail (column < GTK_TREE_MODEL (tlmodel)->columns, NULL);

  list = g_slist_nth ((GSList *) (G_NODE (node)->data), column);
  if (list == NULL || list->data == NULL)
    return NULL;

  return ((GtkTreeCellData *)list->data)->data;
}

static gboolean
gtk_tree_model_node_next (GtkTLModel  *tlmodel,
			  GtkTLNode   *node)
{
  if (node == NULL || *node == NULL)
    return FALSE;

  *node = (GtkTLNode *) G_NODE (*node)->next;
  return (*node != NULL);
}

static GtkTLNode
gtk_tree_model_node_children (GtkTLModel *tlmodel,
			      GtkTLNode   node)
{
  return (GtkTLNode) G_NODE (node)->children;
}

static gboolean
gtk_tree_model_node_has_child (GtkTLModel *tlmodel,
			       GtkTLNode   node)
{
  return G_NODE (node)->children != NULL;
}

static gint
gtk_tree_model_node_n_children (GtkTLModel *tlmodel,
				GtkTLNode   node)
{
  gint i = 0;

  node = (GtkTLNode *) G_NODE (node)->children;
  while (node != NULL)
    {
      i++;
      node = (GtkTLNode *) G_NODE (node)->next;
    }

  return i;
}

static GtkTLNode
gtk_tree_model_node_nth_child (GtkTLModel *tlmodel,
			       GtkTLNode   node,
			       gint        n)
{
  GtkTreeNode *temp = (GtkTreeNode *) node;
  g_return_val_if_fail (node != NULL, NULL);
  
  temp = (GtkTreeNode *) G_NODE (temp)->children;
  if (temp)
    while ((--n > 0) && temp)
      temp = (GtkTreeNode *) G_NODE (temp)->next;

  if (n != 0)
    return NULL;

  return (GtkTLNode) temp;
}

static GtkTLNode
gtk_tree_model_node_parent (GtkTLModel *tlmodel,
			    GtkTLNode   node)
{
  return (GtkTLNode) G_NODE (node)->parent;
}

/* Public accessors */
GtkTreeNode *
gtk_tree_model_node_new (void)
{
  GtkTreeNode *retval;

  retval = g_new (GtkTreeNode, 1);
  G_NODE (retval)->next = NULL;
  G_NODE (retval)->prev = NULL;
  G_NODE (retval)->parent = NULL;
  G_NODE (retval)->data = NULL;
  G_NODE (retval)->children = NULL;

  return retval;
}

/*
 * This is a somewhat inelegant function that does a lot of list
 * manipulations on it's own.
 */
void
gtk_tree_model_node_set_cell (GtkTreeModel *model,
			      GtkTreeNode  *node,
			      gint          column,
			      GtkTLCell    *renderer,
			      gpointer      data)
{
  GSList *list;
  GSList *prev;
  GtkTreeCellData *cell_data;

  g_return_if_fail (model != NULL);
  g_return_if_fail (GTK_IS_TREE_MODEL (model));
  g_return_if_fail (node != NULL);
  g_return_if_fail (column < model->columns);

  prev = list = G_NODE (node)->data;
  while (list != NULL)
    {
      if (column == 0)
	{
	  if (list->data == NULL)
	    {
	      list->data = cell_data = g_new (GtkTreeCellData, 1);
	      cell_data->renderer = renderer;
	      cell_data->data = data;
	    }
	  else
	    {
	      gtk_object_ref (GTK_OBJECT (renderer));
	      /* FIXME: Figure out memory management */
/*	      gtk_tlcell_free_data (((GtkTreeCellData *) list->data)->renderer,
	      ((GtkTreeCellData *) list->data)->data);*/
	      gtk_object_unref (GTK_OBJECT (((GtkTreeCellData *) list->data)->renderer));
	      ((GtkTreeCellData *) list->data)->renderer = renderer;
	      ((GtkTreeCellData *) list->data)->data = data;
	    }
	  return;
	}
      column--;
      prev = list;
      list = list->next;
    }

  if (G_NODE (node)->data == NULL)
    {
      G_NODE (node)->data = list = g_slist_alloc ();
      list->next = NULL;
      list->data = NULL;
    }
  else
    {
      list = prev->next = g_slist_alloc ();
      list->next = NULL;
      list->data = NULL;
    }

  while (column != 0)
    {
      list->next = g_slist_alloc ();
      list = list->next;
      list->next = NULL;
      list->data = NULL;
      column --;
    }
  list->data = cell_data = g_new (GtkTreeCellData, 1);
  cell_data->renderer = renderer;
  cell_data->data = data;
}

void
gtk_tree_model_node_destroy (GtkTreeModel *model,
			     GtkTreeNode  *node)
{
  
}

GtkTreeNode *
gtk_tree_model_node_insert (GtkTreeModel *model,
			    GtkTreeNode  *parent,
			    gint          position,
			    GtkTreeNode  *node)
{
  g_return_val_if_fail (model != NULL, node);
  g_return_val_if_fail (GTK_IS_TREE_MODEL (model), node);
  g_return_val_if_fail (node != NULL, node);

  if (parent == NULL)
    parent = model->root;

  g_node_insert (G_NODE (parent), position, G_NODE (node));

  return node;
}


GtkTreeNode *
gtk_tree_model_node_insert_before (GtkTreeModel *model,
				   GtkTreeNode  *parent,
				   GtkTreeNode  *sibling,
				   GtkTreeNode  *node)
{
  g_return_val_if_fail (model != NULL, node);
  g_return_val_if_fail (GTK_IS_TREE_MODEL (model), node);
  g_return_val_if_fail (node != NULL, node);

  if (parent == NULL)
    parent = model->root;

  g_node_insert_before (G_NODE (parent), G_NODE (sibling), G_NODE (node));

  return node;
}

GtkTreeNode *
gtk_tree_model_node_prepend (GtkTreeModel *model,
			     GtkTreeNode  *parent,
			     GtkTreeNode  *node)
{
  g_return_val_if_fail (model != NULL, node);
  g_return_val_if_fail (GTK_IS_TREE_MODEL (model), node);
  g_return_val_if_fail (node != NULL, node);

  if (parent == NULL)
    parent =model->root;

  g_node_prepend (G_NODE (parent), G_NODE (node));
  return node;
}

GtkTreeNode *
gtk_tree_model_node_append (GtkTreeModel *model,
			    GtkTreeNode  *parent,
			    GtkTreeNode  *node)
{
  g_return_val_if_fail (model != NULL, node);
  g_return_val_if_fail (GTK_IS_TREE_MODEL (model), node);
  g_return_val_if_fail (node != NULL, node);

  if (parent == NULL)
    parent = model->root;

  g_node_append (G_NODE (parent), G_NODE (node));
  return node;
}

GtkTreeNode *
gtk_tree_model_node_get_root (GtkTreeModel *model)
{
  g_return_val_if_fail (model != NULL, NULL);
  g_return_val_if_fail (GTK_IS_TREE_MODEL (model), NULL);

  return (GtkTreeNode *) model->root;
}


gboolean
gtk_tree_model_node_is_ancestor (GtkTreeModel *model,
				 GtkTreeNode  *node,
				 GtkTreeNode  *descendant)
{
  g_return_val_if_fail (model != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_TREE_MODEL (model), FALSE);
  g_return_val_if_fail (node != NULL, FALSE);
  g_return_val_if_fail (descendant != NULL, FALSE);

  return g_node_is_ancestor (G_NODE (node), G_NODE (descendant));
}


gint
gtk_tree_model_node_depth (GtkTreeModel *model,
			   GtkTreeNode  *node)
{
  g_return_val_if_fail (model != NULL, 0);
  g_return_val_if_fail (GTK_IS_TREE_MODEL (model), 0);
  g_return_val_if_fail (node != NULL, 0);

  return g_node_depth (G_NODE (node));
}


GtkTreeNode *
gtk_tree_model_node_find (GtkTreeModel   *model,
			  GtkTreeNode    *root,
			  GTraverseType   order,
			  GTraverseFlags  flags,
			  gpointer        data)
{
  return NULL;
}



