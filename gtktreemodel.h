/* gtktreemodel.h
 * Copyright (C) 2000  Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_TREEMODEL_H__
#define __GTK_TREEMODEL_H__

#include <gtk/gtk.h>
#include "gtktlmodel.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define GTK_TYPE_TREE_MODEL			(gtk_tree_model_get_type ())
#define GTK_TREE_MODEL(obj)			(GTK_CHECK_CAST ((obj), GTK_TYPE_TREE_MODEL, GtkTreeModel))
#define GTK_TREE_MODEL_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_TREEMODEL, GtkTreeModelClass))
#define GTK_IS_TREE_MODEL(obj)			(GTK_CHECK_TYPE ((obj), GTK_TYPE_TREE_MODEL))
#define GTK_IS_TREE_MODEL_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_TREE_MODEL))

typedef struct _GtkTreeNode        GtkTreeNode;
typedef struct _GtkTreeModel       GtkTreeModel;
typedef struct _GtkTreeModelClass  GtkTreeModelClass;

struct _GtkTreeModel
{
  GtkTLModel parent;
  GtkTreeNode *root;
  gint columns;
  gchar **column_headers;
};

struct _GtkTreeModelClass
{
  GtkTLModelClass parent_class;
};

GtkType      gtk_tree_model_get_type           (void);
GtkObject   *gtk_tree_model_new                (void);
void         gtk_tree_model_set_columns        (GtkTreeModel   *model,
						gint            columns);
GtkTreeNode *gtk_tree_model_node_new           (void);
void         gtk_tree_model_node_set_cell      (GtkTreeModel   *model,
						GtkTreeNode    *node,
						gint            column,
						GtkTLCell      *renderer,
						gpointer        data);
void         gtk_tree_model_node_destroy       (GtkTreeModel   *model,
						GtkTreeNode    *node);
GtkTreeNode *gtk_tree_model_node_insert        (GtkTreeModel   *model,
						GtkTreeNode    *parent,
						gint            position,
						GtkTreeNode    *node);
GtkTreeNode *gtk_tree_model_node_insert_before (GtkTreeModel   *model,
						GtkTreeNode    *parent,
						GtkTreeNode    *sibling,
						GtkTreeNode    *node);
GtkTreeNode *gtk_tree_model_node_prepend       (GtkTreeModel   *model,
						GtkTreeNode    *parent,
						GtkTreeNode    *node);
GtkTreeNode *gtk_tree_model_node_append        (GtkTreeModel   *model,
						GtkTreeNode    *parent,
						GtkTreeNode    *node);
GtkTreeNode *gtk_tree_model_node_get_root      (GtkTreeModel   *model);
gboolean     gtk_tree_model_node_is_ancestor   (GtkTreeModel   *model,
						GtkTreeNode    *node,
						GtkTreeNode    *descendant);
gint         gtk_tree_model_node_depth         (GtkTreeModel   *model,
						GtkTreeNode    *node);
GtkTreeNode *gtk_tree_model_node_find          (GtkTreeModel   *model,
						GtkTreeNode    *root,
						GTraverseType   order,
						GTraverseFlags  flags,
						gpointer        data);



#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_TREEMODEL_H__ */
