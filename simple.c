#include "gtktlview.h"
#include "gtktlcelltext.h"
#include "gtktlcellpixbuf.h"
#include "gtktlcelltextpixbuf.h"
#include "gtklistmodel.h"
#include "gtkmodelsimple.h"

#define LINES 8

enum {
  POEM_COLUMN = 0,
  REFRAIN_COLUMN,
  COLOR_COLUMN,
  POSITION_COLUMN
};

static GdkPixbuf *note;

typedef struct {
  char *poem;
  char *refrain;
  char *color;
  GtkPositionType pos;
} ModelType;

ModelType model [] = {
  { "The more it snows", "(TiddlyPom)", "SlateBlue", GTK_POS_LEFT },
  { "The more it goes", "(TiddlyPom)", "MediumSlateBlue", GTK_POS_RIGHT },
  { "The more it goes", "(TiddlyPom)", "DarkSlateBlue", GTK_POS_LEFT },
  { "on snowing.", "", "DarkSlateBlue", GTK_POS_RIGHT },
  { "And nobody knows", "(TiddlyPom)", "SlateBlue", GTK_POS_LEFT },
  { "How cold my toes", "(TiddlyPom)", "MediumSlateBlue", GTK_POS_RIGHT },
  { "How cold my toes", "(TiddlyPom)", "DarkSlateBlue", GTK_POS_LEFT },
  { "are growing.", "", "DarkSlateBlue", GTK_POS_RIGHT },
  { NULL, NULL, NULL}
};

/* Our TLModel::get_node callback.
 * We represent nodes as integers, so we can simply return the indices of the
 * path.
 */
GtkTLNode
simple_get_node (GtkTLModel *tlmodel,
		 GtkTLPath  *path,
		 gpointer    data)
{
  return model + (gtk_tlpath_get_indices (path)[0]) - 1;
}

/* Our TLModel::get_value callback.
 * As our nodes are all offsets, we can just use them as an array offset into
 * the table above. */
void
simple_node_get_value (GtkTLModel *TLModel,
		       GtkTLNode   node,
		       gint        column,
		       GValue     *value,
		       gpointer    data)
{
  /* This is a hack until g_value_init gets fixed */
  value->g_type = 0;

  if (column == POSITION_COLUMN)
    g_value_init (value, G_TYPE_INT);
  else
    g_value_init (value, G_TYPE_STRING);

  switch (column)
    {
    case POEM_COLUMN:
      g_value_set_string (value, ((ModelType *) node)->poem);
      break;
    case REFRAIN_COLUMN:
      g_value_set_string (value, ((ModelType *) node)->refrain);
      break;
    case COLOR_COLUMN:
      g_value_set_string (value, ((ModelType *) node)->color);
      break;
    case POSITION_COLUMN:
      g_value_set_int (value, (gint) ((ModelType *) node)->pos);
      break;
    }
}

/* Our TLModel::node_next callback.
 * As we are indexing our nodes by an int, we can just add one to the old one,
 * until LINES is reached.
 */
gboolean
simple_node_next (GtkTLModel *tlmodel,
		  GtkTLNode  *node,
		  gpointer    data)
{
  *((ModelType **)node) = ((ModelType *)(*node)) + 1;

  if (((ModelType *)(*node))->poem == NULL)
    return FALSE;
  return TRUE;
}

int
main (int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *scrolled_window;
  GtkWidget *tlview;
  GtkObject *model;
  GtkObject *column;
  GtkTLCell *cell;

  gtk_init (&argc, &argv);
  note = gdk_pixbuf_new_from_file ("note.png");

  /* Make the Widgets/Objects */
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_default_size (GTK_WINDOW (window), 300, 350);
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  model = gtk_model_simple_new ();
  tlview = gtk_tlview_new_with_model (GTK_TLMODEL (model));

  /* Put them together */
  gtk_container_add (GTK_CONTAINER (scrolled_window), tlview);
  gtk_container_add (GTK_CONTAINER (window), scrolled_window);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_AUTOMATIC);

  gtk_signal_connect (GTK_OBJECT (window), "destroy", gtk_main_quit, NULL);
  gtk_signal_connect (GTK_OBJECT (model), "node_next", GTK_SIGNAL_FUNC (simple_node_next), NULL);
  gtk_signal_connect (GTK_OBJECT (model), "get_node", GTK_SIGNAL_FUNC (simple_get_node), NULL);
  gtk_signal_connect (GTK_OBJECT (model), "node_get_value", GTK_SIGNAL_FUNC (simple_node_get_value), NULL);

  /* The Poem Column */
  cell = gtk_tlcell_text_new ();
  g_object_set (G_OBJECT (cell),
		"font", "sans bold 14",
		NULL);

  column = gtk_tlcolumn_new_with_vals ("Poem", cell, "text", POEM_COLUMN, NULL);
  gtk_tlview_add_column (GTK_TLVIEW (tlview), GTK_TLCOLUMN (column));

  /* The Refrain Column */
  cell = gtk_tlcell_text_pixbuf_new ();
  g_object_set (G_OBJECT (cell),
		"font", "sans italic 10",
		"pixbuf", note,
		NULL);

  column = gtk_tlcolumn_new_with_vals ("Refrain", cell,
				       "text", REFRAIN_COLUMN,
				       "foreground", COLOR_COLUMN,
				       "pixbufpos", POSITION_COLUMN,
				       NULL);
  gtk_tlview_add_column (GTK_TLVIEW (tlview), GTK_TLCOLUMN (column));
  
  gtk_widget_show_all (window);
  gtk_main ();

  return 0;
}
