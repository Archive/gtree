#include "gtktlmodel.h"
#include "gtktreemodel.h"
#include "gtktlview.h"
#include "grbtree.h"
#include "gtktlcelltext.h"
#include "gtktlselection.h"
#include <stdlib.h>
#include "gtktlcolumn.h"

static GtkTLCell *renderer;
static GtkObject *tree_model;
static GtkObject *tree_model2;
static GtkObject *tree_model3;

typedef struct _TreeTest
{
  GtkWidget *tlview;
  GtkObject *model;
  GtkObject *selection;
  GtkWidget *omenu;
  GtkWidget *button1;
  GtkWidget *button2;
  GtkWidget *button3;
  GtkWidget *size_entry;
  GtkWidget *min_entry;
  GtkWidget *max_entry;
  GtkWidget *title_active;
  GtkWidget *visible;
} TreeTest;


static gboolean
selection_die (GtkTLSelection  *selection,
	       GtkTLModel      *model,
	       GtkTLPath       *path,
	       gpointer         data)
{
  return FALSE;
}

static void
single_selection_cb (GtkWidget *button, TreeTest *tree_test)
{
  if (GTK_TOGGLE_BUTTON (button)->active)
      gtk_tlselection_set_type (GTK_TLSELECTION (tree_test->selection),
				GTK_TLSELECTION_SINGLE);
}

static void
multi_selection_cb (GtkWidget *button, TreeTest *tree_test)
{
  if (GTK_TOGGLE_BUTTON (button)->active)
    gtk_tlselection_set_type (GTK_TLSELECTION (tree_test->selection),
			      GTK_TLSELECTION_MULTI);
}

void
foreach_func (GtkTLModel *model,
	      GtkTLNode  *tlnode,
	      gpointer    data)
{
  gint i;
  gpointer cell_data;

  for (i = 0; i < gtk_tlmodel_get_columns (model); i++)
    {
      cell_data = gtk_tlmodel_node_get_data (model, tlnode, i);
      if (i == gtk_tlmodel_get_columns (model) - 1)
	;//	g_print ("%s\n", (gchar *) cell_data);
      else
	;//	g_print ("%s | ", (gchar *) cell_data);
    }
}

void
selection_cb (GtkTLSelection *selection,
	      GtkTLModel *model,
	      GtkTLNode  *tlnode,
	      gpointer    data)
{
  gint i;
  gpointer cell_data;

  //  g_print ("Row Selected:\t");
  for (i = 0; i < gtk_tlmodel_get_columns (model); i++)
    {
      cell_data = gtk_tlmodel_node_get_data (model, tlnode, i);
      if (i == gtk_tlmodel_get_columns (model) - 1)
	;//g_print ("%s\n", (gchar *) cell_data);
      else
	;//g_print ("%s | ", (gchar *) cell_data);
    }
}

void
unselection_cb (GtkTLSelection *selection,
		GtkTLModel *model,
		GtkTLNode  *tlnode,
		gpointer    data)
{
  gint i;
  gpointer cell_data;

  ;//  g_print ("Row Unselected:\t");
  for (i = 0; i < gtk_tlmodel_get_columns (model); i++)
    {
      cell_data = gtk_tlmodel_node_get_data (model, tlnode, i);
      if (i == gtk_tlmodel_get_columns (model) - 1)
	;//g_print ("%s\n", (gchar *) cell_data);
      else
	;//g_print ("%s | ", (gchar *) cell_data);
    }
}

static void
print_selection (GtkWidget *button, TreeTest *tree_test)
{
  gtk_tlselection_selected_foreach (GTK_TLSELECTION (tree_test->selection),
				    foreach_func,
				    NULL);
}

static void
select_all (GtkWidget *button, TreeTest *tree_test)
{
  gtk_tlselection_select_all (GTK_TLSELECTION (tree_test->selection));
}

static void
unselect_all (GtkWidget *button, TreeTest *tree_test)
{
  gtk_tlselection_unselect_all (GTK_TLSELECTION (tree_test->selection));
}

static void
column_select_callback (GtkWidget *menu_item, TreeTest *tree_test)
{
  gint column;
  gint type;
  gchar *size;

  column = GPOINTER_TO_INT (gtk_object_get_user_data (GTK_OBJECT (menu_item)));
  type = gtk_tlcolumn_get_col_type (gtk_tlview_get_column (GTK_TLVIEW (tree_test->tlview), column));

  switch (type)
    {
    case GTK_TLCOLUMN_RESIZEABLE:
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (tree_test->button1), TRUE);
      gtk_widget_set_sensitive (tree_test->size_entry, FALSE);
      break;
    case GTK_TLCOLUMN_AUTOSIZE:
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (tree_test->button2), TRUE);
      gtk_widget_set_sensitive (tree_test->size_entry, FALSE);
      break;
    case GTK_TLCOLUMN_FIXED:
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (tree_test->button3), TRUE);
      gtk_widget_set_sensitive (tree_test->size_entry, TRUE);
      break;
    }

  size = g_strdup_printf ("%d", gtk_tlcolumn_get_size (gtk_tlview_get_column (GTK_TLVIEW (tree_test->tlview),
									      column)));
  gtk_entry_set_text (GTK_ENTRY (tree_test->size_entry), size);
  g_free (size);
  size = g_strdup_printf ("%d", gtk_tlcolumn_get_min_width (gtk_tlview_get_column (GTK_TLVIEW (tree_test->tlview),
										   column)));
  gtk_entry_set_text (GTK_ENTRY (tree_test->min_entry), size);
  g_free (size);
  size = g_strdup_printf ("%d", gtk_tlcolumn_get_max_width (gtk_tlview_get_column (GTK_TLVIEW (tree_test->tlview),
										   column)));
  gtk_entry_set_text (GTK_ENTRY (tree_test->max_entry), size);
  g_free (size);

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (tree_test->visible),
				gtk_tlcolumn_get_visible (gtk_tlview_get_column (GTK_TLVIEW (tree_test->tlview),
										 column)));
}

static GtkWidget *
make_menu (TreeTest *tree_test)
{
  GtkWidget *menu;
  GtkWidget *menu_item;
  gchar *text;
  gint i;
  gint columns;

  columns = gtk_tlmodel_get_columns (GTK_TLMODEL (tree_test->model));
  menu = gtk_menu_new ();
  for (i = 0; i < columns; i++)
    {
      text = g_strdup_printf ("Column %d", i);
      menu_item = gtk_menu_item_new_with_label (text);
      g_free (text);
      gtk_menu_append (GTK_MENU (menu), menu_item);
      gtk_signal_connect (GTK_OBJECT (menu_item), "activate", column_select_callback, tree_test);
      gtk_object_set_user_data (GTK_OBJECT (menu_item), GINT_TO_POINTER (i));
    }
  gtk_widget_show_all (menu);
  return menu;
}


static void
tree_test_set_model (TreeTest *tree_test)
{
  GtkWidget *menu;
  gtk_option_menu_remove_menu (GTK_OPTION_MENU (tree_test->omenu));
  if (tree_test->model == NULL)
    {
      gtk_widget_set_sensitive (tree_test->omenu->parent, FALSE);
      return;
    }
  gtk_widget_set_sensitive (tree_test->omenu->parent, TRUE);
  menu = make_menu (tree_test);
  gtk_option_menu_set_menu (GTK_OPTION_MENU (tree_test->omenu), menu);

}

static void
apply_column_settings (GtkButton *button, TreeTest *tree_test)
{
  GtkWidget *menu_item;
  gint column;
  gint min_size, max_size;

  if (tree_test->model == NULL)
    return;

  menu_item = GTK_OPTION_MENU (tree_test->omenu)->menu_item;
  column = GPOINTER_TO_INT (gtk_object_get_user_data (GTK_OBJECT (menu_item)));

  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (tree_test->button1)))
    gtk_tlcolumn_set_col_type (gtk_tlview_get_column (GTK_TLVIEW (tree_test->tlview),
						  column),
			   GTK_TLCOLUMN_RESIZEABLE);
  else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (tree_test->button2)))
    gtk_tlcolumn_set_col_type (gtk_tlview_get_column (GTK_TLVIEW (tree_test->tlview),
						  column),
			   GTK_TLCOLUMN_AUTOSIZE);
  else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (tree_test->button3)))
    {
      gtk_tlcolumn_set_col_type (gtk_tlview_get_column (GTK_TLVIEW (tree_test->tlview),
						    column),
			     GTK_TLCOLUMN_FIXED);

    }
  min_size = atoi (gtk_entry_get_text (GTK_ENTRY (tree_test->min_entry)));
  max_size = atoi (gtk_entry_get_text (GTK_ENTRY (tree_test->max_entry)));
  gtk_tlcolumn_set_min_width (gtk_tlview_get_column (GTK_TLVIEW (tree_test->tlview), column), min_size);
  gtk_tlcolumn_set_max_width (gtk_tlview_get_column (GTK_TLVIEW (tree_test->tlview), column), max_size);
  gtk_tlcolumn_set_visible (gtk_tlview_get_column (GTK_TLVIEW (tree_test->tlview),
						   column),

			    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (tree_test->visible)));
}

static void
model_change_callback (GtkToggleButton *button, GtkTLModel *model)
{
  TreeTest *tree_test;

  tree_test = gtk_object_get_user_data (GTK_OBJECT (button));
  if (gtk_toggle_button_get_active (button))
    gtk_tlview_set_model (GTK_TLVIEW (tree_test->tlview), model);
  tree_test->model = GTK_OBJECT (model);
  tree_test_set_model (tree_test);
}

static void
cbox_toggled (GtkToggleButton *button, GtkTLView *view)
{
  gtk_tlview_set_headers_visible (view, gtk_toggle_button_get_active (button));
}

static void
expand_all_cb (GtkButton *button, GtkTLView *view)
{
  gtk_tlview_expand_all (view);
}

static void
shrink_all_cb (GtkButton *button, GtkTLView *view)
{
  gtk_tlview_collapse_all (view);
}


static void
make_test_window (GtkTLModel *model)
{
  static gboolean foo = FALSE;
  TreeTest *tree_test;
  GtkWidget *vbox, *hbox;
  GtkWidget *vbox2, *hbox2, *hbox3;
  GtkWidget *window;
  GtkWidget *scroll;
  GtkWidget *cbox;
  GtkWidget *button;

  GSList *list = NULL;
  tree_test = g_new (TreeTest, 1);
  tree_test->model = GTK_OBJECT (model);
  tree_test->tlview = gtk_tlview_new_with_model (GTK_TLMODEL (model));
  tree_test->selection = gtk_tlselection_new_with_tlview (GTK_TLVIEW (tree_test->tlview));

  /*  gtk_tlselection_set_select_function (GTK_TLSELECTION (tree_test->selection),
      selection_die,
      NULL);*/
  gtk_signal_connect (GTK_OBJECT (tree_test->selection), "row_selected", selection_cb, NULL);
  gtk_signal_connect (GTK_OBJECT (tree_test->selection), "row_unselected", unselection_cb, NULL);
  gtk_tlview_set_headers_visible (GTK_TLVIEW (tree_test->tlview), foo);
  foo = !foo;
  vbox = gtk_vbox_new (FALSE, 10);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 10);
  hbox = gtk_hbox_new (FALSE, 10);
  cbox = gtk_check_button_new_with_label ("Set show headers");
  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (cbox), gtk_tlview_get_headers_visible (GTK_TLVIEW (tree_test->tlview)));
  gtk_signal_connect (GTK_OBJECT (cbox), "toggled", GTK_SIGNAL_FUNC (cbox_toggled), tree_test->tlview);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  scroll = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_add (GTK_CONTAINER (scroll), tree_test->tlview);
  gtk_container_add (GTK_CONTAINER (window), vbox);
  gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), cbox, FALSE, FALSE, 0);
  button = gtk_button_new_with_label ("Expand All");
  gtk_signal_connect (GTK_OBJECT (button), "clicked", expand_all_cb, tree_test->tlview);
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);

  button = gtk_button_new_with_label ("Shrink All");
  gtk_signal_connect (GTK_OBJECT (button), "clicked", shrink_all_cb, tree_test->tlview);
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), gtk_hseparator_new (), FALSE, FALSE, 0);

  hbox2 = gtk_hbox_new (FALSE, 10);
  gtk_box_pack_start (GTK_BOX (vbox), hbox2, FALSE, FALSE, 0);
  vbox2 = gtk_vbox_new (FALSE, 5);
  gtk_box_pack_start (GTK_BOX (hbox2), vbox2, FALSE, FALSE, 0);
  button = gtk_radio_button_new_with_label (list, "Use model FOO (regular model)");
  gtk_object_set_user_data (GTK_OBJECT (button), tree_test);
  gtk_signal_connect (GTK_OBJECT (button), "toggled", model_change_callback, tree_model);
  list = gtk_radio_button_group (GTK_RADIO_BUTTON (button));
  gtk_box_pack_start (GTK_BOX (vbox2), button, FALSE, FALSE, 0);
  button = gtk_radio_button_new_with_label (list, "Use model BAR (small model)");
  gtk_object_set_user_data (GTK_OBJECT (button), tree_test);
  gtk_signal_connect (GTK_OBJECT (button), "toggled", model_change_callback, tree_model2);
  list = gtk_radio_button_group (GTK_RADIO_BUTTON (button));
  gtk_box_pack_start (GTK_BOX (vbox2), button, FALSE, FALSE, 0);
  button = gtk_radio_button_new_with_label (list, "Use model BAZ (large model)");
  gtk_object_set_user_data (GTK_OBJECT (button), tree_test);
  gtk_signal_connect (GTK_OBJECT (button), "toggled", model_change_callback, tree_model3);
  list = gtk_radio_button_group (GTK_RADIO_BUTTON (button));
  gtk_box_pack_start (GTK_BOX (vbox2), button, FALSE, FALSE, 0);
  button = gtk_radio_button_new_with_label (list, "Use no model");
  gtk_object_set_user_data (GTK_OBJECT (button), tree_test);
  list = gtk_radio_button_group (GTK_RADIO_BUTTON (button));
  gtk_signal_connect (GTK_OBJECT (button), "toggled", model_change_callback, NULL);
  gtk_box_pack_start (GTK_BOX (vbox2), button, FALSE, FALSE, 0);

  gtk_box_pack_start (GTK_BOX (hbox2), gtk_vseparator_new (), FALSE, FALSE, 0);

  vbox2 = gtk_vbox_new (FALSE, 5);
  gtk_box_pack_start (GTK_BOX (hbox2), vbox2, FALSE, FALSE, 0);
  tree_test->omenu = gtk_option_menu_new ();
  gtk_box_pack_start (GTK_BOX (vbox2), tree_test->omenu, FALSE, FALSE, 0);
  list = NULL;
  tree_test->button1 = gtk_radio_button_new_with_label (list, "Column Resizeable");
  gtk_box_pack_start (GTK_BOX (vbox2), tree_test->button1, FALSE, FALSE, 0);

  list = gtk_radio_button_group (GTK_RADIO_BUTTON (tree_test->button1));
  tree_test->button2 = gtk_radio_button_new_with_label (list, "Column Autosize");
  gtk_box_pack_start (GTK_BOX (vbox2), tree_test->button2, FALSE, FALSE, 0);

  list = gtk_radio_button_group (GTK_RADIO_BUTTON (tree_test->button2));
  tree_test->button3 = gtk_radio_button_new_with_label (list, "Column Fixed");
  gtk_box_pack_start (GTK_BOX (vbox2), tree_test->button3, FALSE, FALSE, 0);

  hbox3 = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox3, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox3), gtk_label_new ("Fixed Size: "), FALSE, FALSE, 0);
  tree_test->size_entry = gtk_entry_new ();
  gtk_widget_set_usize (tree_test->size_entry, 100, -1);
  gtk_box_pack_start (GTK_BOX (hbox3), tree_test->size_entry, FALSE, FALSE, 0);

  hbox3 = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox3, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox3), gtk_label_new ("Min Size: "), FALSE, FALSE, 0);
  tree_test->min_entry = gtk_entry_new ();
  gtk_widget_set_usize (tree_test->min_entry, 100, -1);
  gtk_box_pack_start (GTK_BOX (hbox3), tree_test->min_entry, FALSE, FALSE, 0);

  hbox3 = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox3, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox3), gtk_label_new ("Max Size: "), FALSE, FALSE, 0);
  tree_test->max_entry = gtk_entry_new ();
  gtk_widget_set_usize (tree_test->max_entry, 100, -1);
  gtk_box_pack_start (GTK_BOX (hbox3), tree_test->max_entry, FALSE, FALSE, 0);

  vbox2 = gtk_vbox_new (FALSE, 5);
  button = gtk_button_new_with_label ("Apply Column\nSettings");
  gtk_box_pack_start (GTK_BOX (hbox2), vbox2, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox2), button, FALSE, FALSE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked", apply_column_settings, tree_test);

  tree_test->visible = gtk_check_button_new_with_label ("Column visible");
  gtk_box_pack_start (GTK_BOX (vbox2), tree_test->visible, FALSE, FALSE, 0);
  tree_test_set_model (tree_test);

  column_select_callback (GTK_OPTION_MENU (tree_test->omenu)->menu_item,
			  tree_test);

  gtk_box_pack_start (GTK_BOX (hbox2), gtk_vseparator_new (), FALSE, FALSE, 0);

  
  vbox2 = gtk_vbox_new (FALSE, 5);
  gtk_box_pack_start (GTK_BOX (hbox2), vbox2, FALSE, FALSE, 0);

  button = gtk_button_new_with_label ("Print selection");
  gtk_box_pack_start (GTK_BOX (vbox2), button, FALSE, FALSE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked", print_selection, tree_test);

  button = gtk_button_new_with_label ("Select all");
  gtk_box_pack_start (GTK_BOX (vbox2), button, FALSE, FALSE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked", select_all, tree_test);

  button = gtk_button_new_with_label ("Unselect all");
  gtk_box_pack_start (GTK_BOX (vbox2), button, FALSE, FALSE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked", unselect_all, tree_test);

  button = gtk_radio_button_new_with_label (NULL, "Single-selection");
  gtk_signal_connect (GTK_OBJECT (button), "toggled", single_selection_cb, tree_test);
  gtk_box_pack_start (GTK_BOX (vbox2), button, FALSE, FALSE, 0);
  list = gtk_radio_button_group (GTK_RADIO_BUTTON (button));

  button = gtk_radio_button_new_with_label (list, "Multi-selection");
  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (button), TRUE);
  gtk_signal_connect (GTK_OBJECT (button), "toggled", multi_selection_cb, tree_test);
  gtk_box_pack_start (GTK_BOX (vbox2), button, FALSE, FALSE, 0);

  gtk_window_set_default_size (GTK_WINDOW (window), 200, 500);
  gtk_widget_show_all (window);
}


static GtkObject *
make_model (gchar *prefix,
	    gint first,
	    gint second,
	    gint third,
	    gint columns,
	    gboolean set_rand)
{
  GtkTreeNode *node, *prev = NULL;
  GtkObject *tree_model;
  gint i, j, k, l;
  gchar *title;
  gint rand_num;
  gint count = 0;

  tree_model = gtk_tree_model_new ();
  gtk_tree_model_set_columns (GTK_TREE_MODEL (tree_model), columns);

  for (i = 0; i < first; i++)
    {
      rand_num = (int) (10.0*rand()/(RAND_MAX+1.0));
      if (!set_rand)
	rand_num = -1;
      node = gtk_tree_model_node_new ();
      for (l = 0; l < columns; l++)
	{
	  if (i == 2000)
	    gtk_tree_model_node_set_cell (GTK_TREE_MODEL (tree_model),
					  node,
					  l, renderer,
					  g_strdup_printf ("This CELL is a really really long Cell at : %d", i));
	  else if (rand_num != l)
 	    gtk_tree_model_node_set_cell (GTK_TREE_MODEL (tree_model),
					  node,
					  l, renderer,
					  g_strdup_printf ("%s %d: %d", prefix, l, i));
	}
      count ++;
      prev = gtk_tree_model_node_insert_before (GTK_TREE_MODEL (tree_model), NULL, prev, node);
      if (i % 3)
	{
	  GtkTreeNode *new_node, *new_prev = NULL;

	  for (j = 0; j < second; j++)
	    {
	      new_node = gtk_tree_model_node_new ();
	      rand_num = (int) (10.0*rand()/(RAND_MAX+1.0));
	      if (!set_rand)
		rand_num = -1;

	      for (l = 0; l < columns; l++)
		{
		  if (i == 2000 && j == 2)
		    gtk_tree_model_node_set_cell (GTK_TREE_MODEL (tree_model),
						  new_node,
						  l, renderer,
						  g_strdup_printf ("This CELL is a really really long Cell at : %d:%d", i, j));
		  else if (rand_num != l)
		    gtk_tree_model_node_set_cell (GTK_TREE_MODEL (tree_model),
						  new_node,
						  l, renderer,
						  g_strdup_printf ("%s %d: %d:%d", prefix, l, i, j));
		}
	      count ++;
	      new_prev = gtk_tree_model_node_insert_before (GTK_TREE_MODEL (tree_model),
							    prev,
							    new_prev,
							    new_node);
	      if (j % 3)
		{
		  GtkTreeNode *new_node2, *new_prev2 = NULL;

		  for (k = 0; k < third; k++)
		    {
		      new_node2 = gtk_tree_model_node_new ();
		      rand_num = (int) (10.0*rand()/(RAND_MAX+1.0));
		      if (!set_rand)
			rand_num = -1;

		      for (l = 0; l < columns; l++)
			{
			  if (i == 2000 && j == 2 && k == 2)
			    gtk_tree_model_node_set_cell (GTK_TREE_MODEL (tree_model),
							  new_node2,
							  l, renderer,
							  g_strdup_printf ("This CELL is a really really long Cell at : %d:%d:%d", i, j, k));
			  else if (rand_num != l)
			    gtk_tree_model_node_set_cell (GTK_TREE_MODEL (tree_model),
							  new_node2,
							  l, renderer,
							  g_strdup_printf ("%s %d: %d:%d:%d", prefix, l, i, j, k));
			}
		      count ++;
		      new_prev2 = gtk_tree_model_node_insert_before (GTK_TREE_MODEL (tree_model),
								     new_prev,
								     new_prev2,
								     new_node2);
		    }
		}
	    }
	}
    }
  g_print ("added %d nodes\n", count);
  return tree_model;
}


int
main (int argc, char *argv[])
{
  gtk_init (&argc, &argv);
  renderer = gtk_tlcell_text_new ();

  srand (0);
  tree_model = make_model ("FOO", 20, 10, 10, 30, FALSE);
  tree_model2 = make_model ("BAR", 5, 5, 5, 2, FALSE);
  tree_model3 = make_model ("BAZ", 50, 40, 50, 4, FALSE);
  
  make_test_window (GTK_TLMODEL (tree_model));
  make_test_window (GTK_TLMODEL (tree_model2));

  gtk_main ();
  return 0;
}
